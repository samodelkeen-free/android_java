package com.aistkem.currencyconverter.services.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.aistkem.currencyconverter.helpers.core.CoreDbHelper;
import com.aistkem.currencyconverter.helpers.core.HttpMultipartHelper;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.repository.core.AccountCoreMethods;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;
import com.aistkem.currencyconverter.R;

public class CoreService extends Service {
	
	private Intent mIntent;
	
	private class TransportThreadData {
		String dataName;
		int minSleepTime;
		int minSleepCount;
		int maxSleepCount;
		int maxFaults;
	}

	protected final String LOG_TAG = this.getClass().getName();
	private final Context mServiceContext = this;
	protected CoreApplication appContext;
	protected String serverUrl;

	private CoreDbHelper dbHelper;
	private SQLiteDatabase db;

	private Handler handler;
	
	private MessageTransportRun messageTransportRun;
	private ServiceTransportRun serviceTransportRun;
	
	private Thread messageTransportThread;
	private Thread serviceTransportThread;
	
	private ServiceBinder binder;

	protected SharedPreferences sharedPreferences;
	
	private static final int MSGTRANS_MIN_SLEEP_TIME = 60; 	//квант времени между сессиями отправки-приемки данных, сек.
	private static final int MSGTRANS_MIN_SLEEP_COUNT = 10; //минимальное число квантов времени между сессиями отправки-приемки данных
	private static final int MSGTRANS_MAX_SLEEP_COUNT = 60; //максимальное число квантов времени между сессиями отправки-приемки данных
	private static final int MSGTRANS_MAX_FAULTS = 10;		//максимальное число неудачных итераций при отправке-приемке данных

	private static final int SERVICE_MIN_SLEEP_TIME = 1; 	//квант времени между сессиями отправки-приемки данных, сек.
	private static final int SERVICE_MIN_SLEEP_COUNT = 1; 	//минимальное число квантов времени между сессиями отправки-приемки данных
	private static final int SERVICE_MAX_SLEEP_COUNT = 600; //максимальное число квантов времени между сессиями отправки-приемки данных
	private static final int SERVICE_MAX_FAULTS = 10;		//максимальное число неудачных итераций при отправке-приемке данных

	protected boolean isStarted = false;
	protected int NOTIFICATION_ID;

	public volatile WakeLock ServiceWakeLock;

    public CoreService() {
    }

    public CoreService(Context context) {
    }

	protected int GetNotificationId() {
		return NOTIFICATION_ID;
	}

    public void DoAwakeTransport(String dataName) {
    	// Прерываем "спячку" транспортной машины:
    	if (dataName.equals("msg")) {
    		messageTransportRun.keepRunning = true;
    		if (messageTransportThread != null && messageTransportThread.isAlive()) messageTransportThread.interrupt();
    	}
    	else if (dataName.equals("srv")) {
    		serviceTransportRun.keepRunning = true;
    		serviceTransportRun.needPause = false;
    		if (serviceTransportThread != null && serviceTransportThread.isAlive()) serviceTransportThread.interrupt();
    	}
    	//ISmartCoreApplication.saveLog(this, dataName + " awakened on " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
    }

    /**
     * Function to show settings alert dialog.
     * On pressing the Settings button it will launch Settings Options.
     * */
    public void showSettingsAlert(){
    	/*
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // On pressing the cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        */
    }

    @Override
	public void onCreate() {
		super.onCreate();
		
	    dbHelper =  appContext.GetDbHelper();
	    serverUrl = dbHelper.GetOptionValue(appContext.GetDb(), "url");
	    
	    binder = new ServiceBinder();
	    
        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
            	String str = "" + msg.obj;
            	Toast.makeText(mServiceContext, str, Toast.LENGTH_LONG).show();
            };
        };

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        ServiceWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);

        /* Прописываем ключ устройства в базе */
		TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		dbHelper.SetOptionValue(appContext.GetDb(), "crypt_key", CommonCoreMethods.md5(telephonyManager.getDeviceId()));
			
		Log.d(LOG_TAG, "onCreate");
	}

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(LOG_TAG, "onStartCommand");
		int result = super.onStartCommand(intent, flags, startId);
		mIntent = intent;
		if (mIntent != null) {

		} else {

		}	
	    readFlags(flags);
	    
	    if (!isStarted) {
	    	/* Создание экземпляров потоков */
		    messageTransportRun = new MessageTransportRun(startId);
		    serviceTransportRun = new ServiceTransportRun(startId);
		    /* Старт транспортной машины сообщений */
		    messageTransportThread = new Thread(messageTransportRun);
		    messageTransportThread.start();
		    /* Старт транспортной машины сервисов */
		    StartSrvTransportThread();
			//Toast.makeText(this, R.string.service_started, Toast.LENGTH_LONG).show();
			//isStarted = true;
	    }
		return result;
		//return START_STICKY;
	}

    @Override
	public void onDestroy() {
		if (isStarted) {
			StopMsgTransportThread(5000);
			StopSrvTransportThread(5000);
		    
	        if (ServiceWakeLock.isHeld()) {
	        	ServiceWakeLock.release();
	        }
	        
			isStarted = false;
			Toast.makeText(this, R.string.service_stopped, Toast.LENGTH_LONG).show();
			Log.v(LOG_TAG, "onDestroy. Stop at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
		}
		super.onDestroy();
	}

    /**
     * Возвращает ссылку на объект подключения к БД сервиса
     * @return
     */
	public final CoreDbHelper GetDbHelper() {
		return this.dbHelper;
	}

	/**
	 * Возвращает ссылку на БД сервиса
	 * @return
	 */
	public SQLiteDatabase GetDb() {
		return appContext.GetDb();
	}

	private void transportLoop(CommonTransportRun transportRun, TransportThreadData data) {
		int transportFaults = 0;
		int sleepCount = data.minSleepCount;
		int maxSleepCount = data.minSleepCount;
		//SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    	
		transportRun.isRunning = true;
		while (transportRun.keepRunning)
			try {
				db = appContext.GetDb();
				sleepCount = data.minSleepCount;
				
				if (data.dataName.equals("msg")) {	// отправка сообщений на наш сервер
					/* Отправка-получение сообщений с сервера */
					// TODO: допилить отправку порциями!
					// Чтение данных из БД и формирование строки для отправки на сервер:
					String str = ""; 		// Хранит человекочитаемую строку для отображения в логах
					String msgdata = "";	// Хранит сроку сообщения для отправки на сервер
					String id = "";			// Строка идентификаторов таблицы для отправленных записей
					ArrayList<String> attachments = new ArrayList<String>();
					String[] columns = {"_id", "request", "timemark", "userid", "guid"};
					Cursor cur = db.query("requests", columns, "userid = " + appContext.userId + " AND sent = 0 AND server = 0", null, null, null, "timemark", null);
					// Ставим позицию курсора на первую строку выборки, и если в выборке нет строк, вернется false:
					if (cur.moveToFirst()) do {
						str +=
						columns[0] + "=" + cur.getLong(0) + ";" +
						columns[1] + "=" + cur.getString(1) + ";" +
						//columns[2] + "=" + new java.sql.Timestamp(cur.getLong(2)).toString() + ";" + //TODO: Переделать под формат MySql
						columns[2] + "=" + appContext.simpleDateFormat.format(cur.getLong(2)) + ";" +
						columns[3] + "=" + cur.getLong(3) + ";" +
						columns[4] + "=" + cur.getString(4) + "\n";
						
						String request = cur.getString(1).replaceFirst("\\|\\|$", "");	// Отрезаем концевой разделитель "||" в запросе
						//request = request.contains("|") ? request : request + "|";
						String[] requestArray = request.split("\\|\\|", 3);				// Выбираем первые три элемента с разделителем "||" (название запроса, данные, вложения).
						msgdata += (!msgdata.isEmpty() ? "," : "") +
						"{" +
						// Блок "request" для наименования запроса:
						"\"request\"" + ":\"" + requestArray[0].replace("\"", "\\\"").replaceAll("[\\n\\r]", "\\\\n").replaceAll("[\\t]", " ") + "\"," +
						// Блок "data" для параметров, передаваемых в формате JSON:
						(requestArray.length > 1 ? "\"data\"" + ":" + requestArray[1].replaceAll("[\\n\\r]", "\\\\n").replaceAll("[\\t]", " ") + "," : "") + // Если имеются дополнительные данные, то заносим их в пакет запроса.
						// Блок "attachment" для объявления файлов, передаваемых на сервер:
						(requestArray.length > 2 ? "\"attachment\"" + ":" + requestArray[2] + "," : "") + // Если имеются вложения, то объявляем их в пакете запроса.
						//"\"time\"" + ":\"" + new java.sql.Timestamp(cur.getLong(2)).toString() + "\"," + //TODO: Переделать под формат MySql
						"\"time\"" + ":\"" + appContext.simpleDateFormat.format(cur.getLong(2)) + "\"," +
						"\"guid\"" + ":\"" + cur.getString(4) + "\"" +
						"}";
						
						// Поиск всех файлов-вложений в сообщении:
						if (requestArray[0].equals(Constants.REQUEST_MESSAGE) || requestArray[0].equals(Constants.REQUEST_LOGS) || requestArray[0].equals(Constants.REQUEST_REPORT)) {
							if (requestArray.length > 2) { // Если имеются прикрепленные файлы:
								JSONArray requestAttachments = new JSONArray(requestArray[2]);
								for (int ii = 0; ii < requestAttachments.length(); ++ii) {
									JSONObject attachment = requestAttachments.getJSONObject(ii);
									attachments.add(cur.getString(4) + "|" + attachment.getString("file"));
								}
							}
						}
						
						id += (!id.equals("") ? "," : "") + cur.getLong(0);
					} while (cur.moveToNext());
					else str += "Nothing to send.";
					cur.close();
					Log.d(LOG_TAG, "transport-" + data.dataName + "> " + str);
					// Отправка данных на сервер:
					String httpAnswer = "";
					if (!msgdata.isEmpty()) {
						// Заполнение заголовочной части:
						String header = 
						"\"user\"" + ":" + appContext.userId + "," +
						"\"key\"" + ":\"" + dbHelper.GetOptionValue(db, "crypt_key") + "\"," +
						"\"message\":";
						String post = header + "[" + msgdata + "]";
						// TODO: Шифрование
						post = "{" + post + "}";
						//ISmartCoreApplication.saveLog(this, "transport-" + data.dataName + "> " + post);
						try {
							transportRun.httpMultipartHelper.setTimeout(5000);
							transportRun.httpMultipartHelper.start(serverUrl, "UTF-8");
							//transportRun.httpMultipartHelper.addHeaderField("User-Agent", "CodeJava");
							//transportRun.httpMultipartHelper.addHeaderField("Test-Header", "Header-Value");
							
							transportRun.httpMultipartHelper.addFormField("msgdata", post);
							for (int ii = 0; ii < attachments.size(); ++ii) {
								try {
									File file = new File(attachments.get(ii).substring(attachments.get(ii).indexOf("|") + 1, attachments.get(ii).length()));
									transportRun.httpMultipartHelper.addFilePart("msgattach[]", file);
								} catch (FileNotFoundException e) {
									if (!dbHelper.ChangeRequestStatus(appContext.GetDb(), attachments.get(ii).substring(0, attachments.get(ii).indexOf("|")), null, 2, e.toString())) {
									}
									throw e;
								}
							}
							List<String> response = transportRun.httpMultipartHelper.finish();
							httpAnswer = TextUtils.join("\n", response);
							//System.out.println("SERVER REPLIED:");
							//for (String line : response) {
							//	System.out.println(line);
							//}
						} catch (FileNotFoundException e) {
							transportRun.needPause = false;
							//ISmartCoreApplication.saveLog(this, "transport-" + data.dataName + "> " + e.toString());
							Log.e(LOG_TAG, "transport-" + data.dataName  + "> " + e.toString());
						} catch (SocketTimeoutException e) {
							httpAnswer = e.toString();
						} catch (IOException e) {
							httpAnswer = e.toString();
						}
					}
					// Обработка ответа сервера:
					if (httpAnswer.contains(Constants.HTTP_OK)) {
						transportFaults = 0;
						maxSleepCount = data.minSleepCount;
						// Парсинг полученного ответа от сервера и занесение его в БД:
						//httpAnswer = httpAnswer.substring(MainActivity.HTTP_OK.length() + 1, httpAnswer.length());
		            	id = "";
			            str = "";
			            int msgStatus = Constants.STATUS_MSG;
			            try {
			            	Object answerData = (new JSONObject(httpAnswer)).get(Constants.HTTP_OK);
			            	if (answerData instanceof JSONArray) {
				            	ContentValues cv;
				            	//JSONArray answers = new JSONArray(httpAnswer);
				            	JSONArray answers = (JSONArray)answerData;
				                for (int ii = 0; ii < answers.length(); ++ii) {
				                	JSONObject answer = answers.getJSONObject(ii);
				                	JSONObject value = answer.getJSONObject("answer");
				                	String time = answer.getString("time");
				                	String guid = answer.getString("guid");
				                	long milliseconds = 0;
				                	try {
				                		milliseconds = appContext.simpleDateFormat.parse(time).getTime();
				                	} catch (ParseException e) {
				                		milliseconds = System.currentTimeMillis();
				                	}
				                	cv = new ContentValues();
									cv.put("answer", value.toString());
									cv.put("timemark", milliseconds);
								    cv.put("sent", 1);
								    long rowCount = db.update("requests", cv, "guid = '" + guid + "'", null);
								    if (rowCount == 0)
								    	// Не удалось добавить ответ сервера в БД:
								    	Log.e(LOG_TAG, "transport-" + data.dataName + "> Error on updating requests database row.");
								    else {	// Сообщение от сервера успешено добавлено в БД.
								    	
								    	/* Выбор обработчика в соответствии с типом входящего сообщения */
								    	switch (value.getString("type")) {
								    	
								    	/* Получен идентификатор пользователя */
								    	case Constants.ANSWER_USER_ID:
							    			JSONObject dataObject = value.getJSONObject("data");
							    			if (dataObject.length() > 0) {
								    			//AccountCoreMethods.getInstance().ClearPrivateData(); EVV: Нельзя здесь этого делать!
								    			AccountCoreMethods.getInstance().InitUser(dataObject.getInt("id"), dataObject.getString("family"), dataObject.getString("name"), dataObject.getString("patr"));
								    			// Проверяем, имеются ли анкетные данные на отправку:
								    			String registrationGuid = dbHelper.GetUserValue(appContext.GetDb(), -1, "registration_guid");
								    			if (registrationGuid != null && !registrationGuid.isEmpty()) {
								    				// Помечаем запрос с анкетными данными к отправке:
								    				dbHelper.ChangeRequestStatus(appContext.GetDb(), registrationGuid, String.valueOf(appContext.userId), 0, null);
								    				// Снимаем пометку отправки анкетных данных:
								    				dbHelper.SetUserValue(appContext.GetDb(), -1, "registration_guid", null);
								    				transportRun.needPause = false;
								    				//DoAwakeTransport("msg");
								    			}
								    			msgStatus = Constants.STATUS_MSG_LOGIN;
							    			} else msgStatus = Constants.STATUS_MSG_SPECIAL;
							    		break;
							    		
							    		/* Получен временный пароль */
								    	case Constants.ANSWER_SMS_PASSWORD:
								    		 msgStatus = Constants.STATUS_MSG_SPECIAL;
								    	break;
								    	
								    	/* Получено подтверждение доставки сообщения */
								    	case Constants.ANSWER_MESSAGE:
							    			JSONObject messageData = value.getJSONObject("data");
							    			String message;
							    			if (Constants.ANSWER_MSG_OK.equals(value.getString("message"))) {
								    			switch (messageData.getString("content")) {
									    			case "report":
									    				// Делаем пометку в таблице отчетов, что клиентское сообщение доставлено:
										    			cv = new ContentValues();
										    			cv.put("sent", 1);
									    				rowCount = db.update("reports", cv, "guid = '" + guid + "'", null);
									    				break;
									    			case "logs":
									    				if (AccountCoreMethods.getInstance().DeleteSentLogs(guid)) {
									    					// Сделать что-то, если цикл доставки лога успешно отработан?
									    					rowCount = 1;
									    				} else {
									    					// Сделать отметку, что надо попытаться повторно отработать?
									    				}
									    				break;
									    			default:
									    				// Делаем пометку в таблице сообщений, что клиентское сообщение доставлено:
										    			cv = new ContentValues();
										    			cv.put("sent", 1);
									    				rowCount = db.update("messages", cv, "direction = 0 AND guid = '" + guid + "'", null);
								    			}
								    			if (rowCount != 0) {
								    				message = getString(R.string.message_delivered);
								    			} else {
								    				message = getString(R.string.server_message_save_error);
								    				Log.e(LOG_TAG, "transport-" + data.dataName + "> Error on updating client's message row into DB.");
								    			}
							    			} else {
							    				message = value.getString("message");
							    			}
									    	cv = new ContentValues();
							    			cv.put("message", message);
							    			cv.put("direction", 1);
											cv.put("timemark", milliseconds);
										    cv.put("userid", appContext.userId);
										    cv.put("guid", guid);
										    long rowID = db.insert("messages", null, cv);
										    if (rowID > -1) {
										    	Log.d(LOG_TAG, "Server message inserted into DB, ID=" + rowID);
										    } else {
										    	Log.e(LOG_TAG, "Error on inserting server message into DB.");
										    }
							    			msgStatus = Constants.STATUS_MSG;
								    	break;
								    	
								    	/* Анкета пользователя доставлена на сервер */
								    	case Constants.ANSWER_REGISTRATION:
								    		msgStatus = Constants.STATUS_MSG_REGISTRATION;
								    	break;
								    	
								    	/* Получена информация из удаленного словаря */
								    	case Constants.ANSWER_DICTIONARY:
							    			msgStatus = Constants.STATUS_MSG_DICTIONARY;
							    			dataObject = value.getJSONObject("data");
								    		String dictionary = dataObject.getString("dictionary");
								    		String searchPrefix = dbHelper.GetMessageValue(db, guid); //TODO: Возможно, стоит возвращать префикс в пакете?
								    		int searchPosition = searchPrefix.indexOf("_");
								    		if (searchPosition > -1) searchPrefix = searchPrefix.substring(0, searchPosition + 1);
								    		else searchPrefix = null;
								    		JSONArray dataArray = dataObject.getJSONArray("items");
								    		for (int jj = 0; jj < dataArray.length(); ++jj) {
								    			JSONObject item = dataArray.getJSONObject(jj);
								    			cv = new ContentValues();
								    			cv.put("remoteid", item.getInt("id"));
											    cv.put("dictionary", dictionary);
											    cv.put("search_prefix", searchPrefix);
											    cv.put("name", item.getString("name"));
											    cv.put("name_rus", item.getString("name_rus"));
											    cv.put("remark", item.getString("remark"));
											    rowID = db.insertWithOnConflict("remote_dictionaries", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
											    if (rowID > -1) {
											    	Log.d(LOG_TAG, "Remote dictionary item inserted into DB, ID=" + rowID);
											    	int countAfter = dbHelper.GetDictionaryCount(db, dictionary);
											    	Log.d(LOG_TAG, "Remote dictionary " + dictionary + " size: " + countAfter);
											    } else {
											    	Log.e(LOG_TAG, "Error on inserting remote dictionary item into DB.");
											    }
								    		}
								    		cv = new ContentValues();
							    			if (Constants.ANSWER_MSG_OK.equals(value.getString("message"))) {
								    			// Делаем пометку в таблице сообщений, что пришел справочник:
								    			cv.put("sent", 1);
							    			} else {
								    			// Делаем пометку в таблице сообщений, что не удалось получить справочник:
								    			cv.put("sent", 2);
							    				CoreApplication.saveLog(this, "transport-" + data.dataName + "> " + value.getString("message"));
							    			}
							    			rowCount = db.update("messages", cv, "direction = 0 AND guid = '" + guid + "'", null);
							    			if (rowCount != 0) {
							    				Log.d(LOG_TAG, "transport-" + data.dataName + "> 'Request for dictionary' row updated into DB.");
							    			} else {
							    				Log.e(LOG_TAG, "transport-" + data.dataName + "> Error on updating 'request for dictionary' row into DB.");
							    			}
							    		break;
								    	
							    		/* Получена информация о пользователе */
								    	case Constants.ANSWER_USER_INFO:	// TODO: Пренести в класс методов авторизации
							    			msgStatus = Constants.STATUS_MSG_USER_INFO;
							    			dataObject = value.getJSONObject("data");
								    		cv = new ContentValues();
								    		cv.put("family", dataObject.getString("family"));
								    		cv.put("name", dataObject.getString("name"));
								    		cv.put("patr", dataObject.getString("patr"));
								    		cv.putNull("registration_guid");
								    		rowCount = db.update("userinfo", cv, "_id = " + appContext.userId, null);
											if (rowCount != 0) Log.d(LOG_TAG, "User info updated into DB, ID=" + appContext.userId);
											else Log.e(LOG_TAG, "Error on updating user info into DB.");
										break;
										
									    /* Получено обычное сообщение от сервера: */
							    		default:
								    		if (Constants.ANSWER_MSG_OK.equals(value.getString("message"))) {
									    		//String[] messageArray = valueArray[2].split("\\<html\\>(.|\\n)*\\<\\/html\\>", -1);
								    			dataArray = value.getJSONArray("data");
									    		for (int jj = 0; jj < dataArray.length(); ++jj) {
									    			JSONObject news = dataArray.getJSONObject(jj);
									    			cv = new ContentValues();
									    			cv.put("message", news.getString("text"));
									    			cv.put("direction", 1);
													cv.put("timemark", milliseconds/*simpleDateFormat.parse(news.getString("time")).getTime()*/);
												    cv.put("userid", appContext.userId);
												    cv.put("guid", news.getString("guid"));
												    rowID = db.insert("messages", null, cv);
												    if (rowID > -1) {
												    	Log.d(LOG_TAG, "Server message inserted into DB, ID=" + rowID);
												    } else {
												    	Log.e(LOG_TAG, "Error on inserting server message into DB.");
												    }
									    		}
								    		} else {
								    			cv = new ContentValues();
								    			cv.put("message", getString(R.string.server_news_error) + " " + value.getString("message"));
								    			cv.put("direction", 1);
												cv.put("timemark", milliseconds);
											    cv.put("userid", appContext.userId);
											    cv.put("guid", guid);
											    rowID = db.insert("messages", null, cv);
											    if (rowID > -1) {
											    	Log.d(LOG_TAG, "Server message inserted into DB, ID=" + rowID);
											    } else {
											    	Log.e(LOG_TAG, "Error on inserting server message into DB.");
											    }
								    		}
								    	break;
								    	
								    	}	// end of switch
								    }	// Сообщение от сервера успешено добавлено в БД.
				                	str += (str.isEmpty() ? "" : ";") + value;
				                }
			            	} else {
				            	// Обработка ошибок уровня сервера:
			            		str = (String)answerData;
				            }
			                Log.d(LOG_TAG, "transport-" + data.dataName + "> Server answers: " + str);
			                // Уведомление приложения о полученном ответе:
				    		transportRun.intent.putExtra(Constants.PARAM_TASK, Constants.TASK_RECEIVED_MSG);
			                transportRun.intent.putExtra(Constants.PARAM_STATUS, msgStatus);
			                sendBroadcast(transportRun.intent);
			            } catch (JSONException e) {
			            	Log.e(LOG_TAG, "transport-" + data.dataName + "> " + e.toString());
			            }
						// Пометка отправленных данных в БД (в случае успешной отправки):
			            /* Видимо, уже не нужно (проверить!):
						if (!id.isEmpty()) {
							ContentValues cv = new ContentValues();
							cv.put("sent", 1);
							long rowCount = db.update("requests", cv, "_id in (" + id + ")", null);
							Log.d(LOG_TAG, "transport-" + data.dataName + "> Sent DB rows marked: " + rowCount);
						}
						*/
					} else if (!httpAnswer.isEmpty()) {
						transportFaults++;
						if (httpAnswer.contains(Constants.HTTP_ERR)) {
							str = (new JSONObject(httpAnswer)).getString(Constants.HTTP_ERR);
						} else {
							str = httpAnswer;
						}
						Log.e(LOG_TAG, "transport-" + data.dataName + "> " + str);
						CoreApplication.saveLog(this, "transport-" + data.dataName + "> " + str);
						//throw new Exception("Test unhandled exception");
					}

				} else if (data.dataName.equals("srv")) {	// отправка запросов к геосервисам и прочим сторонним серверам
					ContentValues cv;
					String str = ""; // Хранит человекочитаемую строку запроса для отображения в логах, а также ответ сервера для загрузки в БД
                	long time = System.currentTimeMillis(); // Время обработки запроса
                	int status = 0; // Статус запроса к серверу (0-на отправку, 1-отправлен, 2-ошибка отправки, -1-зарезервирован к отправке)
                	int attempt = 0; // Число попыток обращения к сервису с текущим запросом
					ServiceTransportRun serviceTransportRun = (ServiceTransportRun) transportRun;
					try {
						String[] columns = {"_id", "request", "timemark", "userid", "guid", "attempt"};
						Cursor cur = db.query("requests", columns, "userid = " + appContext.userId + " AND sent < 1 AND server > 0", null, null, null, "timemark ASC", null);
						// Ставим позицию курсора на первую строку выборки, а если в выборке нет строк, то вернется false:
						if (cur.moveToFirst()) do {
							str =
								columns[0] + "=" + cur.getLong(0) + ";" +
								columns[1] + "=" + cur.getString(1) + ";" +
								//columns[2] + "=" + new java.sql.Timestamp(cur.getLong(2)).toString() + ";" + //TODO: Переделать под формат MySql
								columns[2] + "=" + appContext.simpleDateFormat.format(cur.getLong(2)) + ";" +
								columns[3] + "=" + cur.getLong(3) + ";" +
								columns[4] + "=" + cur.getString(4) + "\n";
							Log.d(LOG_TAG, "transport-" + data.dataName + "> Service request: " + str);
							
							str = "";
		                	time = System.currentTimeMillis();
		                	status = 0;
		                	String guid = cur.getString(4);
		                	attempt = cur.getInt(5);
							String[] requestArray = cur.getString(1).split("\\|\\|", -1);
							switch (requestArray[0]) {
							case "get_location":
								// Прямое геокодирование (адресов в географические координаты):
								
								break;
							}
							if (status != 0) { // Если изменился статус запроса к сервису, то обновляем его поля в БД:
			                	cv = new ContentValues();
								cv.put("answer", str);
								cv.put("timemark", time);
							    cv.put("sent", status);
							    cv.put("attempt", attempt);
							    long rowCount = db.update("requests", cv, "guid = '" + guid + "'", null);
							    if (rowCount == 0) {
							    	// Не удалось добавить ответ сервера в БД:
							    	Log.e(LOG_TAG, "transport-" + data.dataName + "> Error on updating requests database row.");
							    	throw new Exception("Error on updating requests database row.");
							    }
							}
						} while (cur.moveToNext());
						else  {
							str = "Nothing to process.";
							// Если нет запросов к сторонним сервисам, то устанавливаем максимальное время спячки:
							sleepCount = maxSleepCount;
						}
						cur.close();
					} catch (Exception e) {
						// TODO: Отключать обработчик, который вызвал эту ошибку? Или останавливать транспорт сервисов? Пока останавливаем транспорт.
						transportRun.needPause = false;
						transportRun.keepRunning = false;
						str = CoreApplication.GetStackTrace(e); // Получаем полный текст сообщения об ошибке
						Log.e(LOG_TAG, "transport-" + data.dataName + "> " + str); // Кидаем в консоль
						AccountCoreMethods.getInstance().SendLog("transport-" + data.dataName + "> " + str); // Отправляем почтой на сервер
						CoreApplication.saveLog(this, "transport-" + data.dataName + "> " + str); // Сохраняем в файле логов на устройстве клиента
					}
				}
				
				if (transportFaults >= data.maxFaults) {
					transportFaults = 0;
					if (maxSleepCount < data.maxSleepCount) maxSleepCount++;
					sleepCount = maxSleepCount;
				}
				if (transportRun.needPause)
					try {
						TimeUnit.SECONDS.sleep(data.minSleepTime * sleepCount);
					} catch (InterruptedException e) {
						//Log.d(LOG_TAG, "transport-" + data.dataName + "> " + e.toString());
						//CoreApplication.saveLog(this, "transport-" + data.dataName + "> " + e.toString());
		            }
				else transportRun.needPause = true;
			} catch (Exception e) {
				String message = CoreApplication.GetStackTrace(e);
				Log.e(LOG_TAG, "transport-" + data.dataName + "> " + message + ". Transport stopped!");
				AccountCoreMethods.getInstance().SendLog("transport-" + data.dataName + "> " + message);
				CoreApplication.saveLog(this, "Critical transport-" + data.dataName + " exception> " + message);
				//throw new RuntimeException("For test only!", e);
			}
		transportRun.isRunning = false;
	}

	void readFlags(int flags) {
		if ((flags&START_FLAG_REDELIVERY) == START_FLAG_REDELIVERY)
		  Log.d(LOG_TAG, "START_FLAG_REDELIVERY");
		if ((flags&START_FLAG_RETRY) == START_FLAG_RETRY)
		  Log.d(LOG_TAG, "START_FLAG_RETRY");
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(LOG_TAG, "GeoService onBind");
		return binder;
	}

	public void onRebind(Intent intent) {
		super.onRebind(intent);
		Log.d(LOG_TAG, "GeoService onRebind");
	}

	public boolean onUnbind(Intent intent) {
		Log.d(LOG_TAG, "GeoService onUnbind");
		//return super.onUnbind(intent); TODO: Разобраться!
		return true;
	}

	void StopMsgTransportThread(int wait) {
		/* Остановка транспортной машины сообщений */
		messageTransportRun.keepRunning = false;
		//messageTransportRun.awake = true;
		messageTransportRun.HttpAbort();
		if (messageTransportThread != null && messageTransportThread.isAlive()) {
			messageTransportThread.interrupt();
			try { messageTransportThread.join(wait);
			} catch(InterruptedException e){}
		}
	}

	public void StartSrvTransportThread() {
		//if (!isLogined) return;
		if (serviceTransportThread != null && serviceTransportThread.isAlive()) {
			if (serviceTransportRun.isRunning) {
				serviceTransportRun.keepRunning = true;
				return;
			}
			else try { serviceTransportThread.join(1000);
				} catch(InterruptedException e){}
		}
		if (serviceTransportThread == null || !serviceTransportThread.isAlive()) {
			serviceTransportThread = new Thread(serviceTransportRun);
			serviceTransportThread.start();
		}
	}

	public void StopSrvTransportThread(int wait) {
		serviceTransportRun.keepRunning = false;
		//serviceTransportRun.awake = true;
		//serviceTransportThread.getState();
		//serviceTransportRun.HttpAbort();
		if (serviceTransportThread != null && serviceTransportThread.isAlive()) {
			serviceTransportThread.interrupt();
			try { serviceTransportThread.join(wait);
			} catch(InterruptedException e){}
		}
	}

	/**
	 * Отправка сообщений серверу
	 * @param server - идентификатор сервера (0 - основной сервер приложения)
	 * @param guidArray - список идентификаторов запросов в БД
	 * @param params - строки запросов к серверу
	 */
	public void SendMessage(int server, ArrayList<String> guidArray, String... params) {
		dbHelper.MessageToQueue(appContext.GetDb(), server, guidArray, params);
		// Прерываем "спячку" транспортной машины сообщений (либо сервисов):
		if (server == 0) DoAwakeTransport("msg");
		else DoAwakeTransport("srv");
	}

	private void tryStopService(int startId) {
		if (!messageTransportRun.isRunning && !serviceTransportRun.isRunning) {
			//dbHelper.close();
			appContext.CloseDb();
			Log.d(LOG_TAG, "Service stopped by startId#" + startId + ", stopSelfResult(" + startId + ") = " + stopSelfResult(startId));
		}
	}

	private class CommonTransportRun implements Runnable {
		protected int startId;
		protected final String LOG_TAG = this.getClass().getName();
		protected HttpMultipartHelper httpMultipartHelper;
		protected Intent intent;
		
		public volatile boolean keepRunning = true;
		public volatile boolean needPause = true;
		//public volatile boolean awake = false;
		public volatile boolean isRunning = false;

	    public CommonTransportRun() {
		}

	    public CommonTransportRun(int startId) {
	    	this.startId = startId;
	    	httpMultipartHelper = new HttpMultipartHelper(mServiceContext);
		}
	    
	    public void run() {
	    	keepRunning = true;
		    //awake = false;
	    }
	    
	    void stop() {
		    //tryStopService(startId);
	    }
	    
	    // Прерывает HTTP-запрос
	    public void HttpAbort() {
	    	//httpHelper.Abort(); // TODO: Выяснить есть ли подобное для HttpMultipartHelper ?
	    }
	}

	private class MessageTransportRun extends CommonTransportRun {

		public MessageTransportRun(int startId) {
			super(startId);
			Log.d(LOG_TAG, "MsgTransportRun#" + startId + " create");
		}

	    public void run() {
	    	super.run();
	    	intent = new Intent(Constants.BROADCAST_ACTION); // Или на Create ?..
	    	Log.d(LOG_TAG, "MsgTransportRun#" + startId + " start");
	    	TransportThreadData data = new TransportThreadData();
	    	data.dataName = "msg";
			data.minSleepTime = MSGTRANS_MIN_SLEEP_TIME;
			data.minSleepCount = MSGTRANS_MIN_SLEEP_COUNT;
			data.maxSleepCount = MSGTRANS_MAX_SLEEP_COUNT;
			data.maxFaults = MSGTRANS_MAX_FAULTS;
	    	/*Looper.prepare();*/ transportLoop(this, data);
	    	stop();
	    }

	    void stop() {
	    	Log.d(LOG_TAG, "MsgTransportRun#" + startId + " end");
	    	tryStopService(startId);
	    	super.stop();
	    }
	}

	private class ServiceTransportRun extends CommonTransportRun {

		public ServiceTransportRun(int startId) {
			this.startId = startId;
			Log.d(LOG_TAG, "SrvTransportRun#" + startId + " create");
		}

	    public void run() {
	    	super.run();
	    	intent = new Intent(Constants.BROADCAST_ACTION); // Или на Create ?..
	    	Log.d(LOG_TAG, "SrvTransportRun#" + startId + " start");
	    	TransportThreadData data = new TransportThreadData();
	    	data.dataName = "srv";
			data.minSleepTime = SERVICE_MIN_SLEEP_TIME;
			data.minSleepCount = SERVICE_MIN_SLEEP_COUNT;
			data.maxSleepCount = SERVICE_MAX_SLEEP_COUNT;
			data.maxFaults = SERVICE_MAX_FAULTS;
	    	/*Looper.prepare();*/ transportLoop(this, data);
	    	stop();
	    }

	    void stop() {
	    	Log.d(LOG_TAG, "SrvTransportRun#" + startId + " end");
	    	tryStopService(startId);
	    	super.stop();
	    }
	}

	public class ServiceBinder extends Binder {
		public CoreService getService() {
			return CoreService.this;
		}
	}

}
