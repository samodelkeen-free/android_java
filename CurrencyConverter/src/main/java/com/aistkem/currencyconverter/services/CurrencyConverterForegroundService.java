package com.aistkem.currencyconverter.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.aistkem.currencyconverter.services.core.CoreService;
import com.aistkem.currencyconverter.CurrencyConverterApplication;
import com.aistkem.currencyconverter.activities.MainActivity;
import com.aistkem.currencyconverter.R;

public class CurrencyConverterForegroundService extends CoreService {

	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mNotificationBuilder;
	private Notification mNotification;
	
    @Override
	public void onCreate() {
    	NOTIFICATION_ID = 19760118;
	    appContext = (CurrencyConverterApplication) getApplicationContext();
	    sharedPreferences = appContext.getSharedPreferences("currencyconverter", Context.MODE_PRIVATE);
	    mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int result = super.onStartCommand(intent, flags, startId);

		if (!isStarted) {
	    	/* Создание иконки уведомления в топе и перевод сервиса в режим foreground */
			//The intent to launch when the user clicks the expanded notification
			Intent noteIntent = new Intent(this, MainActivity.class);
			noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, noteIntent, 0);
	
			if (Integer.parseInt(Build.VERSION.SDK) >= Build.VERSION_CODES.DONUT) {
				// Build.VERSION.SDK_INT requires API 4 and would cause issues below API 4
	
				mNotificationBuilder = new NotificationCompat.Builder(this);
				mNotificationBuilder.setTicker(getString(R.string.notifyTicker)).setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.notifyNoAction))
					.setWhen(System.currentTimeMillis()).setAutoCancel(false)
					.setOngoing(true).setPriority(Notification.PRIORITY_HIGH)
					.setContentIntent(pendingIntent)
					.setSmallIcon(R.drawable.ic_launcher);
				mNotification = mNotificationBuilder.build();
			} else {
				mNotification = new Notification(R.drawable.ic_launcher, getString(R.string.notifyTicker), System.currentTimeMillis());
				//TODO: Разобраться с deprecated-методом:
				//mNotification.setLatestEventInfo(this, getString(R.string.app_name), getString(R.string.notifyNoAction), pendingIntent);
			}
			mNotification.flags |= Notification.FLAG_NO_CLEAR;
			startForeground(GetNotificationId(), mNotification);
			/* *********************************************************************** */

			Toast.makeText(this, R.string.service_started, Toast.LENGTH_LONG).show();
			isStarted = true;
		}

		return result;
	}

    @Override
	public void onDestroy() {
		if (isStarted) {
	        mNotificationManager.cancelAll();
	        mNotificationManager = null;		
		    stopForeground(true);
		}
		super.onDestroy();
	}

}
