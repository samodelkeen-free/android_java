package com.aistkem.currencyconverter;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import android.util.Log;

public class Classes {

	private final static String LOG_TAG = "com.aistkem.currencyconverter.Classes";

	/**
	 * Базовый класс объекта описания критериев поиска в локальной БД и удаленной БД на сервере приложения
	 * @author Владимир
	 *
	 */
	public abstract static class ListSearch implements Serializable {

		private static final long serialVersionUID = -4930081487875237368L;
		
		protected String guid = null;	// Уникальный идентификатор запроса к серверу
		protected String hash = null;	// Хеш выборки из локальной БД (во избежание повторной передачи данных с сервера)
		protected Object tag = null;	// Свойство для передачи дополнительной информации
		/**
		 * Идентификатор "стартовой" записи в таблице серверной БД
		 */
		protected String start = null;
		/**
		 * Максимальное количество записей в серверной выборке / смещение относительно первой записи (+/-)
		 */
		protected int offset = 25;
		/**
		 * Описание сортировки записей в выборке
		 */
    	public ListSort sort;
    	
		public ListSearch () {}
		
		public String GetGuid() {
			return this.guid;
		}
		
		public void SetGuid(String guid) {
			this.guid = guid;
		}
		
		public void SetStart(String start) {
			this.start = start;
		}
		
		public void SetOffset(int offset) {
			this.offset = offset;
		}
		
		public int GetOffset() {
			return this.offset;
		}
		
		public Object GetTag() {
			return this.tag;
		}
		
		public void SetTag(Object tag) {
			this.tag = tag;
		}
		
		/**
		 * Базовый класс объекта описания сортировки строк выборки
		 * @author Владимир
		 *
		 */
		protected abstract class ListSort implements Serializable {

			private static final long serialVersionUID = -2901047332358263740L;
		
			protected ListSort() {}
	    	
		}
	}

	/**
	 * Класс описания объекта записи внешнего словаря
	 * @author Владимир
	 *
	 */
	public static class RemoteDictionary {
    	int id;
    	int remoteid;
    	public String name;
    	public String remark;
		
		public RemoteDictionary () {
			
		}
		
		public RemoteDictionary (int id, int remoteid, String name, String remark) {
			this.id = id;
			this.remoteid = remoteid;
			this.name =name;
			this.remark = remark;
		}
		
	}

	/**
	 * Класс описания объекта "Валюта" в локальной БД
	 * @author Владимир
	 *
	 */
	public static class Currency implements Serializable {

		private static final long serialVersionUID = 4095277724073066530L;
		
    	public long id;
    	public int numcode;
    	public String charcode;
    	public int nominal;
    	public String name;
    	public float value;
    	public long date;
    	public short usage;
	}

	/**
	 * Класс объекта описания критериев поиска записей во внешнем словаре
	 * @author Владимир
	 *
	 */
	public static class RemoteDictionarySearch extends ListSearch {

		private static final long serialVersionUID = -2018001987278587291L;
		
	    public String dictionary;
	    public String searchPrefix;
	    public String searchConstraint;
	}

	/**
	 * Класс объекта описания критериев поиска курсов валют в локальной и удаленной БД
	 * @author Владимир
	 *
	 */
	public static class CurrencySearch extends ListSearch {

		private static final long serialVersionUID = 7800215118406353807L;
		
		public String name = null;		// Наименование валюты
		public String charcode = null;	// Код валюты

		public CurrencySearch () {
			super();
			sort = new СurrencySort();
	    	start = null;
	    	offset = 25;
		}

		/**
		 * Расчет хеша записей
		 * В контексте этого приложения не используется!
		 * @param сurrencySearch - объект критериев поиска
		 * 
		 */
		public static void calcHash(CurrencySearch сurrencySearch) {
			сurrencySearch.hash = null;
		}

		/**
		 * Генерация JSON-строки из объекта типа СurrencySearch для осуществления запроса к серверу
		 * В контексте этого приложения не используется!
		 * @param сurrencySearch - объект описания критериев поиска
		 * @return Строка в формате JSON
		 */
		public static String getJson(CurrencySearch сurrencySearch) {
			String result = "";
			try {
				JSONObject data = new JSONObject();
				JSONObject search = new JSONObject();
				JSONArray sort = new JSONArray();
				
				search.put("hash", сurrencySearch.hash);
				
				data.put("search", search);
				if (sort.length() == 0) {
					// Сортировка по умолчанию для серверной выборки
				}
				data.put("sort", sort);
				data.put("start", String.valueOf(сurrencySearch.start));
				data.put("offset", Integer.valueOf(сurrencySearch.offset));
				
				result = data.toString();
	        } catch (JSONException e) {
	        	Log.e(LOG_TAG, "СurrencySearch.getJson(): " + e.toString());
	        }
			return result;
		}

		/**
		 * Генерация строки условия для SQL-запроса к локальной БД из объекта типа CurrencySearch
		 * @param сurrencySearch - объект описания критериев поиска
		 * @return Строка условия "WHERE"
		 */
		public static String getWhere(CurrencySearch сurrencySearch) {
			String result = "";
			if (сurrencySearch.name != null) result += "LOWER(name) like '%" + сurrencySearch.name.toLowerCase() + "%'";
			if (сurrencySearch.charcode != null) result += (result.isEmpty() ? "" : " OR ") + "charcode like '%" + сurrencySearch.charcode.toUpperCase() + "%'";
			
			return result.isEmpty() ? "" : " AND (" + result + ")";
		}

		/**
		 * Генерация строки сортировки для SQL-запроса к локальной БД из объекта типа CurrencySearch
		 * @param сurrencySearch - объект описания критериев поиска
		 * @return Строка сортировки "ORDER"
		 */
		public static String getOrder(CurrencySearch сurrencySearch) {
			СurrencySort mSort = (СurrencySort) сurrencySearch.sort;
			String result = "";
			if (mSort.usage != null) result += (result.isEmpty() ? "" : ", ") + "usage " + mSort.usage;
			if (mSort.name != null)	result += (result.isEmpty() ? "" : ", ") + "name " + mSort.name;
			if (mSort.date != null) result += (result.isEmpty() ? "" : ", ") + "date " + mSort.date;
			
			if (result.isEmpty()) {
				mSort.usage = "ASC";
				mSort.name = "ASC";
				result = "usage ASC, name ASC";
			}
			return result;
		}

		private class СurrencySort extends ListSort implements Serializable {

			private static final long serialVersionUID = 8151591387081584282L;
			
			public String usage = "ASC";	// Популярность валюты (0-наиболее употребительная)
			public String name = "ASC";		// Наименование валюты
	    	public String date = null;		// Дата курса
	    	
			private СurrencySort() {
				super();
			}

		}
	}

	/**
	 * Класс описания объекта "Валюта" для десериализации из XML
	 * @author Владимир
	 *
	 */
	@Root(name="Valute")
	public static class Valute 
	{
		@Attribute(name="ID")
	    private String ID;
		
	    @Element(name="NumCode")
	    public int NumCode;

	    @Element(name="CharCode")
	    public String CharCode;
	    
	    @Element(name="Nominal")
	    public int Nominal;
		
	    @Element(name="Name")
	    public String Name;
	    
	    @Element(name="Value")
	    public String Value;
	}

	/**
	 * Класс описания объекта "Список валют" для десериализации из XML
	 * @author Владимир
	 *
	 */
	@Root(name="ValCurs")
	public static class ValCurs
	{
		@Attribute(name="Date")
	    public String Date;
		
		@Attribute(name="name")
	    private String name;
		
	    @ElementList(inline=true, name="Valute")
	    public List<Valute> valute;
	}

	/**
	 * Класс исключения при размещении данных в локальной БД
	 */
	public static class LocalDbDataException extends Exception {

		private static final long serialVersionUID = -8142933228426044652L;

		public LocalDbDataException(String message) {
			super(message);
		}
	}

}
