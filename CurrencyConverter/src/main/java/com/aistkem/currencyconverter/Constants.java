package com.aistkem.currencyconverter;

public final class Constants {
	
	public final static String PACKAGE = "com.aistkem.currencyconverter";

	public final static long LIST_SEARCH_TIMEOUT = 500;	// Таймаут запроса на поиск записи в диалоге поиска по БД
	
	public final static long GEOCODER_GOOGLE_TIMEOUT = 500;	// Таймаут обращения к сервису геокодирования Google
	public final static long PLACE_GOOGLE_TIMEOUT = 1500;	// Таймаут обращения к сервису географических объектов Google
	public final static long GEOCODER_GOOGLE_FAILS = 3;		// Количество неудачных обращений к сервису геокодирования Google, после которого запрос отклоняется

	public final static int DICTIONARY_LIMIT = 200;				// Максимальное количество записей, выбираемых из словаря на сервере
	public final static int DICTIONARY_LIMIT_AUTOCOMPLETE = 10;	// Количество возвращаемых элементов справочника для автодополнения

	/* Список возможных задач приложения Currency Converter */
	public final static int APPLICATION_TASK_FROM		= 1;	// Задача приложения: выбор исходной валюты
	public final static int APPLICATION_TASK_TO			= 2;	// Задача приложения: выбор конечной валюты
	
	public final static int APPLICATION_TASK_NONE		= 0;	// Задача приложения: отсутствие каких-либо текущих задач
	public final static int APPLICATION_TASK_WELCOME	= 3;	// Задача приложения: окно приветствия
	public final static int APPLICATION_TASK_LOGIN		= 4;	// Задача приложения: авторизация
	public final static int APPLICATION_TASK_ABOUT		= 5;	// Задача приложения: окно "О программе"
	public final static int APPLICATION_TASK_OPTIONS	= 6;	// Задача приложения: окно "Настройки"
	public final static int APPLICATION_TASK_TEST		= 1000;	// Задача приложения: окно тестирования

	/* Список задач на обновление содержимого визуальных фрагментов приложения */
	/**
	 * Задача обновления контролов фрагмента: все фрагменты -> обновление компонентов фрагмента по умолчанию
	 */
	public final static int FRAGMENT_REFRESH_TASK_DEFAULT = 0;
	/**
	 * Задача обновления контролов фрагмента: "welcome" -> обновление исходной и конечной валют
	 */
	public final static int FRAGMENT_REFRESH_TASK_CURRENCY = 1;
	/**
	 * Задача обновления контролов фрагмента: "welcome" -> отображение результатов конвертации
	 */
	public final static int FRAGMENT_REFRESH_TASK_RESULT = 2;
	
	public final static int FRAGMENT_REFRESH_TASK_ROUTE_PAY_CALCULATE = 101;	// Задача обновления контролов фрагмента: работа с новым заказом -> стоимость поездки
	public final static int FRAGMENT_REFRESH_TASK_ROUTE_DRAW = 102;				// Задача обновления контролов фрагмента: работа с заказом -> отрисовка маршрута на карте
	public final static int FRAGMENT_REFRESH_TASK_CARRIERS_DRAW = 103;			// Задача обновления контролов фрагмента: работа с заказом -> отрисовка перевозчиков на карте

	/* Default fragments tags */
    public final static String WELCOME_FRAGMENT_TAG = "welcome";
    public final static String ARCHIVE_TAG = "archive";
    public final static String MESSAGE_TAG = "message";
    public final static String TRACK_TAG = "track";
    public final static String CAMERA_TAG = "camera";
    public final static String REGISTRATION_TAG = "registration";
    public final static String LOGIN_FRAGMENT_TAG = "login";
    public final static String OSM_FRAGMENT_TAG = "osm_map";
    public final static String GOOGLE_FRAGMENT_TAG = "google_map";
    public final static String ADDRESS_LIST_FRAGMENT_TAG = "address_list";
    public final static String ADDRESS_INPUT_FRAGMENT_TAG = "address_input";
    public final static String ADDRESS_SEARCH_FRAGMENT_TAG = "address_search";
    public final static String ADDRESS_TYPE_FRAGMENT_TAG = "address_type";
    public final static String ORDER_FRAGMENT_TAG = "order";
    public final static String ORDER_LIST_FRAGMENT_TAG = "order_list";
    public final static String ORDER_INFO_FRAGMENT_TAG = "order_info";
    public final static String ABOUT_FRAGMENT_TAG = "about_us";
    public final static String OPTIONS_FRAGMENT_TAG = "options";
    public final static String TEST_FRAGMENT_TAG = "tests";

    /* Используемые наименования состояний стека фрагментов */
    public final static String STACK_NEW_ORDER = "stack_new_order";	// Форма создания/корректировки нового заказа

	/* Идентификаторы запросов к серверу */
    public final static String REQUEST_SOMETHING = "get_something";
	public final static String REQUEST_SMS_PASSWORD = "get_sms_pass";
	public final static String REQUEST_SMS_PASSWORD_REGISTRATION = "get_sms_pass_reg";
	public final static String REQUEST_USER_ID = "get_userid";
	public final static String REQUEST_MESSAGE = "message";
	public final static String REQUEST_REPORT = "report";
	public final static String REQUEST_LOGS = "logs";
	public final static String REQUEST_NEWS = "get_news";
	public final static String REQUEST_ARCHIVE = "get_archive";
	public final static String REQUEST_TRACK = "get_track";
	public final static String REQUEST_ACTIONS = "get_actions";
	public final static String REQUEST_DICTIONARY = "get_dictionary";
	public final static String REQUEST_REGISTRATION = "registration";
	public final static String REQUEST_USER_INFO = "get_userinfo";
	public final static String REQUEST_ROUTE = "get_route_pay";
	public final static String REQUEST_ORDER = "order";
	public final static String REQUEST_ORDERS = "get_orders";
	public final static String REQUEST_ORDER_INFO = "get_order";
	public final static String REQUEST_ADDRESS = "get_address";
	public final static String REQUEST_FIND_ADDRESS = "find_address_";
	public final static String REQUEST_CARRIERS = "get_carriers";

	/* Идентификаторы ответов сервера */
    public final static String ANSWER_SMS_PASSWORD = "ify_sms_pass";
    public final static String ANSWER_USER_ID = "ify_userid";
    public final static String ANSWER_MESSAGE = "ify_message";
    public final static String ANSWER_NEWS = "ify_news";
    public final static String ANSWER_ERROR = "ify_error";
    public final static String ANSWER_ARCHIVE = "ify_archive";
    public final static String ANSWER_TRACK = "ify_track";
    public final static String ANSWER_ACTIONS = "ify_actions";
    public final static String ANSWER_DICTIONARY = "ify_dictionary";
    public final static String ANSWER_REGISTRATION = "ify_registration";
    public final static String ANSWER_USER_INFO = "ify_userinfo";
    public final static String ANSWER_ROUTE = "ify_route_pay";
    public final static String ANSWER_ORDER = "ify_order";
    public final static String ANSWER_ORDERS = "ify_orders";
    public final static String ANSWER_ORDER_INFO = "ify_order_info";
    public final static String ANSWER_ADDRESS = "ify_address";
    public final static String ANSWER_CARRIERS = "ify_carriers";

    public final static String HTTP_OK = "IFY_OK";
    public final static String HTTP_ERR = "IFY_ERROR";
    public final static String ANSWER_MSG_OK = "MESSAGE_OK";
    public final static String ANSWER_GEO_OK = "LOCATION_OK";

    public final static String PARAM_TASK = "task";
    public final static String PARAM_STATUS = "status";

    public final static String BROADCAST_ACTION = "com.aistkem.currencyconverter";

    public final static int TASK_RECEIVED_MSG = 10;
    public final static int TASK_SENT_MSG = 20;
    public final static int TASK_RECEIVED_GEO = 30;
    public final static int TASK_START_LOCATION = 40;
    public final static int TASK_STOP_LOCATION = 41;

    public final static int STATUS_NONE = 0;				// Статус отсутствует
    public final static int STATUS_MSG_LOGIN = 1;			// Получен идентификатор пользователя
    public final static int STATUS_MSG_SPECIAL = 10;			// Все прочие специальные сообщения сервера
    public final static int STATUS_MSG = 20;				// Все прочие сообщения сервера
    public final static int STATUS_MSG_DICTIONARY = 30;		// Получена информация из удаленного словаря на сервере
    public final static int STATUS_MSG_REGISTRATION = 40;	// Анкета пользователя доставлена на сервер
    public final static int STATUS_MSG_USER_INFO = 50;		// Получена информация о пользователе
    public final static int STATUS_MSG_ROUTE = 60;			// Пришел маршрут с сервера
	public final static int STATUS_GEOCODER_DATA = 70;		// Пришли данные с сервиса геокодирования
	public final static int STATUS_MSG_ORDER = 80;			// Пришел ответ сервера на размещение-корректировку заказа
	public final static int STATUS_MSG_ORDERS = 90;			// Пришел список заказов по запросу клиента
	public final static int STATUS_MSG_ORDER_INFO = 100;	// Пришла информация о заказе по запросу клиента
	public final static int STATUS_MSG_CARRIERS = 110;		// Пришел список перевозчиков по запросу клиента
	
	public final static int GEOCODER_SUCCESS_RESULT = 1;
	public final static int GEOCODER_FAILURE_RESULT = 0;
	
	public final static short GEOCODER_PROVIDER_GOOGLE = 0;			// Провайдер геокодирования - Google
	public final static short GEOCODER_PROVIDER_GOOGLE_PLACES = 1;	// Провайдер геокодирования - Google Places
	public final static short GEOCODER_PROVIDER_OSM = 2;			// Провайдер геокодирования - OSM
	public final static short GEOCODER_PROVIDER_CUSTOM = 3;			// Провайдер геокодирования - наш
	
	public final static long DICTIONARY_WAIT_INTERVAL = 500;	// Интервал опроса базы при загрузке справочников
	public final static int DICTIONARY_WAIT_COUNT = 20; 		// Количество итераций опроса базы при загрузке справочников
	
	public final static long REGISTRATION_WAIT_INTERVAL = 500;	// Интервал опроса базы при загрузке анкеты пользователя
	public final static int REGISTRATION_WAIT_COUNT = 10; 		// Количество итераций опроса базы при загрузке анкеты пользователя
	
	// directory name to store captured images and videos
	public final static String IMAGE_DIRECTORY_NAME = "photos";
	public final static String THUMB_DIRECTORY_NAME = "thumbs";

	public final static int CAMERA_CYCLE_FULL = 0; // использовать полный цикл работы с изображением
	public final static int CAMERA_CYCLE_POST = 1; // только просмотр и подтверждение

	/* *** Настройки констант для геолокации *** */

	public final static String PREF_USE_GPS = "use_gps";
	public final static String PREF_USE_NET = "use_net";
	public final static String PREF_USE_GOOGLE = "use_google";

	public final static String PREF_TIME_GPS = "time_gps";
	public final static String PREF_DISTANCE_GPS = "distance_gps";
	public final static String PREF_TIME_NET = "time_net";
	public final static String PREF_DISTANCE_NET = "distance_net";
	public final static String PREF_TIME_GOOGLE = "time_google";
	public final static String PREF_DISTANCE_GOOGLE = "distance_google";
	
	public final static int GPS_MIN_TIME = 10;
	public final static int NET_MIN_TIME = 20;
	public final static int GOOGLE_MIN_TIME = 10;
	public final static int GPS_MAX_TIME = 60;
	public final static int NET_MAX_TIME = 60;
	public final static int GOOGLE_MAX_TIME = 60;
	
	public final static int GPS_INIT_TIME = 10; // Не реже раза в 10 секунд
	public final static int NET_INIT_TIME = 30;
	public final static int GOOGLE_INIT_TIME = 10;
	public final static int GPS_INIT_DISTANCE = 150; // Не реже раза на 150 метров
	public final static int NET_INIT_DISTANCE = 50;
	public final static int GOOGLE_INIT_DISTANCE = 100;
	
	public final static int MIN_SATELLITES = 4;
	public final static int MAX_MEASUREMENTS = 0; // максимальный индекс повторных измерений координаты (начиная с 0)
	public final static float MAX_SPEED = 56f; // максимальная отслеживаемая скорость перемещения объекта, м/с (200 км/ч)
	public final static float MAX_ACCELERATION = 100f * 1000 / 3600 / 8; // максимальная отслеживаемое ускорение объекта, м/с/с
	
	public final static float GPS_ACCURACY_MAX = 100;		// максимальная величина погрешности определения координаты GPS и Network
	public final static float GOOGLE_ACCURACY_MAX = 900;	// максимальная величина погрешности определения координаты Google

	public final static String DEFAULT_LOGIN = "+7"; // логин пользователя по умолчанию
	
	/* *** Статусы заказа *** */
	
	public final static int ORDER_STATUS_CREATE = 0;	// Заказ создан в локальной БД
	public final static int ORDER_STATUS_SENT = 1;		// Заказ отправлен на сервер, но подтверждения о размещении еще не пришло
	public final static int ORDER_STATUS_PLACED = 2;	// Заказ успешно размещен-откорректирован на сервере
	
	/* *** Статусы клиентов и перевозчиков *** */
	
	public final static short CARRIER_STATUS_OFFLINE = 0;	// Перевозчик не на линии
	public final static short CARRIER_STATUS_READY = 1;		// Перевозчик готов принять заказ
	
	public final static long ORDER_SEND_TIMEOUT = 1000 * 30;	// Таймаут возможности повторной отправки заказа на сервер ISmartTaxi (30 секунд)
	
	/* *** Константы циклов обновления информации в БД приложения *** */
	
	public final static int SOMETHING_MIN_SLEEP_TIME = 5;	//квант времени между сессиями отправки запроса о чем-либо, сек.
	public final static int	SOMETHING_MIN_SLEEP_COUNT = 1;	//минимальное число квантов времени между сессиями отправки запроса о чем-либо
	public final static int SOMETHING_MAX_SLEEP_COUNT = 60;	//максимальное число квантов времени между сессиями отправки запроса о чем-либо
	public final static int SOMETHING_MAX_FAULTS = 10;		//максимальное число неудачных итераций при отправке запроса о чем-либо

}
