package com.aistkem.currencyconverter.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.repository.TestMethods;
import com.aistkem.currencyconverter.R;

public class TestFragment extends PlaceholderFragment implements OnClickListener {

    public TestFragment() {
    	super();
    }

	public TestFragment(int taskNumber) {
		super(taskNumber);
	}

	public TestFragment(int taskNumber, String phTag) {
		super(taskNumber, phTag);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View rootView = inflater.inflate(R.layout.fragment_test, container, false);

		((Button) rootView.findViewById(R.id.btnTest0)).setOnClickListener(this);
		((Button) rootView.findViewById(R.id.btnTest1)).setOnClickListener(this);
		((Button) rootView.findViewById(R.id.btnTest2)).setOnClickListener(this);
		((Button) rootView.findViewById(R.id.btnTest3)).setOnClickListener(this);
		((Button) rootView.findViewById(R.id.btnTest4)).setOnClickListener(this);
    	
		((Button) rootView.findViewById(R.id.btnTest0)).setText("Сброс пользователя.\nЗапрос СМС-пароля для регистрации");
		((Button) rootView.findViewById(R.id.btnTest1)).setText("Передача СМС-пароля и анкеты при регистрации");
		((Button) rootView.findViewById(R.id.btnTest2)).setText("Запрос СМС-пароля для авторизации");
		((Button) rootView.findViewById(R.id.btnTest3)).setText("Передача откорректированной анкеты на сервер");
		((Button) rootView.findViewById(R.id.btnTest4)).setText("Получение анкеты с сервера");
		
    	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
    	super.onStart();
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnTest0:
				TestMethods.getInstance().SmsPasswordRegistrationRequest("+7 (905) 079-14-45", "кошка-матрешка");
			break;
			case R.id.btnTest1:
				TestMethods.getInstance().UserIdRequest("1234");
			break;
			case R.id.btnTest2:
				TestMethods.getInstance().SmsPasswordRequest("+7 (905) 079-14-45", "кошка-поварешка");
			break;
			case R.id.btnTest3:
				TestMethods.getInstance().SendUserInfo();
			break;
			case R.id.btnTest4:
				TestMethods.getInstance().UserInfoRequest();
			break;
		}
	}

    @Override
	public void RefreshView(Object... values) {

	}

}
