package com.aistkem.currencyconverter.fragments;

import java.io.Serializable;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;

import com.aistkem.currencyconverter.Classes.Currency;
import com.aistkem.currencyconverter.Classes.CurrencySearch;
import com.aistkem.currencyconverter.Classes.ListSearch;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.R;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.fragments.core.TemplateListDialogFragment;
import com.aistkem.currencyconverter.interfaces.ConverterListener;
import com.aistkem.currencyconverter.interfaces.CurrencyListListener;
import com.aistkem.currencyconverter.repository.CommonMethods;

public class MainFragment extends PlaceholderFragment implements OnClickListener {

	private ConverterListener converterListener;

	private ImageButton buttonFrom;
	private ImageButton buttonTo;
	private ImageButton buttonConvert;

    public MainFragment() {
    	super();
    }

	public MainFragment(int taskNumber) {
		super(taskNumber);
	}

	public MainFragment(int taskNumber, String phTag) {
		super(taskNumber, phTag);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
		try {
			converterListener = (ConverterListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ConverterListener");
		}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View rootView = null;

		rootView = inflater.inflate(R.layout.fragment_main, container, false);
		buttonFrom = (ImageButton) rootView.findViewById(R.id.btnSelectCurrencyFrom);
		buttonFrom.setOnClickListener(this);
		buttonTo = (ImageButton) rootView.findViewById(R.id.btnSelectCurrencyTo);
		buttonTo.setOnClickListener(this);
		buttonConvert = (ImageButton) rootView.findViewById(R.id.btnCurrencyConvert);
		buttonConvert.setOnClickListener(this);
		
		((EditText) rootView.findViewById(R.id.editCurrencyValue)).addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0 || !CommonMethods.isNumber(s.toString()) || Float.parseFloat(s.toString()) < 0.001f)
					((TableLayout) getView().findViewById(R.id.tlayoutResult)).setVisibility(View.INVISIBLE);
				else converterListener.ConvertCurrencyEvent(Float.parseFloat(s.toString()));
			}

			@Override    
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override    
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

    	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RefreshView();
    }

    @Override
    public void onStart() {
    	super.onStart();

    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    }

	@Override
	public void OnBindService() {

	}

	@Override
    public void RefreshView(Object... values) {
		int refreshViewTask = values.length > 0 ? (int)values[0] : Constants.FRAGMENT_REFRESH_TASK_DEFAULT;
		switch (refreshViewTask) {
			case Constants.FRAGMENT_REFRESH_TASK_DEFAULT:
			break;
			case Constants.FRAGMENT_REFRESH_TASK_CURRENCY:
				((TableLayout) this.getView().findViewById(R.id.tlayoutResult)).setVisibility(View.INVISIBLE);
				((TextView) this.getView().findViewById(R.id.tvCurrencyInfo)).setVisibility(View.VISIBLE);
				String currencyInfo = "";
				if (values[1] != null) {
					Currency currency = (Currency) values[1];
					((EditText) this.getView().findViewById(R.id.editCurrencyFrom)).setText(currency.nominal + " " + currency.name);
					currencyInfo += currency.charcode + "=" + currency.value + " (" + application.readableDateFormat.format(currency.date) + " UTC)" + "\n";
				}
				if (values[2] != null) {
					Currency currency = (Currency) values[2];
					((EditText) this.getView().findViewById(R.id.editCurrencyTo)).setText(currency.nominal + " " + currency.name);
					currencyInfo += currency.charcode + "=" + currency.value + " (" + application.readableDateFormat.format(currency.date) + " UTC)" + "\n";
				}
				((TextView) this.getView().findViewById(R.id.tvCurrencyInfo)).setText(currencyInfo);
			break;
			case Constants.FRAGMENT_REFRESH_TASK_RESULT:
				((TableLayout) this.getView().findViewById(R.id.tlayoutResult)).setVisibility(View.VISIBLE);
				((EditText) this.getView().findViewById(R.id.editCurrencyResult)).setText(String.format("%.2f",(Float)values[1]));
			break;
		}
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnSelectCurrencyFrom:
				converterListener.ShowCurrencyListEvent(this, Constants.APPLICATION_TASK_FROM);
			break;
			case R.id.btnSelectCurrencyTo:
				converterListener.ShowCurrencyListEvent(this, Constants.APPLICATION_TASK_TO);
			break;
			case R.id.btnCurrencyConvert:
				String value = ((EditText) this.getView().findViewById(R.id.editCurrencyValue)).getText().toString();
				if (!value.isEmpty()) converterListener.ConvertCurrencyEvent(Float.parseFloat(value));
			break;
		}
	}

	// EVV: Решил не использовать, а реализовать через интерфейс в активити.
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data == null) return;
	    if (requestCode == TemplateListDialogFragment.REQUEST_CODE) {
	        Currency currency = (Currency) data.getSerializableExtra(TemplateListDialogFragment.SELECTION_BUNDLE_KEY);
	        //long id = data.getLongExtra(TemplateListDialogFragment.LOCALID_BUNDLE_KEY, -1);
	    	//String str = data.getStringExtra(TemplateListDialogFragment.SELECTION_BUNDLE_KEY); // EVV: Для тестирования.
	        Log.d(LOG_TAG, "onActivityResult(): " + currency.toString());
	    }
	}

	/**
	 * Класс фрагмента-диалога выбора валюты из списка
	 * @author Владимир
	 *
	 */
	public static class CurrencyDialogFragment extends TemplateListDialogFragment {

		private CurrencyListListener currencyListListener;

		public static CurrencyDialogFragment newInstance(ListSearch listSearch, int loader) {
			CurrencyDialogFragment dialogFragment = new CurrencyDialogFragment();

			Bundle args = new Bundle();
			args.putSerializable(SEARCH_BUNDLE_KEY, listSearch);
			args.putInt(LOADER_BUNDLE_KEY, loader);
			dialogFragment.setArguments(args);
			return dialogFragment;
		}

		@Override
		protected void init() {
			LIST_LAYOUT = R.layout.fragment_dialog_list;
			LIST_VIEW = R.id.listViewItems;
			//LIST_ITEM = R.layout.container_listitem;
			LIST_ITEM = R.layout.simple_dropdown_item_2line;
			REQUEST_IDENTIFIER = Constants.REQUEST_SOMETHING;
			SERVER_IDENTIFIER = 0;
			CURSOR_IDENTIFIER_POSITION = 0;
			// Cтолбцы сопоставления для адаптера:
			ADAPTER_FROM = new String[] { "charcode", "full_name" };
			//ADAPTER_TO = new int[] { R.id.textListItemName, R.id.textListItemValue };
			ADAPTER_TO = new int[] { R.id.text1, R.id.text2 };
	    }

	    @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
			try {
				currencyListListener = (CurrencyListListener) activity;
			} catch (ClassCastException e) {
				throw new ClassCastException(activity.toString() + " must implement CurrencyListListener");
			}
	    }

	    @Override
	    public void onDismiss(DialogInterface dialog) {
	    	super.onDismiss(dialog);
	    	currencyListListener.CancelRemoteRequestEvent();
	    }

	    @Override
	    public void onCancel(DialogInterface dialog) {
	    	super.onCancel(dialog);
	    	currencyListListener.CancelRemoteRequestEvent();
	    }

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Cursor cur = cursorAdapter.getCursor();
			cur.moveToPosition(position);
			currentIdentifier = cur.getString(CURSOR_IDENTIFIER_POSITION);
			currencyListListener.SelectCurrencyEvent((Currency) getListItem(cur), GetTaskIdentifier());
			this.dismiss();
		}

		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
			return new CurrencyCursorLoader(this);
		}

	    @Override
	    protected boolean getRemoteData(Object... values) {
        	imageViewSearch.setVisibility(View.INVISIBLE);
        	progressBarSearch.setVisibility(View.VISIBLE);
	    	//return super.getRemoteData(values); // EVV: Для тестирования
	    	/* EVV: Начальная запись и смещение здесь не используются. Используется сервис исполнения команд CoreCommandExecutorService.
	    	listSearch.SetStart((String)values[0]);
	    	listSearch.SetOffset((int)values[1]);
	    	CurrencyMethods.getInstance().GetRemoteRates((CurrencySearch)listSearch);
	    	*/
	    	currencyListListener.StartRemoteRequestEvent();
	        return true;
	    }

		@Override
		protected Serializable getListItem(Cursor cur) {
			Currency currency = new Currency();
			currency.id = cur.getLong(0);
			currency.numcode = cur.getInt(1);
			currency.charcode = cur.getString(2);
			currency.nominal = cur.getInt(3);
			currency.name = cur.getString(4);
			currency.value = cur.getFloat(5);
			currency.date = cur.getLong(6);
			currency.usage = cur.getShort(7);
			return currency;
		}

		@Override
		protected void setSearchConstraint(String str) {
			str = str.isEmpty() ? null : str;
			((CurrencySearch)listSearch).name = str;
			((CurrencySearch)listSearch).charcode = str;
		}

		@Override
		public void showProgress(int progress) {
			if (progress < 100) {
	        	imageViewSearch.setVisibility(View.INVISIBLE);
	        	progressBarSearch.setVisibility(View.VISIBLE);
			}
		}

		private static class CurrencyCursorLoader extends CursorLoaderTemplate {

			public CurrencyCursorLoader(TemplateListDialogFragment fragment) {
				super(fragment);
			}

			@Override
			public Cursor loadInBackground() {
				return super.loadInBackground();
			}

			@Override
			protected String getSql() {
				return
					"SELECT _id, numcode, charcode, nominal, name, value, date, usage, nominal || ' ' || name full_name"
					+ " FROM currency WHERE 1=1 " + CurrencySearch.getWhere((CurrencySearch) ((CurrencyDialogFragment) dialogFragment).GetListSearch())
					+ " ORDER BY " + CurrencySearch.getOrder((CurrencySearch) ((CurrencyDialogFragment) dialogFragment).GetListSearch());
			}

		}

	}
	
}
