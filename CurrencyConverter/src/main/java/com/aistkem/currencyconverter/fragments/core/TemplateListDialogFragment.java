package com.aistkem.currencyconverter.fragments.core;

import java.io.Serializable;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.aistkem.currencyconverter.Classes.ListSearch;
import com.aistkem.currencyconverter.Classes.RemoteDictionarySearch;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.R;
import com.aistkem.currencyconverter.activities.core.CoreActivity;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;

/**
 * Базовый класс для фрагментов-диалогов со списками, динамически подгружаемыми с сервера.
 * @author Владимир
 *
 */
public abstract class TemplateListDialogFragment extends DialogFragment implements LoaderCallbacks<Cursor>, OnItemClickListener {

	public static final int REQUEST_CODE = 19780325;
	/**
	 * Ключ для хранения выбранного объекта listitem
	 */
	public static final String SELECTION_BUNDLE_KEY = "selection";
	/**
	 * Ключ для хранения объекта поиска по БД
	 */
	public static final String SEARCH_BUNDLE_KEY = "search";
	/**
	 * Ключ для хранения идентификатора записи в локальной БД
	 */
	public static final String LOCALID_BUNDLE_KEY = "localid";
	/**
	 * Ключ для хранения идентификатора записи в удаленной БД
	 */
	public static final String REMOTEID_BUNDLE_KEY = "remoteid";
	/**
	 * Ключ для хранения идентификатора loader'a
	 */
	public static final String LOADER_BUNDLE_KEY = "loader";
	
	private static final int SEARCH_TEXT_CHANGED = 100;
	
	protected final String LOG_TAG = this.getClass().getName();
	
	protected CoreActivity activity;
    protected SimpleCursorAdapter cursorAdapter;
    protected ListView listView;
    protected boolean listViewScrollDown = false;
    /**
     * Текущее значение идентификатора (берется из активной строки выборки, например)
     */
    protected String currentIdentifier = "";
    /**
     * Текущая строка для поиска в БД
     */
    protected String currentConstraint = "";
	/**
	 * Объект критериев поиска в БД
	 */
	protected ListSearch listSearch;

	// Переменные для метода init():
    /**
     * Описание фрагмента-контейнера для списка
     */
	protected int LIST_LAYOUT;
	/**
	 * Список-ListView
	 */
	protected int LIST_VIEW;
	/**
	 * Строка-layout списка
	 */
	protected int LIST_ITEM;
	/**
	 * Строка-идентификатор запроса к серверу
	 */
	protected String REQUEST_IDENTIFIER;
	/**
	 * Идентификатор сервера данных
	 */
	protected int SERVER_IDENTIFIER;
	/**
	 * Позиция поля-идентификатора в выборке
	 */
	protected int CURSOR_IDENTIFIER_POSITION;
	/**
	 * Параметр конструктора адаптера from
	 */
	protected String[] ADAPTER_FROM;
	/**
	 * Параметр конструктора адаптера to
	 */
	protected int[] ADAPTER_TO;
	/**
	 * Заголовок окна диалога
	 */
	protected String TITLE;
	
    private int mLoader;
    
	protected EditText editTextSearch;
	protected ProgressBar progressBarSearch;
	protected ImageView imageViewSearch;
	
	private boolean mIsRestored = false;
	
	public TemplateListDialogFragment() {
		super();
		init();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO: Разобраться с сохранением-восстановлением параметров диалога!
        if (savedInstanceState != null) {
        	listSearch = (ListSearch) savedInstanceState.getSerializable(SEARCH_BUNDLE_KEY);
        }
        listSearch = (ListSearch) getArguments().getSerializable(SEARCH_BUNDLE_KEY);
		mLoader = getArguments().getInt(LOADER_BUNDLE_KEY);
		setRetainInstance(true);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (CoreActivity) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	View rootView = activity.getLayoutInflater().inflate(LIST_LAYOUT, null);
    	
    	listView = (ListView) rootView.findViewById(LIST_VIEW);
        //listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    	//listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
    	
    	cursorAdapter = new SimpleCursorAdapter(activity, LIST_ITEM, null, ADAPTER_FROM, ADAPTER_TO, 0);
        listView.setAdapter(cursorAdapter);
        listView.setOnItemClickListener(this);
    	
        // Создаем лоадер для чтения данных:
    	activity.getSupportLoaderManager().initLoader(mLoader, null, this);
    	
    	AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
    	if (TITLE != null) dialogBuilder.setTitle(TITLE);
    	dialogBuilder.setView(rootView);
    	
    	//dialogBuilder.setSingleChoiceItems(cursorAdapter, -1, this);	// Вывод и выбор единственного элемента в списке
    	//dialogBuilder.setAdapter(cursorAdapter, this);				// Вывод списка элементов
    	/* EVV: Не понадобился.
    	cursorAdapter.registerDataSetObserver(new DataSetObserver() {
    		@Override
    		public void onChanged() {
    			super.onChanged();
    			activity.getSupportLoaderManager().getLoader(mLoader).forceLoad();
    		}
    		@Override
    		public void onInvalidated() {
    			super.onInvalidated();
    		}
    	});
    	*/
        /** Click listener for the OK button of the dialog window */
    	/* EVV: Пока никак не используем
    	DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if (listView.getCheckedItemPosition() > -1) {
	            	Intent intent = new Intent();
	            	Cursor c = cursorAdapter.getCursor();
	            	c.moveToPosition(listView.getCheckedItemPosition());
	            	intent.putExtra(SELECTION_BUNDLE_KEY, c.getString(1));
	            	intent.putExtra(REMOTEID_BUNDLE_KEY, c.getString(0));
	            	intent.putExtra(DICTIONARY_BUNDLE_KEY, mDictionary);
	                getTargetFragment().onActivityResult(getTargetRequestCode(), REQUEST_CODE, intent);
            	}
            }
        };
        
    	dialogBuilder.setNegativeButton(getString(R.string.btnCancel), null);
        dialogBuilder.setPositiveButton(getString(R.string.btnOk), clickListener);
        */
    	
    	listView.setOnScrollListener(new OnScrollListener() {
    		private int mLastInScreen = 0;
    		private int mFirstInScreen = 0;
    		
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            	if (scrollState != 0) return;
        		if (mLastInScreen == view.getCount() && !listViewScrollDown) {
        			// Достигнут конец списка:
	            	Cursor c = cursorAdapter.getCursor();
	            	c.moveToPosition(listView.getCount() - 1);
	            	currentIdentifier = c.getString(CURSOR_IDENTIFIER_POSITION);
	            	/*if (c.getInt(1) > 0)*/ getRemoteData(currentIdentifier, listSearch.GetOffset());
        		} else if (mFirstInScreen == 0 && listViewScrollDown) {
        			// Достигнуто начало списка:
        			Cursor c = cursorAdapter.getCursor();
	            	c.moveToPosition(0);
	            	/*if (c.getInt(1) > 0)*/ currentIdentifier = c.getString(CURSOR_IDENTIFIER_POSITION);
	            	/*else currentIdentifier = "";*/
        			getRemoteData(currentIdentifier, -listSearch.GetOffset());
        		}
            }
            
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            	mFirstInScreen = firstVisibleItem;
            	mLastInScreen = firstVisibleItem + visibleItemCount;

            }
    	});

    	listView.setOnTouchListener(new OnTouchListener() {
    		private float mInitialY;

    		@Override
    		public boolean onTouch(View v, MotionEvent event) {
    			switch (event.getAction()) {
    				case MotionEvent.ACTION_DOWN:
    					mInitialY = event.getY();
    				break;
    				case MotionEvent.ACTION_MOVE:
    					final float yDiff = event.getY() - mInitialY;
    					if (yDiff > 0.0) {
    						listViewScrollDown = true;
                        } else if (yDiff < 0.0) {
                        	listViewScrollDown = false;
                        }
    				break;
    			}
    			return false;
    		}
    	});
    	
    	imageViewSearch = (ImageView) rootView.findViewById(R.id.ivSearch);
    	imageViewSearch.setVisibility(View.VISIBLE);
    	progressBarSearch = (ProgressBar) rootView.findViewById(R.id.progressBarSearch);
    	progressBarSearch.setVisibility(View.INVISIBLE);
    	editTextSearch = (EditText) rootView.findViewById(R.id.editSearch);
		TextWatcherSearch inputTextWatcher = new TextWatcherSearch();
		editTextSearch.addTextChangedListener(inputTextWatcher);
		
		mIsRestored = (mIsRestored || savedInstanceState != null);
    	
        return dialogBuilder.create();
    }

	@Override
	public void onStart() {
		super.onStart();
		if (!mIsRestored) getRemoteData(currentIdentifier, listSearch.GetOffset());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (listSearch != null) outState.putSerializable(SEARCH_BUNDLE_KEY, listSearch);
	}

    @Override
    public void onDestroy() {
    	activity.getSupportLoaderManager().destroyLoader(mLoader);
    	super.onDestroy();
    }

    @Override
    public void onDestroyView() {
      if (getDialog() != null && getRetainInstance())
        getDialog().setOnDismissListener(null);
      	//getDialog().setDismissMessage(null);
      super.onDestroyView();
    }

/* EVV: Для DialogInterface.OnClickListener
	@Override
	public void onClick(DialogInterface dialog, int which) {
		listView.setItemChecked(which, true);
		dialog.dismiss();
	}
*/

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Cursor cur = cursorAdapter.getCursor();
		cur.moveToPosition(position);
		currentIdentifier = cur.getString(CURSOR_IDENTIFIER_POSITION);
    	Intent intent = new Intent();
    	if (!(listSearch instanceof RemoteDictionarySearch)) {
    		intent.putExtra(LOCALID_BUNDLE_KEY, currentIdentifier);
    		intent.putExtra(SELECTION_BUNDLE_KEY, getListItem(cur));
    	} else {
	    	intent.putExtra(SELECTION_BUNDLE_KEY, cur.getString(2));
	    	intent.putExtra(REMOTEID_BUNDLE_KEY, cur.getLong(1));
	    	intent.putExtra(LOCALID_BUNDLE_KEY, cur.getLong(0));
    	}
        getTargetFragment().onActivityResult(getTargetRequestCode(), REQUEST_CODE, intent);
        this.dismiss();
	}

	@Override
	public abstract Loader<Cursor> onCreateLoader(int id, Bundle bndl);

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
    	cursorAdapter.swapCursor(cursor);
    	if (!currentIdentifier.isEmpty()) {
    		listView.setSelection(getCurrentId());
    	}
    	else listView.setSelection(0 /*cursorAdapter.getCount() - 1*/);
    	imageViewSearch.setVisibility(View.VISIBLE);
    	progressBarSearch.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    	cursorAdapter.swapCursor(null);
    }

	protected abstract void init();

	protected int getCurrentId() {
		int ii = 0;
		Cursor c = cursorAdapter.getCursor();
		if (c.moveToFirst()) do {
			if (currentIdentifier.equals(c.getString(CURSOR_IDENTIFIER_POSITION))) {
				break;
			}
			ii++;
		} while (c.moveToNext());
		if (c.getCount() > ii) return ii;
		else return 0;
	}

	public String getCurrentIdentifier() {
		return currentIdentifier;
	}

	/**
	 * Универсальный обработчик для подгрузки данных из удаленной БД на сервере.
	 * @param values
	 * @return
	 */
	protected boolean getRemoteData(Object... values) {
    	imageViewSearch.setVisibility(View.INVISIBLE);
    	progressBarSearch.setVisibility(View.VISIBLE);
		if (!(listSearch instanceof RemoteDictionarySearch)) {
			String request = REQUEST_IDENTIFIER + "||" + values[0].toString() + "||" + values[1].toString();
			ArrayList<String> guidArray = new ArrayList<String>();
			if (activity.GetApplication().IsBound())
				activity.GetApplication().GetService().SendMessage(SERVER_IDENTIFIER, guidArray, request);
			else
				activity.GetApplication().GetDbHelper().MessageToQueue(activity.GetApplication().GetDb(), SERVER_IDENTIFIER, guidArray, request);
			if (guidArray.get(0) != null) {
				return true;
			}
			return false;
		} else {
			return CommonCoreMethods.getInstance().GetRemoteDictionary(((RemoteDictionarySearch)listSearch).dictionary, ((RemoteDictionarySearch)listSearch).searchPrefix, ((RemoteDictionarySearch)listSearch).searchConstraint);
		}
	}

	/**
	 * Возвращает идентификатор задачи для диалога выбора из списка
	 * @return Идентификатор задачи диалога (mLoader - по умолчанию)
	 */
	public int GetTaskIdentifier() {
		return mLoader;
	}

	/**
	 * Возвращает значение объекта критериев поиска в БД для этого фрагмента
	 * @return Объект критериев поиска в базе данных
	 */
	public ListSearch GetListSearch() {
		return listSearch;
	}

	/**
	 * Устанавливает значение объекта критериев поиска в БД для этого фрагмента
	 * @param listSearch - объект критериев поиска в базе данных
	 */
	public void SetListSearch(ListSearch listSearch) {
		this.listSearch = listSearch;
	}

	/**
	 * Устанавливает заголовок диалога
	 * @param title
	 */
	public void setTitle(String title) {
		TITLE = title;
	}

	/**
	 * Получает объект данных для строки списка
	 * @return
	 */
	protected abstract Serializable getListItem(Cursor cur);

	/**
	 * Устанавливает значение строки поиска в локальной БД
	 * @return
	 */
	protected abstract void setSearchConstraint(String str);

	/**
	 * Отображает степень выполнения задачи обновления информации для списка
	 * @param progress
	 */
	public abstract void showProgress(int progress);

	/**
	 * Базовый класс CursorLoader'а
	 * @author Владимир
	 *
	 */
	protected abstract static class CursorLoaderTemplate extends CursorLoader {

		protected final String LOG_TAG = this.getClass().getName();
		
		protected TemplateListDialogFragment dialogFragment;
		protected ListSearch listSearch;

		public CursorLoaderTemplate(TemplateListDialogFragment fragment) {
			super(fragment.activity);
			dialogFragment = fragment;
			listSearch = dialogFragment.GetListSearch();
		}

		@Override
		public Cursor loadInBackground() {
			if (!(listSearch instanceof RemoteDictionarySearch))
				return readData();
			else
				return readDictionaryData();
		}

		protected Cursor readData() {
			String sql = getSql();
			Cursor cur = dialogFragment.activity.GetApplication().GetDb().rawQuery(sql, null);
			return cur;
		}

		protected abstract String getSql();

		protected Cursor readDictionaryData() {
			RemoteDictionarySearch listSearch = (RemoteDictionarySearch) this.listSearch;
			
	    	String sql = "dictionary = '" + listSearch.dictionary + "'";
	    	if (listSearch.searchPrefix != null) {
	    		sql += " AND search_prefix = '" + listSearch.searchPrefix + "'";
	    	}
	    	if (!listSearch.searchConstraint.isEmpty()) {
	    		sql += " AND (name like '%" + listSearch.searchConstraint + "%'" + " OR name_rus like '%" + listSearch.searchConstraint.toLowerCase() + "%')";
	    	}
        	sql = "SELECT _id, remoteid, name, remark FROM remote_dictionaries "
        		+ "WHERE " + sql + " ORDER BY name ASC, remoteid ASC";
        	Cursor cur = dialogFragment.activity.GetApplication().GetDb().rawQuery(sql, null);
            if (cur != null) {
            	cur.moveToFirst();
            }
            return cur;
        }
	}

	private class TextWatcherSearch implements TextWatcher {

		private boolean afterTextChangedEnable = true;

		private final Handler mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
	        	imageViewSearch.setVisibility(View.INVISIBLE);
	        	progressBarSearch.setVisibility(View.VISIBLE);
	        	if (!(listSearch instanceof RemoteDictionarySearch)) {
	        		setSearchConstraint((String) msg.obj);
	        	} else {
	        		((RemoteDictionarySearch)listSearch).searchConstraint = (String) msg.obj;
	        	}
	        	activity.getSupportLoaderManager().getLoader(mLoader).forceLoad();
			}
		};

		public TextWatcherSearch() {
			super();
		}

		@Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	    }

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count) {

	    }

	    @Override
	    public void afterTextChanged(Editable s) {
	    	/* EVV: Используется, если менять текст вручную:
	    	if (!afterTextChangedEnable) {
	    		afterTextChangedEnable = true;
	    		return;
	    	}
			*/
	        String str = s.toString();
	        // EVV: Разделил логику для пустого и не пустого стакана на будущее:
	        if (str.length() > 0) {	// Если имеется строка для поиска, то создаем задачу поиска записи в БД:
	            mHandler.removeMessages(SEARCH_TEXT_CHANGED);
	            mHandler.sendMessageDelayed(mHandler.obtainMessage(SEARCH_TEXT_CHANGED, str), Constants.LIST_SEARCH_TIMEOUT);
	        } else  {	// Если строка для поиска отсутствует, то также создаем задачу поиска записи в БД:
	        	mHandler.removeMessages(SEARCH_TEXT_CHANGED);
	        	mHandler.sendMessageDelayed(mHandler.obtainMessage(SEARCH_TEXT_CHANGED, str), Constants.LIST_SEARCH_TIMEOUT);
	        }
	        /* EVV: Не пригодилось пока...
	        afterTextChangedEnable = false;
            editTextSearch.setText(str);
            editTextSearch.setSelection(str.length());
            */
	    }
    }

}
