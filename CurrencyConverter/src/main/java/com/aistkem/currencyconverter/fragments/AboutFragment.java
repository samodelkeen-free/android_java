package com.aistkem.currencyconverter.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.R;

public class AboutFragment extends PlaceholderFragment implements OnClickListener {

    public AboutFragment() {
    	super();
    }

	public AboutFragment(int taskNumber) {
		super(taskNumber);
	}

	public AboutFragment(int taskNumber, String phTag) {
		super(taskNumber, phTag);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View rootView = inflater.inflate(R.layout.fragment_about, container, false);

    	((TextView) rootView.findViewById(R.id.tvSqliteVersion)).setText(getString(R.string.tvSqliteVersion, application.GetDbHelper().GetSqliteVersion(application.GetDb())));
    	
    	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
    	super.onStart();
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.ivTool0) {
		}
	}

    @Override
	public void RefreshView(Object... values) {

	}

}
