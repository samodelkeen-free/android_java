package com.aistkem.currencyconverter.fragments.core;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.activities.core.CoreActivity;

public class PlaceholderFragment extends Fragment {

	public static class PlaceholderFragmentData {
    	public int containerId;
    	public PlaceholderFragment fragment;

		public PlaceholderFragmentData(int containerId, PlaceholderFragment fragment) {
			this.containerId = containerId;
			this.fragment = fragment;
		}

		public PlaceholderFragmentData(int containerId, PlaceholderFragment fragment, String tag) {
			this.containerId = containerId;
			this.fragment = fragment;
			this.fragment.SetTag(tag);
		}

		public static boolean containsFragment(List<PlaceholderFragmentData> list, PlaceholderFragment fragment) {
			if (!list.isEmpty())
				for(PlaceholderFragmentData item : list) {
					if(item != null && item.fragment.equals(fragment)) {
						return true;
					}
				}
			return false;
		}

		public static PlaceholderFragmentData getFragmentData(List<PlaceholderFragmentData> list, PlaceholderFragment fragment) {
			if (!list.isEmpty())
				for(PlaceholderFragmentData item : list) {
					if(item != null && item.fragment.equals(fragment)) {
						return item;
					}
				}
			return null;
		}

	}

	public static class ReplaceFragmentData {
    	public int containerId;
    	public PlaceholderFragment oldFragment;
    	public PlaceholderFragment newFragment;
    	public boolean toStack;
    	public int requestCode;

    	/**
    	 * Конструктор объекта параметров заменяемых фрагментов
    	 * @param containerId - контейнер
    	 * @param oldFragment - ссылка на старый фрагмент
    	 * @param newFragment - ссылка на новый фрагмент
    	 * @param toStack - отметка о помещении транзакции в стек
    	 * @param requestCode - код возврата данных родительскому фрагменту
    	 */
		public ReplaceFragmentData(int containerId, PlaceholderFragment oldFragment, PlaceholderFragment newFragment, boolean toStack, int requestCode) {
			this.containerId = containerId;
			this.oldFragment = oldFragment;
			this.newFragment = newFragment;
			this.toStack = toStack;
			this.requestCode = requestCode;
		}

	}

    //protected static final String ARG_SECTION_NUMBER = "section_number";

	protected final String LOG_TAG = this.getClass().getName();
	protected int taskNumber = 0;
    protected String phTag = this.getClass().getName();
    protected CoreApplication application;
    protected CoreActivity activity;
    //protected SharedPreferences sharedPreferences;

    public PlaceholderFragment() {}

    public PlaceholderFragment(int taskNumber) {
    	this.taskNumber = taskNumber;
        Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, taskNumber);
        setArguments(args);
    }

	public PlaceholderFragment(String phTag) {
		this.phTag = phTag;
	}

	public PlaceholderFragment(int taskNumber, String phTag) {
		this.taskNumber = taskNumber;
		this.phTag = phTag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
	        taskNumber = savedInstanceState.getInt("taskNumber");
	        phTag = savedInstanceState.getString("phTag");
        }
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    	View rootView = null;
    	/*
        if (!PlaceholderFragmentData.containsFragment(this.activity.activeCenterFragments, this)) {
        	this.activity.activeCenterFragments.add( new PlaceholderFragmentData(-1, this) );
        }
    	*/
    	return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (CoreActivity) activity;
        this.application = (CoreApplication) activity.getApplication();
        if (!PlaceholderFragmentData.containsFragment(this.activity.activeCenterFragments, this)) {
        	this.activity.activeCenterFragments.add( new PlaceholderFragmentData(-1, this) );
        }
    }

	@Override
	public void onStop() {
		super.onStop();
	}

    @Override
    public void onDetach() {
        super.onDetach();
        activity.activeCenterFragments.remove(PlaceholderFragmentData.getFragmentData(activity.activeCenterFragments, this));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
	        //taskNumber = savedInstanceState.getInt("taskNumber");
	        //phTag = savedInstanceState.getString("phTag");
        }
        //this.activity.updateTitle(getArguments().getInt(ARG_SECTION_NUMBER));
        this.activity.updateTitle(taskNumber);
        //if (getArguments().getInt(ARG_SECTION_NUMBER) == SECTION_MAIN) RefreshView();
    }

	@Override
	public void onStart() {
		super.onStart();
        if (!PlaceholderFragmentData.containsFragment(this.activity.activeCenterFragments, this)) {
        	this.activity.activeCenterFragments.add( new PlaceholderFragmentData(-1, this) );
        }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("taskNumber", taskNumber);
		outState.putString("phTag", phTag);
	}

	public void RefreshView(Object... values) {
		//for(Object c : values){}
	}

    public void ResizeView(View view) {

    }
    
    public void OnBindService() {

    }
    
    public void OnUnBindService() {

    }
    
    public void Close() {

    }

    /**
     * Возвращает код текущей задачи фрагмента
     * @return Код задачи фрагмента
     */
	public int GetTaskNumber() {
		return taskNumber;
	}

	public void SetTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String GetTag() {
		return phTag;
	}

	public void SetTag(String tag) {
		phTag = tag;
	}

}
