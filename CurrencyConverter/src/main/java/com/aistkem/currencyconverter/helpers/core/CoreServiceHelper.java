/*
 * Copyright (C) 2013 Alexander Osmanov (http://perfectear.educkapps.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.aistkem.currencyconverter.helpers.core;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.aistkem.currencyconverter.interfaces.core.CoreServiceCallbackListener;
import com.aistkem.currencyconverter.handlers.core.CoreServiceCommand;
import com.aistkem.currencyconverter.services.core.CoreCommandExecutorService;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.SparseArray;

public abstract class CoreServiceHelper {

    private ArrayList<CoreServiceCallbackListener> currentListeners = new ArrayList<CoreServiceCallbackListener>();

    private AtomicInteger idCounter = new AtomicInteger();

    private SparseArray<Intent> pendingActivities = new SparseArray<Intent>();

    protected Application application;

    public CoreServiceHelper(Application app) {
    	this.application = app;
    }

    public void addListener(CoreServiceCallbackListener currentListener) {
    	currentListeners.add(currentListener);
    }

    public void removeListener(CoreServiceCallbackListener currentListener) {
    	currentListeners.remove(currentListener);
    }

    public void cancelCommand(int requestId) {
		Intent i = new Intent(application, CoreCommandExecutorService.class);
		i.setAction(CoreCommandExecutorService.ACTION_CANCEL_COMMAND);
		i.putExtra(CoreCommandExecutorService.EXTRA_REQUEST_ID, requestId);
	
		application.startService(i);
		/* EVV: Исправил обработчик прерывания команды
		pendingActivities.remove(requestId);
		*/
    }

    public boolean isPending(int requestId) {
    	return pendingActivities.get(requestId) != null;
    }

    public boolean check(Intent intent, Class<? extends CoreServiceCommand> clazz) {
		Parcelable commandExtra = intent.getParcelableExtra(CoreCommandExecutorService.EXTRA_COMMAND);
		return commandExtra != null && commandExtra.getClass().equals(clazz);
    }

    protected int createId() {
    	return idCounter.getAndIncrement();
    }

    protected int runRequest(final int requestId, Intent i) {
		pendingActivities.append(requestId, i);
		application.startService(i);
		return requestId;
    }

    protected Intent createIntent(final Context context, CoreServiceCommand command, final int requestId) {
		Intent i = new Intent(context, CoreCommandExecutorService.class);
		i.setAction(CoreCommandExecutorService.ACTION_EXECUTE_COMMAND);
	
		i.putExtra(CoreCommandExecutorService.EXTRA_COMMAND, command);
		i.putExtra(CoreCommandExecutorService.EXTRA_REQUEST_ID, requestId);
		i.putExtra(CoreCommandExecutorService.EXTRA_STATUS_RECEIVER, new ResultReceiver(new Handler()) {
		    @Override
		    protected void onReceiveResult(int resultCode, Bundle resultData) {
			Intent originalIntent = pendingActivities.get(requestId);
			if (isPending(requestId)) {
			    if (resultCode != CoreServiceCommand.RESPONSE_PROGRESS) {
				pendingActivities.remove(requestId);
			    }
	
			    for (CoreServiceCallbackListener currentListener : currentListeners) {
				if (currentListener != null) {
				    currentListener.onServiceCallback(requestId, originalIntent, resultCode, resultData);
				}
			    }
			}
		    }
		});
	
		return i;
    }

}
