package com.aistkem.currencyconverter.helpers.core;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.aistkem.currencyconverter.Classes.RemoteDictionary;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;

public class CoreDbHelper extends SQLiteOpenHelper {
	
	protected final String LOG_TAG = this.getClass().getName();
	protected CoreApplication application;

    public CoreDbHelper(Context context, String name, CursorFactory factory, int version) {
      super(context, name, factory, version);
      application = (CoreApplication) context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    	Log.d(LOG_TAG, "*** onCreate database ***");

    	/**
    	 * Хранение произвольных настроек приложения. TODO: Возможно, стоит хранить все в SharedPreferences?
    	 */
    	db.execSQL("create table options ("
    	+ "_id integer primary key autoincrement,"
    	+ "name text unique not null,"
    	+ "value text null"
    	+ ");");
    	
    	/**
    	 * Хранение списков выбора для настроек приложения. TODO: Возможно, стоит хранить все в SharedPreferences?
    	 */
    	db.execSQL("create table listitems ("
    	+ "_id integer primary key autoincrement,"
    	+ "list_guid text not null,"
    	+ "list_item text not null,"
    	+ "item_name text not null,"
    	+ "item_guid text not null,"
    	+ "is_active integer not null default 1"
    	+ ");");
    	
    	/**
    	 * Информация о пользователе программы
    	 */
    	db.execSQL("create table userinfo ("
    	+ "_id integer primary key,"	// Идентификатор пользователя на сервере приложения
    	+ "family text not null,"
    	+ "name text not null,"
    	+ "patr text null,"
    	+ "keyword text null,"
    	+ "userid_guid text null,"
    	+ "pass_guid text null,"
    	+ "registration_guid text null"
    	+ ");");
    	
    	/**
    	 * Запросы к серверу/ответы на запросы
    	 */
    	db.execSQL("create table requests ("
    	+ "_id integer primary key autoincrement,"
    	+ "request text not null,"
    	+ "answer text null,"
    	+ "timemark integer not null,"
    	+ "userid integer not null,"
    	+ "guid text not null,"
    	+ "sent integer not null default 0,"	// Статус запроса к серверу (-1-зарезервирован к отправке, 0-на отправку, 1-отправлен, 2-ошибка отправки, 3-отправка отменена)
    	+ "server integer not null default 0,"	// Внутренний идентификатор сервера для обмена пакетами
    	+ "attempt integer not null default 0,"	// Текущее количество попыток связи с сервером
    	+ "FOREIGN KEY(userid) REFERENCES userinfo(_id) ON DELETE CASCADE"
    	+ ");");
    	
    	db.execSQL("CREATE INDEX oi_requests$guid ON requests(guid)");
    	
    	/**
    	 * Произвольные сообщения пользователя и сервера
    	 */
    	db.execSQL("create table messages ("
    	+ "_id integer primary key autoincrement,"
    	+ "message text not null,"
    	+ "direction integer not null,"
    	+ "timemark integer not null,"
    	+ "userid integer not null,"
    	+ "guid text not null,"
    	+ "sent integer not null default 0,"
    	+ "FOREIGN KEY(userid) REFERENCES userinfo(_id) ON DELETE CASCADE"
    	+ ");");
    	
    	db.execSQL("CREATE INDEX oi_messages$guid ON messages(guid)");
    	
    	/**
    	 * Файлы вложений к сообщениям пользователя
    	 */
    	db.execSQL("create table message_attachments ("
    	+ "_id integer primary key autoincrement,"
    	+ "messageid integer not null,"
    	+ "path text not null,"
    	+ "FOREIGN KEY(messageid) REFERENCES messages(_id) ON DELETE CASCADE"
    	+ ");");
    	
    	/**
    	 * Служебные сообщения системы (логи ошибок и прочее)
    	 */
    	db.execSQL("create table reports ("
    	+ "_id integer primary key autoincrement,"
    	+ "message text not null,"
    	+ "timemark integer not null,"
    	+ "userid integer not null,"
    	+ "guid text not null,"
    	+ "sent integer not null default 0,"
    	+ "FOREIGN KEY(userid) REFERENCES userinfo(_id) ON DELETE CASCADE"
    	+ ");");
    	
    	/**
    	 * Файлы вложений к сообщениям системы
    	 */
    	db.execSQL("create table report_attachments ("
    	+ "_id integer primary key autoincrement,"
    	+ "reportid integer not null,"
    	+ "type text not null,"		// Тип/описание файла вложения (если их несколько)
    	+ "path text not null,"
    	+ "FOREIGN KEY(reportid) REFERENCES reports(_id) ON DELETE CASCADE"
    	+ ");");
    	
    	/**
    	 * Локальное хранилище удаленных словарей
    	 */
    	db.execSQL("create table remote_dictionaries ("
    	+ "_id integer primary key autoincrement,"
    	+ "remoteid integer not null,"
    	+ "dictionary text not null,"
    	+ "search_prefix text null,"
    	+ "name text not null,"
    	+ "name_rus text null,"
    	+ "remark text null"
    	+ ");");
    	
    	db.execSQL("CREATE UNIQUE INDEX ui_remote_dictionaries$dictionary$remoteid ON remote_dictionaries(dictionary, remoteid)");
  	    
    	db.execSQL("insert into userinfo (_id, family, name) values ("
		+ "-1,"
		+ "'',"
		+ "''"
		+ ");");
    	
    	db.execSQL("insert into options (name, value) values ("
		+ "'userid',"
		+ "'-1'"
		+ ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    	db.execSQL("DROP TABLE IF EXISTS " + "options");
    	db.execSQL("DROP TABLE IF EXISTS " + "listitems");
    	db.execSQL("DROP TABLE IF EXISTS " + "remote_dictionaries");
    	db.execSQL("DROP TABLE IF EXISTS " + "requests");
    	db.execSQL("DROP TABLE IF EXISTS " + "message_attachments");
    	db.execSQL("DROP TABLE IF EXISTS " + "messages");
    	db.execSQL("DROP TABLE IF EXISTS " + "report_attachments");
    	db.execSQL("DROP TABLE IF EXISTS " + "reports");
    	db.execSQL("DROP TABLE IF EXISTS " + "userinfo");
    }

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		
		db.execSQL("PRAGMA foreign_keys=ON");
	}

    /* Устанавливает значение опции в таблице настроек */
    public boolean SetOptionValue(SQLiteDatabase db, String name, String value) {
    	boolean result = false;
		String[] columns = {"_id"};
		Cursor cur = db.query("options", columns, "name = '" + name + "'", null, null, null, null);
		if (cur.moveToFirst()) {
			int id = cur.getInt(0);
			ContentValues cv = new ContentValues();
			if (value != null) cv.put("value", value);
			else cv.putNull("value");
			long rowCount = db.update("options", cv, "_id = " + id, null);
			if (rowCount > 0) {
				result = true;
				Log.d(LOG_TAG, "Updated option: " + name + "=" + value);
			}
			else {
				Log.e(LOG_TAG, "Error when updating option: " + name);
			}
		} else {
			ContentValues cv = new ContentValues();
			cv.put("name", name);
			if (value != null) cv.put("value", value);
			else cv.putNull("value");
			long rowID = db.insert("options", null, cv);
		    if (rowID > -1) {
		    	result = true;
		    	Log.d(LOG_TAG, "Created new option: " + name + "=" + value);
		    }
		    else {
		    	Log.e(LOG_TAG, "Error when creating option: " + name);
		    }
		}
    	return result;
    }

    /* Читает значение опции из таблицы настроек */
    public String GetOptionValue(SQLiteDatabase db, String name) {
    	String result = null;
    	String[] columns = {"value"};
    	try {
			Cursor cur = db.query("options", columns, "name = '" + name + "'", null, null, null, null);
			if (cur.moveToFirst()) {
				result = cur.getString(0);
			}
		} catch (SQLiteException e) {
			Log.e(LOG_TAG, "GetOptionValue(): " + e.toString());
		} catch (IllegalStateException e) {
			Log.e(LOG_TAG, "GetOptionValue(): " + e.toString());
		}
    	return result;
    }

    /**
     * Читает ответ сервера из таблицы запросов к серверу (используется GUID)
     * @param db
     * @param guid
     * @return
     */
    public String GetAnswerValue(SQLiteDatabase db, String guid) {
    	String result = null;
    	String[] columns = {"answer"};
		Cursor cur = db.query("requests", columns, "guid = '" + guid + "'", null, null, null, null);
		if (cur.moveToFirst()) {
			result = cur.getString(0);
		}
    	return result;
    }

    /**
     * Читает текст сообщения из таблицы сообщений (используется GUID)
     * @param db
     * @param guid
     * @return
     */
    public String GetMessageValue(SQLiteDatabase db, String guid) {
    	String result = null;
    	String[] columns = {"message"};
		Cursor cur = db.query("messages", columns, "guid = '" + guid + "'", null, null, null, null);
		if (null != cur) {
			if (cur.moveToFirst()) {
				result = cur.getString(0);
			}
			cur.close();
		}
    	return result;
    }

    /**
     * Устанавливает значение поля в таблице-анкете пользователя
     * @param db
     * @param userid
     * @param name
     * @param value
     * @return
     */
    public boolean SetUserValue(SQLiteDatabase db, int userid, String name, String value) {
		ContentValues cv = new ContentValues();
		if (value != null) cv.put(name, value);
		else cv.putNull(name);
		long rowCount = db.update("userinfo", cv, "_id = " + userid, null);
		if (rowCount > 0) {
			Log.d(LOG_TAG, "Updated " + name + "=" + (value != null ? value : "null") + " for userid=" + userid);
			return true;
		}
		else {
			Log.e(LOG_TAG, "Error when updating " + name + " (" + (value != null ? value : "null") + ") for userid=" + userid);
			return false;
		}
    }
    
    /**
     * Читает значение поля из таблицы-анкеты пользователя
     * @param db
     * @param userid
     * @param name
     * @return
     */
    public String GetUserValue(SQLiteDatabase db, int userid, String name) {
    	String result = null;
    	String[] columns = {name};
    	Cursor cur = db.query("userinfo", columns, "_id = " + userid, null, null, null, null);
    	if (null != cur) {
			if (cur.moveToFirst()) {
				result = cur.getString(0);
			}
			cur.close();
		}
    	return result;
    }

    /**
     * Возвращает число записей в таблице справочников для указанного справочника
     * @param db - база данных
     * @param dictionary - название справочника
     * @return число записей
     */
    public int GetDictionaryCount(SQLiteDatabase db, String dictionary) {
    	int result = 0;
    	String sql = "dictionary = '" + dictionary + "'";
		String[] columns = { "COUNT(*)" };
		Cursor cur = db.query("remote_dictionaries", columns, sql, null, null, null, null);
		if (null != cur) {
			if (cur.moveToFirst()) {
				result = cur.getInt(0);
			}
			cur.close();
		}
    	return result;
    }

    /**
     * Возвращает md5-хэш столбца "Наименование" всех записей в таблице справочников для указанного справочника и строки поиска
     * @param db - база данных
     * @param dictionary - название справочника
     * @param searchPrefix - ключи для поиска по связанным таблицам
     * @param search - строка для поиска
     * @param limit - максимальное количество строк для выборки
     * @return md5-хэш
     */
    public String GetDictionaryHash(SQLiteDatabase db, String dictionary, String searchPrefix, String search, int limit) {
    	String result = null;
    	String sql = "dictionary = '" + dictionary + "'";
    	if (searchPrefix != null) {
    		sql += " AND search_prefix = '" + searchPrefix + "'";
    	}
    	if (search != null) {
    		sql += " AND (name like '%" + search + "%'" + " OR name_rus like '%" + search.toLowerCase() + "%')";
    	}
		String[] columns = { "remoteid", "name", "remark" };
		Cursor cur = db.query("remote_dictionaries", columns, sql, null, null, null, "name ASC, remoteid ASC", Integer.toString(limit));
		if (null != cur) {
			if (cur.moveToFirst()) {
				String str = "";
				do {
					str += cur.getString(1);
				} while (cur.moveToNext());
				result = CommonCoreMethods.md5(str);
			}
			cur.close();
		}
    	return result;
    }

    /**
     * Ожидает появления справочника в БД и возвращает его
     * @param db - база данных
     * @param dictionary - наименование справочника
     * @param searchPrefix - ключи для поиска по связанным таблицам
     * @param search - строка для поиска
     * @param limit - максимальное количество строк для выборки
     * @param guid - GUID запроса на справочник из сети
     * @param dictionaryList - справочник (возвращаемый параметр)
     */
    public void GetDictionary(SQLiteDatabase db, String dictionary, String searchPrefix, String search, int limit, String guid, ArrayList<RemoteDictionary> dictionaryList) {
		String[] columnsMessages = {"sent"};
		try {
			Cursor cur;
			for (int ii = 0; ii < Constants.DICTIONARY_WAIT_COUNT; ++ii) {
				cur = db.query("messages", columnsMessages, "guid = '" + guid + "'", null, null, null, null);
				if (null != cur) {
					if (cur.moveToFirst() && cur.getInt(0) > 0) {
				    	String sql = "dictionary = '" + dictionary + "'";
				    	if (searchPrefix != null) {
				    		sql += " AND search_prefix = '" + searchPrefix + "'";
				    	}
				    	if (search != null) {
				    		sql += " AND (name like '%" + search + "%'" + " OR name_rus like '%" + search.toLowerCase() + "%')";
				    	}
						String[] columns = { "_id", "remoteid", "name", "remark" };
						cur = db.query("remote_dictionaries", columns, sql, null, null, null, "name ASC, remoteid ASC", Integer.toString(limit));
						if (null != cur) {
							if (cur.moveToFirst()) {
								do {
									dictionaryList.add( new RemoteDictionary(cur.getInt(0), cur.getInt(1), cur.getString(2), cur.getString(3)) );
								} while (cur.moveToNext());
							}
							cur.close();
						}
						break;
					}
					cur.close();
				}
				try {
					TimeUnit.MILLISECONDS.sleep(Constants.DICTIONARY_WAIT_INTERVAL);
				} catch (InterruptedException e) {

				}
			}
		} catch (SQLiteException e) {
			Log.e(LOG_TAG, "GetDictionary(): " + e.toString());
		} catch (IllegalStateException e) {
			Log.e(LOG_TAG, "GetDictionary(): " + e.toString());
		}
    }

    /**
     * Помещает сообщения в очередь на отправку. TODO: Переделать, т.к. сообщения упаковываются в JSON перед вставкой.
     * @param db - база данных
     * @param server - идентификатор сервера назначения
     * @param guidArray - массив GUID
     * @param status - статус запроса к серверу (-1-зарезервирован к отправке, 0-на отправку, 1-отправлен, 2-ошибка отправки)
     * @param params - массив сообщений
     * @return true, если все сообщения размещены в очереди успешно
     */
    public boolean MessageToQueue(SQLiteDatabase db, int server, ArrayList<String> guidArray, int status, String... params) {
    	boolean result = true;
    	int userid = application.userId; //GetOptionValue(db, "userid");
		ContentValues cv;
		for(int ii = 0; ii < params.length; ++ii) {
			String[] paramArray = params[ii].split("\\|\\|", 3);
			try {
				long timemark = System.currentTimeMillis();
				String guid = UUID.randomUUID().toString();
				cv = new ContentValues();
				cv.put("request", params[ii]);
			    cv.put("timemark", timemark);
			    cv.put("userid", userid);
			    cv.put("guid", guid);
			    cv.put("sent", status);
			    cv.put("server", server);
			    long rowID = db.insert("requests", null, cv);
			    guidArray.add((rowID > -1) ? guid : null);
			    if (rowID > -1) {
			    	Log.d(LOG_TAG, "MessageToQueue" + "> Request data inserted into DB, ID=" + rowID);
			    	Log.d(LOG_TAG, "Requests count: " + GetRowCount(db, "requests"));
			    	switch (paramArray[0]) {
			    	case Constants.REQUEST_MESSAGE:
			    		// Вставка отправленных пользователем сообщений в таблицу:
			    		JSONObject requestData = new JSONObject(paramArray[1]);
					    cv = new ContentValues();
					    cv.put("message", requestData.getString("text"));
						cv.put("direction", 0);
						cv.put("timemark", timemark);
					    cv.put("userid", userid);
					    cv.put("guid", guid);
					    rowID = db.insert("messages", null, cv);
					    if (rowID > -1) {
					    	Log.d(LOG_TAG, "Client message inserted into DB, ID=" + rowID);
					    	// Вставка в таблицу путей до прикрепленных файлов:
					    	if (paramArray.length > 2) { // Если имеются прикрепленные файлы:
					    		JSONArray requestAttachments = new JSONArray(paramArray[2]);
						    	for (int jj = 0; jj < requestAttachments.length(); ++jj) {
						    		JSONObject attachment = requestAttachments.getJSONObject(jj);
								    cv = new ContentValues();
								    cv.put("messageid", rowID);
								    cv.put("path", attachment.getString("file"));
								    db.insert("attachments", null, cv);
						    	}
					    	}
					    } else {
					    	result = false;
					    	Log.e(LOG_TAG, "Error on inserting client message into DB.");
					    }
					    break;
			    	case Constants.REQUEST_REPORT:
			    		// Вставка отправленных пользователем отчетов в таблицу:
			    		requestData = new JSONObject(paramArray[1]);
					    cv = new ContentValues();
					    cv.put("message", requestData.getString("text"));
						cv.put("timemark", timemark);
					    cv.put("userid", userid);
					    cv.put("guid", guid);
					    rowID = db.insert("reports", null, cv);
					    if (rowID > -1) {
					    	Log.d(LOG_TAG, "Client report inserted into DB, ID=" + rowID);
					    	// Вставка в таблицу путей до прикрепленных файлов:
					    	if (paramArray.length > 2) { // Если имеются прикрепленные файлы:
						    	JSONArray requestAttachments = new JSONArray(paramArray[2]);
						    	for (int jj = 0; jj < requestAttachments.length(); ++jj) {
						    		JSONObject attachment = requestAttachments.getJSONObject(jj);
								    cv = new ContentValues();
								    cv.put("reportid", rowID);
								    cv.put("type", attachment.getString("name"));
								    cv.put("path", attachment.getString("file"));
								    db.insert("report_attachments", null, cv);
						    	}
					    	}
					    } else {
					    	result = false;
					    	Log.e(LOG_TAG, "Error on inserting client report into DB.");
					    }
					    break;
			    	case Constants.REQUEST_DICTIONARY:
			    		requestData = new JSONObject(paramArray[1]);
			    		// Вставка запроса на справочник в таблицу:
					    cv = new ContentValues();
					    cv.put("message", requestData.getString("search"));
					    cv.put("direction", 0);
						cv.put("timemark", timemark);
					    cv.put("userid", userid);
					    cv.put("guid", guid);
					    rowID = db.insert("messages", null, cv);
					    if (rowID > -1) {
					    	Log.d(LOG_TAG, "Request for dictionary inserted into DB, ID=" + rowID);
					    } else {
					    	result = false;
					    	Log.e(LOG_TAG, "Error on inserting request for dictionary into DB.");
					    }
					    break;
			    	default:
			    		// Прочие сообщения
			    		break;
			    	}
			    } else {
			    	result = false;
			    	Log.e(LOG_TAG, "MessageToQueue():" + "Error on inserting request data into DB.");
			    }
	        } catch (JSONException e) {
	        	Log.e(LOG_TAG, "MessageToQueue(): " + e.toString());
	        }
		}
		return result;
    }

    /**
     * Помещает сообщения в очередь на отправку в ближайшее время
     * @param db - база данных
     * @param server - идентификатор сервера назначения
     * @param guidArray - массив GUID
     * @param params - массив сообщений
     * @return true, если все сообщения размещены в очереди успешно
     */
    public boolean MessageToQueue(SQLiteDatabase db, int server, ArrayList<String> guidArray, String... params) {
    	return MessageToQueue(db, server, guidArray, 0, params);
    }

    /**
     * Отзывает запросы из очереди на обработку (по GUID записи в таблице запросов)
     * @param db - база данных
     * @param guidArray - массив GUID
     * @return true, если все запросы отозваны успешно, либо false, если нечего отзывать или проблема с БД
     */
    public boolean RecallMessage(SQLiteDatabase db, ArrayList<String> guidArray) {
    	return true;
    }

    /**
     * Отзывает запросы из очереди на обработку (по идентификатору строки запроса к серверу)
     * @param db - база данных
     * @param identifier - строка идентификатора
     * @return true, если все запросы отозваны успешно, либо false, если нечего отзывать или проблема с БД
     */
    public boolean RecallMessage(SQLiteDatabase db, String identifier) {
    	ContentValues cv = new ContentValues();
	    cv.put("sent", 3); // Меняем статус запроса на "отозван":
	    long rowCount = db.update("requests", cv, "sent < 1 AND request like '" + identifier + "||" + "%'", null);
	    if (rowCount == 0) {
	    	// Не удалось обновить статус запросов в БД (либо нечего было обновлять):
	    	Log.d(LOG_TAG, "No updated database rows for recalling requests (" + identifier + ").");
	    	return false;
	    } else {
	    	// Запросы успешно отозваны из обработки:
	    	Log.d(LOG_TAG, "Recalled " + rowCount + " requests (" + identifier + ").");
	    	return true;
	    }
    }

    /**
     * Изменяет статус запроса в очереди
     * @param db - база данных
     * @param guid - идентификатор запроса
     * @param userid - идентификатор пользователя
     * @param status - статус отправки
     * @param answer - ответ сервера или комментарий транспортной машины
     * @return true, если все сообщения размещены в очереди успешно
     */
	public boolean ChangeRequestStatus(SQLiteDatabase db, String guid, String userid, int status, String answer) {
		
		boolean result;
		ContentValues cv = new ContentValues();
		if (userid != null) cv.put("userid", userid);
		if (answer != null) cv.put("answer", answer);
		cv.put("timemark", System.currentTimeMillis());
	    cv.put("sent", status);
	    long rowCount = db.update("requests", cv, "guid = '" + guid + "'", null);
	    result = (rowCount != 0);
	    if (!result) Log.e(LOG_TAG, "Error on updating request by ChangeRequestStatus()");
	    else Log.d(LOG_TAG, "Request updated successfully by ChangeRequestStatus()");
		return result;
	}

    /**
     * Очищает таблицу по ее наименованию
     * @param db - база данных
     * @param name - наименование таблицы в базе данных
     * @return True, если записи были успешно удалены.
     */
    public static boolean ClearTable(SQLiteDatabase db, String name) {
    	long rowCount = db.delete(name, "1", null);
    	if (rowCount > 0) rowCount = db.delete("sqlite_sequence", "name = '" + name + "'", null);
    	return rowCount > 0;
    }

    /**
     * Возвращает число записей в таблице
     * @param db - база данных
     * @param table - название таблицы в БД
     * @return число записей
     */
	public static int GetRowCount(SQLiteDatabase db, String table) {
		int result = 0;
		String[] columns = { "COUNT(*)" };
		Cursor cur = db.query(table, columns, null, null, null, null, null);
		if (null != cur) {
			if (cur.moveToFirst()) {
				result = cur.getInt(0);
			}
			cur.close();
		}
		return result;
	}

    /**
     * "Сжимает" базу данных
     * @param db - база данных
     * @return
     */
    public static boolean Vacuum(SQLiteDatabase db) {
    	db.execSQL("VACUUM");
    	return true;
    }

	/**
	 * Получает версию Sqlite
	 * @param db
	 * @return
	 */
	public static String GetSqliteVersion(SQLiteDatabase db) {
		String result = "";
		Cursor cur = db.rawQuery("select sqlite_version() AS sqlite_version", null);
		if (null != cur) {
			while (cur.moveToNext()) {
				result += cur.getString(0);
			}
			cur.close();
		}
		return result;
	}
}
