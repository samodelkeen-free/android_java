package com.aistkem.currencyconverter.helpers;

import com.aistkem.currencyconverter.handlers.HttpRequestCommand;
import com.aistkem.currencyconverter.handlers.TestServiceCommand;
import com.aistkem.currencyconverter.helpers.core.CoreServiceHelper;

import android.app.Application;
import android.content.Intent;

public class ServiceHelper extends CoreServiceHelper {

    public ServiceHelper(Application app) {
    	super(app);
    }

    public int HttpRequestAction(String url) {
		final int requestId = createId();

		Intent i = createIntent(application, new HttpRequestCommand(url), requestId);
		return runRequest(requestId, i);
    }
    
    public int TestServiceAction(String arg) {
		final int requestId = createId();

		Intent i = createIntent(application, new TestServiceCommand(arg), requestId);
		return runRequest(requestId, i);
    }

}
