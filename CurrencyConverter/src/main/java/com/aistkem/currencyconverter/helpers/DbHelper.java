package com.aistkem.currencyconverter.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.aistkem.currencyconverter.helpers.core.CoreDbHelper;

public class DbHelper extends CoreDbHelper {
	
    public DbHelper(Context context) {
      super(context, "CurrencyConverterDb", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    	super.onCreate(db);
    	
    	/**
    	 * Локальное хранилище курсов валют
    	 */
    	db.execSQL("create table currency ("
    	+ "_id integer primary key autoincrement,"
    	+ "numcode integer not null,"
    	+ "charcode text not null,"
    	+ "nominal integer not null,"
    	+ "name text not null,"
    	+ "value real not null,"
    	+ "date integer not null,"
    	+ "usage integer not null default 2"
    	+ ");");
    	
    	db.execSQL("CREATE UNIQUE INDEX ui_currency$numcode$charcode ON currency(numcode, charcode)");
    	
    	db.execSQL("insert into options (name, value) values ("
		+ "'url',"
		+ "'http://192.168.0.104:8080/index.php'"
		+ ");");
    	
    	db.execSQL("insert into listitems (list_guid, list_item, item_name, item_guid) values ("
		+ "'4621ecbd-5e5f-4001-b5f6-c0373aa787bd', 'http://94.251.61.137:8080/index.php', 'remote_aist', 'de18cd3e-d548-4879-a232-4571da15f779');");
    	db.execSQL("insert into listitems (list_guid, list_item, item_name, item_guid) values ("
    	+ "'4621ecbd-5e5f-4001-b5f6-c0373aa787bd', 'http://192.168.0.104:8080/index.php', 'local_aist', '5051aa9c-d2de-43b8-873f-17555077fc7a');");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	super.onUpgrade(db, oldVersion, newVersion);

    	onCreate(db);
    }

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

}
