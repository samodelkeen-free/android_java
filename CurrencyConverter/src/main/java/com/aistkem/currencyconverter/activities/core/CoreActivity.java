package com.aistkem.currencyconverter.activities.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.EditText;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.R;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment.PlaceholderFragmentData;
import com.aistkem.currencyconverter.helpers.core.CoreServiceHelper;
import com.aistkem.currencyconverter.interfaces.core.CoreServiceCallbackListener;

public abstract class CoreActivity extends AppCompatActivity implements CoreServiceCallbackListener {

	protected final String LOG_TAG = this.getClass().getName();
	protected CoreApplication application;
	protected CoreServiceHelper serviceHelper;
	
	protected int applicationTask = Constants.APPLICATION_TASK_NONE;
	protected HashMap<Integer, Object> applicationTasks = new HashMap<Integer, Object>();
	/**
	 * Цвет пунктов бокового меню
	 */
	protected int drawerListColor;
	/**
	 * Стартовый пункт бокового меню
	 */
	protected int drawerListStart = 0;
	/**
	 * Список заголовков для Placeholder-фрагментов
	 */
	protected SparseIntArray titleArray;
	/**
	 * Список пунктов бокового меню-шторки
	 */
	public String[] DrawerListArray;

	public Point displaySize;

    public List<PlaceholderFragmentData> activeCenterFragments = new ArrayList<PlaceholderFragmentData>();

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    protected CharSequence mTitle;

	/* *** Реализации override-методов CoreActivity *** */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = (CoreApplication) this.getApplication();
		application.displaySize = this.displaySize = CoreApplication.GetDisplaySize(this);
		serviceHelper = application.getServiceHelper();
		
		mTitle = getTitle();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("applicationTask", applicationTask);
		outState.putSerializable("applicationTasks", applicationTasks);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		applicationTask = savedInstanceState.getInt("applicationTask");
		applicationTasks = (HashMap<Integer, Object>) savedInstanceState.getSerializable("applicationTasks");
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		serviceHelper.addListener(this);
		application.setCurrentActivity(this);
	}

	@Override
	protected void onPause() {
		serviceHelper.removeListener(this);
		clearReferences();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		clearReferences();
		super.onDestroy();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

    /**
     * Called when a service request finishes executing.
     * 
     * @param requestId - original request id
     * @param requestIntent - original request data
     * @param resultCode - result of execution code
     * @param resultData - result data
     */
	@Override
	public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {}

	/* *** Блок функционала для работы с CoreActivity и его фрагментами *** */

	public final CoreServiceHelper GetServiceHelper() {
		return serviceHelper;
	}

	public final CoreApplication GetApplication() {
		return application;
	}

	public final HashMap<Integer, Object> GetApplicationTasks() {
		return applicationTasks;
	}
/*
	public abstract int GetDrawerListColor();
*/

	public int GetDrawerListColor() {
		return drawerListColor;
	}

	public int GetDrawerListStart() {
		return drawerListStart;
	}

	public abstract void updateTitle(Object param);

    protected void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        //actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setTitle(mTitle);
        //actionBar.setDisplayUseLogoEnabled(true);
    }

	/**
	 * Удаляет ссылку на текущую Activity в Application
	 */
	protected void clearReferences() {
    	Activity currentActivity = application.getCurrentActivity();
    	if (this.equals(currentActivity)) application.setCurrentActivity(null);
    }

	protected static void StartCommonRefreshThread(Thread commonRefreshThread, CommonRefreshRun commonRefreshRun) {
		if (commonRefreshThread != null && commonRefreshThread.isAlive()) {
			if (commonRefreshRun.isRunning) {
				commonRefreshRun.keepRunning = true;
				return;
			}
			else try { commonRefreshThread.join(1000);
				} catch(InterruptedException e){}
		}
		if (commonRefreshThread == null || !commonRefreshThread.isAlive()) {
			commonRefreshThread = new Thread(commonRefreshRun);
			commonRefreshThread.start();
		}
	}

	protected static void StopCommonRefreshThread(Thread commonRefreshThread, CommonRefreshRun commonRefreshRun, int wait) {
		commonRefreshRun.keepRunning = false;
		if (commonRefreshThread != null && commonRefreshThread.isAlive()) {
			commonRefreshThread.interrupt();
			try { commonRefreshThread.join(wait);
			} catch(InterruptedException e){}
		}
	}

	/**
	 * Метод обновления информации, выполняемый в потоке
	 * @author Владимир
	 *
	 */
	protected class CommonRefreshRun implements Runnable {

		protected final String LOG_TAG = this.getClass().getName();
		protected Intent intent;

		public volatile boolean keepRunning = true;
		public volatile boolean needPause = true;
		public volatile boolean isRunning = false;

		public CommonRefreshRun() {
		}

		public void run() {
			keepRunning = true;
		}

		protected void stop() {
		}

	}

	/**
	 * Цикл обновления информации в приложении
	 * @param refreshRun - метод обновления информации, выполняемый в потоке
	 * @param data - объект настроек потока циклического обновления информации в приложении
	 */
	protected abstract void refreshLoop(CommonRefreshRun refreshRun, RefreshThreadData data);

	/* Методы для работы с фрагментами */

	//protected abstract void getCenterFragments(Object param);

    public void addCenterFragments() {
        if (activeCenterFragments.size() > 0) {
        	FragmentManager fragmentManager = getSupportFragmentManager();
        	FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            for (PlaceholderFragmentData centerFragment : activeCenterFragments) {
            	fragmentTransaction.add(centerFragment.containerId, centerFragment.fragment, centerFragment.fragment.GetTag());
            }
            fragmentTransaction.commit();
        }
    }

    public void removeCenterFragments() {
    	removeCenterFragments(true);
    }

    public void removeCenterFragments(boolean clearStack) {
        if (activeCenterFragments.size() > 0) {
        	FragmentManager fragmentManager = getSupportFragmentManager();
        	if (clearStack) fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); // Удаляет сохраненные фрагменты из стека
        	FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            for (PlaceholderFragmentData centerFragment : activeCenterFragments) {
                fragmentTransaction.remove(centerFragment.fragment);
            }
            fragmentTransaction.commit();
            activeCenterFragments.clear();
        }
    }

    public void restoreStackFragments(String stateName) {
		removeCenterFragments(false);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.popBackStack/*Immediate*/(stateName, /*0*/FragmentManager.POP_BACK_STACK_INCLUSIVE); // Восстанавливает состояние стека фрагментов с идентификатором stateName
    }

	public void onBindServiceFragmentNotification() {
		PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentById(R.id.container);
		if (fragment != null) fragment.OnBindService();
	}

	public void onUnBindServiceFragmentNotification() {
		PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentById(R.id.container);
		if (fragment != null) fragment.OnUnBindService();
	}

	/* *** Блок служебных статических классов *** */

	public static class TextWatcherPhone implements TextWatcher {

        private final static String regexCode0 = "(^\\+(\\d{1,3})\\s).*";
        private final static String regexNumber0 = "^\\+\\d+\\s|\\D+";
        private final static String regexCode1 = "(^\\+(\\d{1})).*";
        private final static String regexNumber1 = "^\\+\\d{1}|\\D+";
        private final static String regexNumber2 = "\\D+";
        private final static String regexPhone = "^\\+\\d{1,3}\\s\\(\\d{3}\\)\\s\\d{3}\\-\\d{2}-\\d{2}$";
		
		public EditText editText;
		private boolean afterTextChangedEnable = true;
		
		public TextWatcherPhone(EditText editText) {
			super();
			this.editText = editText;
		}

		@Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	    }

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count) {

	    }

	    @Override
	    public void afterTextChanged(Editable s) {
	    	
	    	if (!afterTextChangedEnable) {
	    		afterTextChangedEnable = true;
	    		return;
	    	}
	        
	        String code = "";
	        String number = "";
	        if (s.toString().matches(regexPhone)) {
	        	return;
	        } else if (s.toString().matches(regexCode0)) { //Если обнаружился код страны с пробелом:
	        	code = String.valueOf(s).replaceFirst(regexCode0, "$2");
	        	number = String.valueOf(s).replaceAll(regexNumber0, "");
	        } else if (s.toString().matches(regexCode1)) { //Если обнаружился код страны без пробела:
	        	code = String.valueOf(s).replaceFirst(regexCode1, "$2");
	        	number = String.valueOf(s).replaceAll(regexNumber1, "");
	        } else {
	        	number = String.valueOf(s).replaceAll(regexNumber2, "");
	        	if (number.length() == 0) {
	        		code = "7";
	        	} else {
	        		code = number.substring(0, 1);
	        		number = number.substring(1, number.length());
	        	}
	        }
	        
	        String formatted = "+" + code + " ";
        	if (number.length() > 2) {
        		formatted += "(" + number.substring(0,3) + ")";
        		if (number.length() > 5) {
        			formatted += " " + number.substring(3,6);
        			if (number.length() > 7) {
        				formatted += "-" + number.substring(6,8);
        				if (number.length() > 9) {
        					formatted += "-" + number.substring(8,10);
        				} else formatted += number.substring(8,number.length());
        			} else formatted += number.substring(6,number.length());
        		} else formatted += number.substring(3,number.length());
        	} else formatted += number;
	        afterTextChangedEnable = false;
            editText.setText(formatted);
            editText.setSelection(formatted.length());
            
	        /*
	        String regex1 = "(\\+\\d)(\\d{3})";
	        String regex2 = "(.+ )(\\d{3})$";
	        String regex3 = "(.+\\-)(\\d{2})$";
	        if (s.toString().matches(regex1)) {
	            formatted = String.valueOf(s).replaceFirst(regex1, "$1 ($2) ");
	            editLogin.setText(formatted);
	            editLogin.setSelection(formatted.length());
	        } else if (s.toString().matches(regex2)) {
	            formatted = String.valueOf(s).replaceFirst(regex2, "$1$2-");
	            editLogin.setText(formatted);
	            editLogin.setSelection(formatted.length());
	        } else if (s.toString().matches(regex3) && s.length() < 18) {
	            formatted = String.valueOf(s).replaceFirst(regex3, "$1$2-");
	            editLogin.setText(formatted);
	            editLogin.setSelection(formatted.length());
	        }
	        */
	    }
    }

	public static class OnGlobalLayoutListenerAist implements OnGlobalLayoutListener {
		//@SuppressLint("NewApi")
		//@SuppressWarnings("deprecation")
		
		private View view;
		private PlaceholderFragment fragment;
		private int width;
		private int height;
		
		public OnGlobalLayoutListenerAist(View view, PlaceholderFragment fragment) {
			super();
			this.view = view;
			this.fragment = fragment;
		}
		
		public int getWidth() {
			return width;
		}
		
		public int getHeight() {
			return height;
		}
		
		@Override
		public void onGlobalLayout() {
			//now we can retrieve the width and height
			width = view.getWidth();
			height = view.getHeight();
			fragment.ResizeView(view);
			//...
			//do whatever you want with them
			//...
			//this is an important step not to keep receiving callbacks:
			//we should remove this listener
			//I use the function to remove it based on the api level!

			if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
				view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
			else
				view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
		}
	}

	/**
	 * Класс для хранения настроек потоков циклического обновления информации в activity
	 * @author Владимир
	 *
	 */
	protected static class RefreshThreadData {
		public String dataName;
		public int minSleepTime;
		public int minSleepCount;
		public int maxSleepCount;
		public int maxFaults;
		
		public RefreshThreadData() {
			
		}
	}
	
	/* Механизм слабых ссылок (EVV: использовать!)
	public class MainActivity extends Activity {
		 
        Handler handler;
        TextView tvTest;
        int cnt = 0;
 
        @Override
        protected void onCreate(Bundle savedInstanceState) {
 
                super.onCreate(savedInstanceState);
                setContentView(R.layout.main);
 
                tvTest = (TextView) findViewById(R.id.tvTest);
               
                handler = new MyHandler(this);
                handler.sendEmptyMessageDelayed(0, 1000);
        }
 
        void someMethod() {
                tvTest.setText("Count = " + cnt++);
                handler.sendEmptyMessageDelayed(0, 1000);
        }
 
        @Override
        protected void onDestroy() {
                if (handler != null)
                        handler.removeCallbacksAndMessages(null);
                super.onDestroy();
        }
 
        static class MyHandler extends Handler {
 
                WeakReference<MainActivity> wrActivity;
 
                public MyHandler(MainActivity activity) {
                        wrActivity = new WeakReference<MainActivity>(activity);
                }
 
                @Override
                public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        MainActivity activity = wrActivity.get();
                        if (activity != null)
                                activity.someMethod();
                }
        }
	}
	*/

}
