package com.aistkem.currencyconverter.activities;

import java.util.concurrent.TimeUnit;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.aistkem.currencyconverter.Classes.Currency;
import com.aistkem.currencyconverter.Classes.CurrencySearch;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.CurrencyConverterApplication;
import com.aistkem.currencyconverter.R;
import com.aistkem.currencyconverter.activities.core.CoreActivity;
import com.aistkem.currencyconverter.fragments.AboutFragment;
import com.aistkem.currencyconverter.fragments.MainFragment;
import com.aistkem.currencyconverter.fragments.TestFragment;
import com.aistkem.currencyconverter.fragments.core.NavigationDrawerFragment;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment.PlaceholderFragmentData;
import com.aistkem.currencyconverter.fragments.core.TemplateListDialogFragment;
import com.aistkem.currencyconverter.handlers.HttpRequestCommand;
import com.aistkem.currencyconverter.handlers.TestServiceCommand;
import com.aistkem.currencyconverter.handlers.core.CoreServiceCommand;
import com.aistkem.currencyconverter.helpers.ServiceHelper;
import com.aistkem.currencyconverter.interfaces.AppTasksListener;
import com.aistkem.currencyconverter.interfaces.ConverterListener;
import com.aistkem.currencyconverter.interfaces.CurrencyListListener;
import com.aistkem.currencyconverter.interfaces.MainTasksListener;
import com.aistkem.currencyconverter.receivers.MainActivityReceiver;
import com.aistkem.currencyconverter.repository.CommonMethods;

public class MainActivity extends CoreActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, 
	MainTasksListener,
	AppTasksListener,
	ConverterListener,
	CurrencyListListener {
	
	private int requestId = -1;
	
	private Currency currencyFrom, currencyTo;
	
	private Thread somethingRefreshThread;
	private SomethingRefreshRun somethingRefreshRun;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private BroadcastReceiver broadcastReceiver;

    public final static int SECTION_MAIN = 0;
    //public final static int SECTION_OPTIONS = 1;
    public final static int SECTION_ABOUT = 1;
    public final static int SECTION_HIDE = 2;
    public final static int SECTION_EXIT = 3;
    //public final static int SECTION_TEST = 4;

	/** *** Реализации override-методов MainActivity *** */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		drawerListColor = CommonMethods.getColor(this, R.color.client_menu_body);
	    DrawerListArray = new String[] {
	    		getString(R.string.drawer_section_main),
	    		//getString(R.string.drawer_section_options),
	    		getString(R.string.drawer_section_about),
	    		getString(R.string.drawer_section_hide),
	    		getString(R.string.drawer_section_exit),
	    		//getString(R.string.drawer_section_test),
	    		};
	    titleArray = new SparseIntArray();
	    titleArray.put(Constants.APPLICATION_TASK_WELCOME, R.string.title_section_main);
	    titleArray.put(Constants.APPLICATION_TASK_ABOUT, R.string.title_section_about);
	    //titleArray.put(Constants.APPLICATION_TASK_TEST, R.string.title_section_test);

		setContentView(R.layout.activity_main);
		((CurrencyConverterApplication) this.GetApplication()).CreateServiceConnection();

		// EVV: В контексте этого приложения никак не используется.
		somethingRefreshRun = new SomethingRefreshRun();

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(com.aistkem.currencyconverter.R.id.navigation_drawer);
		mNavigationDrawerFragment.setUp(com.aistkem.currencyconverter.R.id.navigation_drawer, (DrawerLayout) findViewById(com.aistkem.currencyconverter.R.id.drawer_layout));

        /* Создаем BroadcastReceiver для получения сообщений от сервиса */
		broadcastReceiver = new MainActivityReceiver(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("requestId", requestId);
		if (currencyFrom != null) outState.putSerializable("currencyFrom", currencyFrom);
		if (currencyTo != null) outState.putSerializable("currencyTo", currencyTo);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		requestId = savedInstanceState.getInt("requestId");
		currencyFrom = (Currency) savedInstanceState.getSerializable("currencyFrom");
		currencyTo = (Currency) savedInstanceState.getSerializable("currencyTo");
	}

	@Override
	protected void onStart() {
		super.onStart();
		application.StartService();
		IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION);
		registerReceiver(broadcastReceiver, intentFilter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		//if (requestId != -1 && serviceHelper.isPending(requestId)) { EVV: Здесь это не используется.
			PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentByTag(Constants.WELCOME_FRAGMENT_TAG);
			if (fragment != null) {
				fragment.RefreshView(Constants.FRAGMENT_REFRESH_TASK_CURRENCY, currencyFrom, currencyTo);
			}
		//}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		unregisterReceiver(broadcastReceiver);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		StopCommonRefreshThread(somethingRefreshThread, somethingRefreshRun, 5000);
		if ( ("android.intent.action.MAIN").equalsIgnoreCase(getIntent().getAction()) ) application.UnbindService();
		super.onDestroy();
	}

	@Override
    public void onNavigationDrawerItemSelected(int position, boolean fromSavedInstanceState) {
		switch (position) {
			case SECTION_MAIN:
				if (!fromSavedInstanceState) {
					TaskWelcomeEvent();
					/*
					if (CurrencyConverterApplication.isLogined) TaskWelcomeEvent();
					else TaskLoginEvent();
					*/
				}
				break;
				/*
			case SECTION_OPTIONS:
				if (!fromSavedInstanceState) TaskOptionsEvent();
				break;
				*/
			case SECTION_ABOUT:
	    		if (!fromSavedInstanceState) TaskAboutEvent();
	    		break;
	    	case SECTION_HIDE:
	            //System.exit(0);
	            finish();
	            break;
	    	case SECTION_EXIT:
	            //System.exit(0);
	            finish();
	            application.UnbindStopService();
	            break;
	            /*
			case SECTION_TEST:
	    		if (!fromSavedInstanceState) TaskTestEvent();
	    		break;
	    		*/
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed() {
		removeCenterFragments(false);
		super.onBackPressed();
	}

	@Override
    public void updateTitle(Object param) {
		// Обновление заголовков в зависимости от APPLICATION_TASK_*:
		mTitle = getString(titleArray.get((int)param, R.string.title_section_main));
		restoreActionBar(); // TODO: Пока приткнул сюда. Не знаю пока, какие вылезут косяки.
    }

	/**
	 * Обработчик ответов сервиса исполнения команд
	 */
	@Override
	public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
		super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

		if (serviceHelper.check(requestIntent, HttpRequestCommand.class)) {
			StopRemoteRequestEvent(resultCode, resultData);
		} else
			if (serviceHelper.check(requestIntent, TestServiceCommand.class)) { // EVV: Для тестирования сервиса
				if (resultCode == CoreServiceCommand.RESPONSE_SUCCESS) {
					Toast.makeText(this, resultData.getString("data").substring(0, resultData.getString("data").length() > 10 ? 10 : resultData.getString("data").length()), Toast.LENGTH_LONG).show();
				} else if (resultCode == CoreServiceCommand.RESPONSE_PROGRESS) {
					resultData.getInt(CoreServiceCommand.EXTRA_PROGRESS, -1);
				} else if (resultCode == CoreServiceCommand.RESPONSE_CANCEL) {
					Toast.makeText(this, resultData.getString("message"), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(this, resultData.getString("error"), Toast.LENGTH_LONG).show();
				}
			}
    }

	/** *** Блок методов, реализующих подключенные интерфейсы *** */

	/** com.aistkem.currencyconverter.interfaces.AppTasksListener */

	@Override
	public void TaskSwitchActivityEvent() {}

	/** com.aistkem.currencyconverter.interfaces.MainTasksListener */

	@Override
	public void TaskWelcomeEvent() {
		this.applicationTask = Constants.APPLICATION_TASK_WELCOME;
		removeCenterFragments();
		activeCenterFragments.add( new PlaceholderFragmentData(R.id.container, new MainFragment(this.applicationTask, Constants.WELCOME_FRAGMENT_TAG)) );
		addCenterFragments();
	}

	@Override
	public void TaskLoginEvent() {
		this.applicationTask = Constants.APPLICATION_TASK_LOGIN;
	}

	@Override
	public void TaskOptionsEvent() {
		this.applicationTask = Constants.APPLICATION_TASK_OPTIONS;
	}

	@Override
	public void TaskAboutEvent() {
		this.applicationTask = Constants.APPLICATION_TASK_ABOUT;
		removeCenterFragments();
		activeCenterFragments.add( new PlaceholderFragmentData(R.id.container, new AboutFragment(this.applicationTask, Constants.ABOUT_FRAGMENT_TAG)) );
		addCenterFragments();
	}

	@Override
	public void TaskTestEvent() {
		this.applicationTask = Constants.APPLICATION_TASK_TEST;
		removeCenterFragments();
		activeCenterFragments.add( new PlaceholderFragmentData(R.id.container, new TestFragment(this.applicationTask, Constants.TEST_FRAGMENT_TAG)) );
		addCenterFragments();
	}

	/** com.aistkem.currencyconverter.interfaces.ConverterListener */

	@Override
	public void ShowCurrencyListEvent(PlaceholderFragment fragment, int task) {
		CurrencySearch listSearch = new CurrencySearch();
		/*
		RemoteDictionarySearch listSearch = new RemoteDictionarySearch(); // EVV: Вариант обращения к словарю на сервере
		listSearch.dictionary = "model";
		listSearch.searchPrefix = "toyota_";
		listSearch.searchConstraint = "";
		*/
		MainFragment.CurrencyDialogFragment dialogFragment = MainFragment.CurrencyDialogFragment.newInstance(listSearch, task);
		if (task == Constants.APPLICATION_TASK_FROM) {
			dialogFragment.setTitle(getString(R.string.txtCurrencyFrom));
		} else dialogFragment.setTitle(getString(R.string.txtCurrencyTo));
		//CoreApplication.ShowDialogFragment(fragment, dialogFragment, MainFragment.CurrencyDialogFragment.REQUEST_CODE);
		// EVV: Обрабатывать событие выбора из списка будем в активити, поэтому используем просто .show
		dialogFragment.show(getSupportFragmentManager(), "dialog");
	}

	@Override
	public void ConvertCurrencyEvent(float value) {
		if (currencyFrom == null || currencyTo == null || value < 0.001f) return;
		float result = value * currencyFrom.value * currencyTo.nominal / currencyTo.value / currencyFrom.nominal;
		PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentByTag(Constants.WELCOME_FRAGMENT_TAG);
		if (fragment != null) {
			fragment.RefreshView(Constants.FRAGMENT_REFRESH_TASK_RESULT, result);
		}
	}

	/** com.aistkem.currencyconverter.interfaces.CurrencyListListener */

	@Override
	public void StartRemoteRequestEvent() {
		requestId = ((ServiceHelper) serviceHelper).HttpRequestAction("http://www.cbr.ru/scripts/XML_daily.asp");
		//requestId = ((ServiceHelper) serviceHelper).TestServiceAction("something"); // EVV: Для тестирования
	}

	@Override
	public void StopRemoteRequestEvent(int resultCode, Bundle resultData) {
		if (resultCode == CoreServiceCommand.RESPONSE_SUCCESS) {
			TemplateListDialogFragment dialogFragment = (TemplateListDialogFragment) getSupportFragmentManager().findFragmentByTag("dialog");
			if (dialogFragment != null) {
				if (getSupportLoaderManager().getLoader(dialogFragment.GetTaskIdentifier()) != null)
					getSupportLoaderManager().getLoader(dialogFragment.GetTaskIdentifier()).forceLoad();
			}
		} else if (resultCode == CoreServiceCommand.RESPONSE_PROGRESS) {
			TemplateListDialogFragment dialogFragment = (TemplateListDialogFragment) getSupportFragmentManager().findFragmentByTag("dialog");
			if (dialogFragment != null) dialogFragment.showProgress(resultData.getInt(CoreServiceCommand.EXTRA_PROGRESS, -1));
		} else if (resultCode == CoreServiceCommand.RESPONSE_CANCEL) {
			// EVV: Это событие не обрабатывается здесь.
		} else { // Обработка ошибки обращения к серверу (RESPONSE_FAILURE):
			Toast.makeText(this, resultData.getString("error"), Toast.LENGTH_LONG).show();
			DialogFragment dialogFragment = (DialogFragment) getSupportFragmentManager().findFragmentByTag("dialog");
			if (dialogFragment != null) dialogFragment.dismiss();
		}
	}

	@Override
	public void CancelRemoteRequestEvent() {
		serviceHelper.cancelCommand(requestId);
	}

	@Override
	public void SelectCurrencyEvent(Currency currency, int task) {
		if (task == Constants.APPLICATION_TASK_FROM) {
			currencyFrom = currency;
		} else {
			currencyTo = currency;
		}
		PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentByTag(Constants.WELCOME_FRAGMENT_TAG);
		if (fragment != null) {
			fragment.RefreshView(Constants.FRAGMENT_REFRESH_TASK_CURRENCY, currencyFrom, currencyTo);
		}
		//Log.d(LOG_TAG, "SelectCurrencyEvent(): " + task);
	}

	/** Вспомогательные методы и классы для фрагментов этого активити */

	// EVV: В контексте этого приложения никак не используется.
	private class SomethingRefreshRun extends CommonRefreshRun {

		public SomethingRefreshRun() {
			super();
			Log.d(LOG_TAG, "SomethingRefreshRun create");
		}

		public void run() {
			super.run();
			Log.d(LOG_TAG, "SomethingRefreshRun start");
	    	RefreshThreadData data = new RefreshThreadData();
	    	data.dataName = "something";
			data.minSleepTime = Constants.SOMETHING_MIN_SLEEP_TIME;
			data.minSleepCount = Constants.SOMETHING_MIN_SLEEP_COUNT;
			data.maxSleepCount = Constants.SOMETHING_MAX_SLEEP_COUNT;
			data.maxFaults = Constants.SOMETHING_MAX_FAULTS;
	    	refreshLoop(this, data);
			stop();
		}

		protected void stop() {
			Log.d(LOG_TAG, "SomethingRefreshRun stop");
			super.stop();
		}
	}

	// EVV: В контексте этого приложения никак не используется.
	@Override
	protected void refreshLoop(CommonRefreshRun refreshRun, RefreshThreadData data) {
		int refreshFaults = 0;
		int sleepCount = data.minSleepCount;
		int maxSleepCount = data.minSleepCount;
    	
		refreshRun.isRunning = true;
		while (refreshRun.keepRunning)
			try {
				sleepCount = data.minSleepCount;
				
				if (data.dataName.equals("something")) {	// Обновление информации о чем-либо
					
				}
				
				if (refreshFaults >= data.maxFaults) {
					refreshFaults = 0;
					if (maxSleepCount < data.maxSleepCount) maxSleepCount++;
					sleepCount = maxSleepCount;
				}
				if (refreshRun.needPause)
					try {
						TimeUnit.SECONDS.sleep(data.minSleepTime * sleepCount);
					} catch (InterruptedException e) {
						Log.d(LOG_TAG, "refresh-" + data.dataName + "> " + e.toString());
						//CoreApplication.saveLog(this, "refresh-" + data.dataName + "> " + e.toString());
		            }
				else refreshRun.needPause = true;
			} catch (Exception e) {
				String message = CoreApplication.GetStackTrace(e);
				Log.e(LOG_TAG, "refresh-" + data.dataName + "> " + message + ". Refresh stopped!");
				//AccountCoreMethods.getInstance().SendLog("refresh-" + data.dataName + "> " + message);
				CoreApplication.saveLog(this, "refresh-" + data.dataName + "> " + message);
			}
		refreshRun.isRunning = false;
	}

}
