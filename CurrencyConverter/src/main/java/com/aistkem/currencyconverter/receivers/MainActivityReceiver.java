package com.aistkem.currencyconverter.receivers;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.receivers.core.CoreActivityReceiver;
import com.aistkem.currencyconverter.activities.MainActivity;

public class MainActivityReceiver extends CoreActivityReceiver {

	public MainActivityReceiver(MainActivity activity) {
		super(activity);
	}

	@Override
	public void onReceive_STATUS_MSG() {

	}

	@Override
	public void onReceive_STATUS_MSG_SPECIAL() {

	}

	@Override
	public void onReceive_STATUS_MSG_LOGIN() {

	}

	@Override
	public void onReceive_STATUS_MSG_DICTIONARY() {

	}

	@Override
	public void onReceive_STATUS_MSG_USER_INFO() {

	}

	@Override
	public void onReceive_STATUS_MSG_REGISTRATION() {

	}

}
