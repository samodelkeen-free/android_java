package com.aistkem.currencyconverter.receivers.core;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.activities.core.CoreActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public abstract class CoreActivityReceiver extends BroadcastReceiver {

	protected final String LOG_TAG = this.getClass().getName();
	protected CoreActivity mActivity;
	protected CoreApplication mApplication;
	
	public CoreActivityReceiver(CoreActivity activity) {
		mActivity = activity;
		mApplication = mActivity.GetApplication();
	}
	
	public void onReceive(Context context, Intent intent) {
		int task = intent.getIntExtra(Constants.PARAM_TASK, 0);
		int status = intent.getIntExtra(Constants.PARAM_STATUS, 0);
		switch (task) {
			case Constants.TASK_RECEIVED_MSG:
				switch (status) {
					case Constants.STATUS_MSG:
						onReceive_STATUS_MSG();
					break;
					case Constants.STATUS_MSG_SPECIAL:
						onReceive_STATUS_MSG_SPECIAL();
					break;
					case Constants.STATUS_MSG_LOGIN:
						onReceive_STATUS_MSG_LOGIN();
					break;
					case Constants.STATUS_MSG_DICTIONARY:
						onReceive_STATUS_MSG_DICTIONARY();
					break;
					case Constants.STATUS_MSG_USER_INFO:
						onReceive_STATUS_MSG_USER_INFO();
					break;
					case Constants.STATUS_MSG_REGISTRATION:
						onReceive_STATUS_MSG_REGISTRATION();
					break;
				}
			break;
			case Constants.TASK_SENT_MSG:

			break;
		}
		Log.d(LOG_TAG, "onReceive: task=" + task + ", status=" + status);
	}

	/**
	 * Получено произвольное сообщение сервера
	 */
	public abstract void onReceive_STATUS_MSG();

	/**
	 * Получено служебное сообщение сервера
	 */
	public abstract void onReceive_STATUS_MSG_SPECIAL();

	/**
	 * Получен идентификатор пользователя
	 */
	public abstract void onReceive_STATUS_MSG_LOGIN();

	/**
	 * Получен словарь
	 */
	public abstract void onReceive_STATUS_MSG_DICTIONARY();

	/**
	 * Получены данные пользователя
	 */
	public abstract void onReceive_STATUS_MSG_USER_INFO();

	/**
	 * Анкета пользователя доставлена на сервер
	 */
	public abstract void onReceive_STATUS_MSG_REGISTRATION();

}
