package com.aistkem.currencyconverter.handlers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.aistkem.currencyconverter.handlers.core.CoreServiceCommand;

public class TestServiceCommand extends CoreServiceCommand {

    private static final String TAG = "TestServiceCommand";

    private String arg;

    @Override
    public void doExecute(Intent intent, Context context, ResultReceiver callback) {
    	Bundle data = new Bundle();
	
	    int progress = 0;
	    sendProgress(progress);
	    /* EVV: Пример на будущее (внутреннее прерывание задачи)
		if (cancelled) {
			data.putString("message", "Command was cancelled internally");
    		notifyCancel(data);
		    Log.w(TAG, "Command was cancelled internally");
		    return;
		}
		*/
		try {
			Thread.sleep(10000);
			progress = 100;
			sendProgress(progress);
			data.putString("data", "Command completed");
			notifySuccess(data);
		} catch (InterruptedException e) {
			data.putString("error", e.toString());
			notifyFailure(data);
		}
	}

    @Override
    public int describeContents() {
    	return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    	dest.writeString(arg);
    }

    public static final Parcelable.Creator<TestServiceCommand> CREATOR = new Parcelable.Creator<TestServiceCommand>() {
		public TestServiceCommand createFromParcel(Parcel in) {
		    return new TestServiceCommand(in);
		}
	
		public TestServiceCommand[] newArray(int size) {
		    return new TestServiceCommand[size];
		}
    };

    private TestServiceCommand(Parcel in) {
    	isCancellable = false;
    	arg = in.readString();
    }

    public TestServiceCommand(String url) {
    	this.arg = arg;
    }

}
