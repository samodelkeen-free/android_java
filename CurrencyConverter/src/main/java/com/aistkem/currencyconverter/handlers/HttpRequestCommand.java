package com.aistkem.currencyconverter.handlers;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.aistkem.currencyconverter.Classes.LocalDbDataException;
import com.aistkem.currencyconverter.handlers.core.CoreServiceCommand;
import com.aistkem.currencyconverter.helpers.core.HttpMultipartHelper;
import com.aistkem.currencyconverter.repository.CurrencyMethods;

public class HttpRequestCommand extends CoreServiceCommand {

    private String url;

    @Override
    public void doExecute(Intent intent, Context context, ResultReceiver callback) {
    	Bundle data = new Bundle();
	
		if (TextUtils.isEmpty(url)) {
		    data.putString("error", "Incorrect URL");
		    notifyFailure(data);
		} else {
		    int progress = 0;
		    sendProgress(progress);
		    /* EVV: Пример на будущее
			if (cancelled) {
				data.putString("message", "Command was cancelled internally");
	    		notifyCancel(data);
			    Log.w(LOG_TAG, "Command was cancelled internally");
			    return;
			}
			*/
			try {
				HttpMultipartHelper httpMultipartHelper = new HttpMultipartHelper(context);
				httpMultipartHelper.setTimeout(20000);
				List<String> response = httpMultipartHelper.processGet(url, /*"UTF-8"*/"windows-1251");
				progress = 50;
				sendProgress(progress);
				//String str = "<ValCurs Date=\"11.12.2016\" name=\"Foreign Currency Market\"><Valute ID=\"R01010\"><NumCode>036</NumCode><CharCode>AUD</CharCode><Nominal>1</Nominal><Name>Australian dollar</Name><Value>47,2745</Value></Valute><Valute ID=\"R01020A\"><NumCode>944</NumCode><CharCode>AZN</CharCode><Nominal>1</Nominal><Name>Azer manat</Name><Value>35,8697</Value></Valute></ValCurs>";
				String str = TextUtils.join("", response);
				CurrencyMethods.getInstance().PlaceRemoteData(str);
				data.putString("data", "OK");
				notifySuccess(data);
			} catch (SocketTimeoutException e) {
				data.putString("error", e.toString());
				notifyFailure(data);
			} catch (IOException e) {
				data.putString("error", e.toString());
				notifyFailure(data);
			} catch (LocalDbDataException e) {
				data.putString("error", e.toString());
				notifyFailure(data);
			}
			progress = 100;
			sendProgress(progress);
		}
	}

    @Override
    public int describeContents() {
    	return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    	dest.writeString(url);
    }

    public static final Parcelable.Creator<HttpRequestCommand> CREATOR = new Parcelable.Creator<HttpRequestCommand>() {
		public HttpRequestCommand createFromParcel(Parcel in) {
		    return new HttpRequestCommand(in);
		}
	
		public HttpRequestCommand[] newArray(int size) {
		    return new HttpRequestCommand[size];
		}
    };

    private HttpRequestCommand(Parcel in) {
    	isCancellable = false;
    	url = in.readString();
    }

    public HttpRequestCommand(String url) {
    	this.url = url;
    }

}
