/*
 * Copyright (C) 2013 Alexander Osmanov (http://perfectear.educkapps.com)
 * 
 * Modified in 2016 by Vladimir Egorov: added CANCEL notifier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.aistkem.currencyconverter.handlers.core;

import com.aistkem.currencyconverter.Constants;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

@SuppressLint("ParcelCreator")
public abstract class CoreServiceCommand implements Parcelable {

    public static final String EXTRA_PROGRESS = Constants.PACKAGE.concat(".EXTRA_PROGRESS");

    public static final int RESPONSE_SUCCESS = 0;

    public static final int RESPONSE_FAILURE = 1;

    public static final int RESPONSE_PROGRESS = 2;

    public static final int RESPONSE_CANCEL = 3;

    private ResultReceiver mCallback;
    
    protected volatile boolean cancelled = false;
    
    public boolean isCancellable = true;
    
    protected final String LOG_TAG = this.getClass().getName();

	public final void execute(Intent intent, Context context, ResultReceiver callback) {
		this.mCallback = callback;
		doExecute(intent, context, callback);
	}

    protected abstract void doExecute(Intent intent, Context context, ResultReceiver callback);

	protected void notifySuccess(Bundle data) {
		sendUpdate(RESPONSE_SUCCESS, data);
	}

	protected void notifyFailure(Bundle data) {
		sendUpdate(RESPONSE_FAILURE, data);
	}

	protected void sendProgress(int progress) {
		Bundle b = new Bundle();
		b.putInt(EXTRA_PROGRESS, progress);
	
		sendUpdate(RESPONSE_PROGRESS, b);
	}

    protected void notifyCancel(Bundle data) {
    	sendUpdate(RESPONSE_CANCEL, data);
    }

	private void sendUpdate(int resultCode, Bundle data) {
		if (mCallback != null) {
			mCallback.send(resultCode, data);
		}
	}

	public synchronized void cancel() {
		cancelled = true;
	}

}
