package com.aistkem.currencyconverter.repository;

import com.aistkem.currencyconverter.repository.core.AccountCoreMethods;
import com.aistkem.currencyconverter.CurrencyConverterApplication;

public final class AccountMethods extends AccountCoreMethods {

	private static AccountMethods mInstance;

	public AccountMethods(CurrencyConverterApplication application) {
		super(application);
	}

	public static void initInstance(CurrencyConverterApplication application) {
		if (mInstance == null) {
			mInstance = new AccountMethods(application);
		}
	}

	public static AccountMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/* *** Работа с локальной БД *** */

}
