package com.aistkem.currencyconverter.repository;

import java.io.Reader;
import java.io.StringReader;

import org.simpleframework.xml.core.Persister;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.aistkem.currencyconverter.Classes.CurrencySearch;
import com.aistkem.currencyconverter.Classes.LocalDbDataException;
import com.aistkem.currencyconverter.Classes.ValCurs;
import com.aistkem.currencyconverter.Classes.Valute;
import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.CurrencyConverterApplication;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;

public final class CurrencyMethods {

	private static CurrencyMethods mInstance;

	protected final String LOG_TAG = this.getClass().getName();
	protected final CoreApplication mApplication;

	public CurrencyMethods(CurrencyConverterApplication application) {
		mApplication = application;
	}

	public static void initInstance(CurrencyConverterApplication application) {
		if (mInstance == null) {
			mInstance = new CurrencyMethods(application);
		}
	}

	public static CurrencyMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	// EVV: Не пригодилось.
	/**
	 * Отправляет запрос на курсы валют к серверу ЦБ
	 * @param currencySearch - объект критериев поиска. Здесь этот параметр никак не задействован, т.к. выбираем всё.
	 */
	public void GetRemoteRates(CurrencySearch currencySearch) {
		if (currencySearch == null) return;
		
	}

	/* *** Работа с локальной БД *** */

	/**
	 * Размещает в БД данные, полученные от сервера ЦБ
	 * @param data - строка в формате XML
	 */
	public void PlaceRemoteData(final String data) throws LocalDbDataException {
		final String[] usages = new String[] {"RUB", "USD", "EUR", "CNY"};
		Reader reader = new StringReader(data);
		Persister serializer = new Persister();
		try {
			ValCurs valCurs = serializer.read(ValCurs.class, reader, false);
			// Добавляем валюту "Российский рубль в список" (для актуализации даты курса рубля):
			Valute ruble = new Valute();
			ruble.NumCode = 643;
			ruble.CharCode = "RUB";
			ruble.Nominal = 1;
			ruble.Name = "Российский рубль";
			ruble.Value = "1.0";
            valCurs.Date = valCurs.Date + " 03:00:00"; // Приводим московское время в ответе сервера ЦБ к UTC
			valCurs.valute.add(ruble);
			mApplication.GetDb().beginTransaction();
			try {
		        for (Valute valute : valCurs.valute) {
		            ContentValues cv = new ContentValues();
		            cv.put("numcode", valute.NumCode);
		            cv.put("charcode", valute.CharCode);
		            cv.put("nominal", valute.Nominal);
		            cv.put("name", valute.Name);
		            cv.put("value", valute.Value.replace(",", "."));
		            cv.put("date", mApplication.readableDateFormat.parse(valCurs.Date).getTime());
		            int usage = CommonCoreMethods.getIndex(usages, valute.CharCode);
		            cv.put("usage", usage != -1 ? usage : usages.length );
					long rowID = (long) mApplication.GetDb().insertWithOnConflict("currency", null, cv, SQLiteDatabase.CONFLICT_IGNORE);
					if (rowID == -1) {
						if (mApplication.GetDb().update("currency", cv, "numcode = ? AND charcode = ?", new String[] { cv.getAsString("numcode"), cv.getAsString("charcode") }) == 0) {
							throw new LocalDbDataException("Ошибка обновления списка валют");
						}
					}
		        }
		        mApplication.GetDb().setTransactionSuccessful();
		        Log.d(LOG_TAG, "PlaceRemoteData():" + "Актуализировано " + valCurs.valute.size() + " элементов");
			} catch (SQLiteException e) {
				Log.e(LOG_TAG, "PlaceRemoteData():" + e.toString());
		    } finally {
		    	mApplication.GetDb().endTransaction();
		    }
		} catch (Exception e) {
			Log.e(LOG_TAG, "PlaceRemoteData():" + "Ошибка разбора XML-данных. " + e.toString());
			throw new LocalDbDataException("Ошибка разбора XML-данных. " + e.toString());
		}
	}

	/* *** Вспомогательные статические методы *** */

}
