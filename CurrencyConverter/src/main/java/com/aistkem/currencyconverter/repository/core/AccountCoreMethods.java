package com.aistkem.currencyconverter.repository.core;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.helpers.core.CoreDbHelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class AccountCoreMethods {

	private static AccountCoreMethods mInstance;

	protected final String LOG_TAG = this.getClass().getName();
	protected final CoreApplication mApplication;

	public AccountCoreMethods(CoreApplication application) {
		mApplication = application;
	}

	public static void initInstance(CoreApplication application) {
		if (mInstance == null) {
			mInstance = new AccountCoreMethods(application);
		}
	}

	public static AccountCoreMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/**
	 * Запрос временного пароля с сервера приложения
	 * @param phone
	 * @param keyword
	 * @return
	 */
	public boolean SmsPasswordRequest(String phone, String keyword) {
		String phoneChecking = CoreApplication.preparePhone(phone);
		String keywordChecking = CoreApplication.prepareKeyword(keyword);
		if (!phoneChecking.isEmpty()) {
			try {
				mApplication.GetDbHelper().RecallMessage(mApplication.GetDb(), Constants.REQUEST_SMS_PASSWORD);
				String request = Constants.REQUEST_SMS_PASSWORD + "||";
				JSONObject data = new JSONObject();
				data.put("group", mApplication.getClass().getSimpleName().equals("CurrencyConverterApplication") ? 0 : 1);
				data.put("phone", phoneChecking);
				if (!keywordChecking.isEmpty()) data.put("keyword", keywordChecking);
				request += data.toString();
				mApplication.GetDbHelper().SetOptionValue(mApplication.GetDb(), "login", phone);
				ArrayList<String> guidArray = new ArrayList<String>();
				mApplication.GetService().SendMessage(0, guidArray, request);
				if (guidArray.get(0) != null) {
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "userid_guid", null);
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "pass_guid", guidArray.get(0));
					return true;
				}
			} catch (JSONException e) {
				Log.e(LOG_TAG, "SmsPasswordRequest(): " + e.toString());
				return false;
			}
		}
		return false;
	}

	/**
	 * Передача временного пароля на сервер, запрос идентификатора пользователя приложения
	 * @param password
	 * @return
	 */
	public boolean UserIdRequest(String password) {
		String phone = mApplication.GetDbHelper().GetOptionValue(mApplication.GetDb(), "login");
		if (phone == null) return false;
		phone = CoreApplication.preparePhone(phone);
		if (phone.isEmpty()) return false;
		//String crypt_key = dbHelper.GetOptionValue(db, "crypt_key");
		//if (crypt_key == null) return;
		if (!password.isEmpty()) {
			try {
				mApplication.GetDbHelper().RecallMessage(mApplication.GetDb(), Constants.REQUEST_USER_ID);
				String request = Constants.REQUEST_USER_ID + "||";
				JSONObject data = new JSONObject();
				data.put("group", mApplication.getClass().getSimpleName().equals("CurrencyConverterApplication") ? 0 : 1);
				data.put("phone", phone);
				data.put("smspass", password);
				request += data.toString();
				ArrayList<String> guidArray = new ArrayList<String>();
				mApplication.GetService().SendMessage(0, guidArray, request);
				if (guidArray.get(0) != null) {
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "userid_guid", guidArray.get(0));
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "pass_guid", null);
					//mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "registration_guid", null);
					return true;
				}
			} catch (JSONException e) {
				Log.e(LOG_TAG, "UserIdRequest(): " + e.toString());
				return false;
			}
		}
		return false;
	}

	/**
	 * Запрос СМС-пароля для подтверждения регистрации (телефон и пароль разблокировки будут помещены в блок "data").
	 * @param phone
	 * @return
	 */
	public boolean SmsPasswordRegistrationRequest(String phone, String keyword) {
		String phoneChecking = CoreApplication.preparePhone(phone);
		String keywordChecking = CoreApplication.prepareKeyword(keyword);
		if (!phoneChecking.isEmpty() /*&& !keyword.isEmpty()*/) {
			try {
				mApplication.GetDbHelper().SetOptionValue(mApplication.GetDb(), "login", phone);
				mApplication.GetDbHelper().RecallMessage(mApplication.GetDb(), Constants.REQUEST_SMS_PASSWORD_REGISTRATION);
				String request = Constants.REQUEST_SMS_PASSWORD_REGISTRATION + "||"; // Запрос СМС-пароля для подтверждения регистрации (телефон и пароль разблокировки будут помещены в блок "data").
				JSONObject data = new JSONObject();
				data.put("group", mApplication.getClass().getSimpleName().equals("CurrencyConverterApplication") ? 0 : 1);
				data.put("phone", phoneChecking);
				if (!keywordChecking.isEmpty()) data.put("keyword", keywordChecking);
				request += data.toString();
				ArrayList<String> guidArray = new ArrayList<String>();
				mApplication.GetService().SendMessage(0, guidArray, request);
				if (guidArray.get(0) != null) {
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "userid_guid", null);
					mApplication.GetDbHelper().SetUserValue(mApplication.GetDb(), -1, "pass_guid", guidArray.get(0));
					return true;
				}
			} catch (JSONException e) {
				Log.e(LOG_TAG, "SmsPasswordRegistrationRequest(): " + e.toString());
				return false;
			}
		}
		return false;
	}

    /**
     * Отправляет анкетные данные пользователя на сервер
     * @param userid - идентификатор пользователя на сервере приложения
     * @param status - статус запроса к серверу (-1-зарезервирован к отправке, 0-на отправку)
     * @return GUID запроса к серверу
     */
    public String SendUserInfo(int userid, int status) {
    	String result = null;
		String[] columns = {"_id", "keyword", "family", "name", "patr"};
		try {
			Cursor cur = mApplication.GetDb().query("userinfo", columns, "_id = " + userid, null, null, null, null);
			if (null != cur) {
				if (cur.moveToFirst()) {
					JSONObject data = new JSONObject();
					data.put("group", 0);	// Группа пользователей - зарезервированная категория.
					data.put("keyword", cur.getString(1));
					data.put("family", cur.getString(2));
					data.put("name", cur.getString(3));
					data.put("patr", cur.getString(4));
					String request = Constants.REQUEST_REGISTRATION + "||" + data.toString();
					ArrayList<String> guidArray = new ArrayList<String>();
					if (mApplication.IsBound() && status != -1)
						mApplication.GetService().SendMessage(0, guidArray, request);
					else
						mApplication.GetDbHelper().MessageToQueue(mApplication.GetDb(), 0, guidArray, status, request);
					if (guidArray.get(0) != null) {
						result = guidArray.get(0);
						// Затираем парольную фразу и заносим GUID запроса к серверу:
						ContentValues cv = new ContentValues();
						cv.put("keyword", "");
						cv.put("registration_guid", guidArray.get(0));
						mApplication.GetDb().update("userinfo", cv, "_id = " + userid, null);
					}
				}
				cur.close();
			}
		} catch (JSONException e) {
			Log.e(LOG_TAG, "SendUserInfo(): " + e.toString());
		}
		return result;
    }

    /**
     * Запрос анкетных данных текущего пользователя с сервера и занесение их в БД.
     * @return GUID запроса к серверу
     */
    public String UserInfoRequest() {
    	String result = null;
    	//String userid = mApplication.GetDbHelper().GetOptionValue(mApplication.GetDb(), "userid");
    	//if (userid.equals("-1")) return;
    	if (mApplication.userId == -1) return result;
    	mApplication.GetDbHelper().RecallMessage(mApplication.GetDb(), Constants.REQUEST_USER_INFO);
		String request = Constants.REQUEST_USER_INFO + "||";
		ArrayList<String> guidArray = new ArrayList<String>();
		if (mApplication.IsBound())
			mApplication.GetService().SendMessage(0, guidArray, request);
		else
			mApplication.GetDbHelper().MessageToQueue(mApplication.GetDb(), 0, guidArray, request);
		if (guidArray.get(0) != null) {
			result = guidArray.get(0);
		}
		return result;
    }

	/* *** Работа с локальной БД *** */

	/**
	 * Инициализация текущего пользователя
	 */
	public void InitUser() {
		mApplication.userId = Integer.parseInt(mApplication.GetDbHelper().GetOptionValue(mApplication.GetDb(), "userid"));
		mApplication.isLogined = mApplication.userId != -1;
	}

	/**
	 * Обработка авторизации пользователя на сервере
	 * @param userid - идентификатор пользователя на сервере приложения
	 * @param family
	 * @param name
	 * @param patr
	 */
	public void InitUser(int userid, String family, String name, String patr) {
		ContentValues cv = new ContentValues();
		cv.put("_id", userid);
		cv.put("family", family);
		cv.put("name", name);
		cv.put("patr", patr);
	    long rowID = mApplication.GetDb().replace("userinfo", null, cv); // EVV: Внимание! Вычищаются все данные прошлого пользователя!
	    if (rowID != -1) {
	    	mApplication.GetDbHelper().SetOptionValue(mApplication.GetDb(), "userid", Integer.toString(userid));
			mApplication.userId = userid;
			mApplication.isLogined = true;
	    } else Log.e(LOG_TAG, "Error on inserting or updating 'userinfo' table in InitUser()");
	}

	/**
	 * Удаляет из локальной БД приватную информацию. Готовит базу данных для нового пользователя.
	 * @param
	 * @return
	 */
	public void ClearPrivateData() {
		CoreDbHelper.ClearTable(mApplication.GetDb(), "userinfo");
		mApplication.GetDbHelper().SetOptionValue(mApplication.GetDb(), "userid", "-1");
		mApplication.GetDbHelper().SetOptionValue(mApplication.GetDb(), "login", null);
		mApplication.GetDb().execSQL("insert into userinfo (_id, family, name) values ("
		+ "-1,"
		+ "'',"
		+ "''"
		+ ");");
		InitUser();
	}

	/**
	 * Отправка сообщения (и протокола) отладки на сервер
	 * @param text - сообщение отладки
	 * @param files - файлы протоколов
	 * @return
	 */
	public boolean SendLog(String text, String... files) {
		try {
			String request = Constants.REQUEST_LOGS + "||";
			JSONObject data = new JSONObject();
			data.put("text", text);
			request += data.toString();
			JSONArray attachments = new JSONArray();
			JSONObject attachment;
			for(int ii = 0; ii < files.length; ++ii) {
				attachment = new JSONObject();
				attachment.put("name", ii);
				attachment.put("file", files[ii]);
				attachments.put(attachment);
			}
			request += attachments.length() > 0 ? "||" + attachments.toString() : "";
			ArrayList<String> guidArray = new ArrayList<String>();
			mApplication.GetDbHelper().MessageToQueue(mApplication.GetDb(), 0, guidArray, request);
			if (guidArray.get(0) != null) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(LOG_TAG, "SendLog():" + e.toString());
		}
		return false;
	}

	/**
	 * Удаляет отправленные файлы логов с диска
	 * @param guid - GUID запроса к серверу приложения
	 * @return
	 */
	public boolean DeleteSentLogs(String guid) {
		boolean result = false;
		String[] columns = { "request" };
			Cursor cur = mApplication.GetDb().query("requests", columns, "guid = '" + guid + "'", null, null, null, null, "1");
			if (null != cur) {
				if (cur.moveToFirst()) {
					String[] paramArray = cur.getString(0).split("\\|\\|", 3);
					if (paramArray.length > 2) // Если имелись прикрепленные файлы:
					try {
						JSONArray attachments = new JSONArray(paramArray[2]);
						for (int ii = 0; ii < attachments.length(); ++ii) {
							JSONObject attachment = attachments.getJSONObject(ii);
							File file = new File(attachment.getString("file"));
							file.delete();
						}
						result = true;
			        } catch (JSONException e) {
			        	Log.e(LOG_TAG, "DeleteSentLogs():" + e.toString());
			        }
					else result = true;
				}
				cur.close();
			}
		return result;
	}

}
