package com.aistkem.currencyconverter.repository.core;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;

public class CommonCoreMethods {

	private static CommonCoreMethods mInstance;

	protected final String LOG_TAG = this.getClass().getName();
	protected final CoreApplication mApplication;

	public CommonCoreMethods(CoreApplication application) {
		mApplication = application;
	}

	public static void initInstance(CoreApplication application) {
		if (mInstance == null) {
			mInstance = new CommonCoreMethods(application);
		}
	}

	public static CommonCoreMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/**
	 * Запрос данных из удаленного словаря на сервере приложения TODO: Переделать формирование JSON-пакета!
	 * @param dictionary - идентификатор словаря
	 * @param searchPrefix - префикс данных словаря
	 * @param search - строка для поиска
	 * @return true/false - признак успешности размещения запроса в очереди
	 */
	public boolean GetRemoteDictionary(String dictionary, String searchPrefix, String search) {
    	mApplication.GetDbHelper().RecallMessage(mApplication.GetDb(), Constants.REQUEST_DICTIONARY);
    	String hash = mApplication.GetDbHelper().GetDictionaryHash(mApplication.GetDb(), dictionary, searchPrefix, search, Constants.DICTIONARY_LIMIT);
		String request = Constants.REQUEST_DICTIONARY + "||";
		request += "{";
		request += "\"dictionary\":" + "\"" + dictionary + "\",";
		request += "\"hash\":" + "\"" + hash + "\",";
		request += "\"search\":" + "\"" + (searchPrefix != null ? searchPrefix : "") + search + "\",";
		request += "\"limit\":" + "\"" + Constants.DICTIONARY_LIMIT + "\"";
		request += "}";
		ArrayList<String> guidArray = new ArrayList<String>();
		if (mApplication.IsBound())
			mApplication.GetService().SendMessage(0, guidArray, request);
		else
			mApplication.GetDbHelper().MessageToQueue(mApplication.GetDb(), 0, guidArray, request);
		if (guidArray.get(0) != null) {
    		return true;

		}
		return false;
	}

	/* *** Работа с локальной БД *** */

	/* *** Вспомогательные статические методы *** */

	public static String md5(String string) {
		byte[] hash;
	 
	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("Huh, MD5 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
	    }
	 
	    StringBuilder hex = new StringBuilder(hash.length * 2);
	 
	    for (byte b : hash) {
	        int i = (b & 0xFF);
	        if (i < 0x10) hex.append('0');
	        hex.append(Integer.toHexString(i));
	    }

	    return hex.toString();
	}

	public static String sha1(String string) {
		byte[] hash;

	    try {
	        hash = MessageDigest.getInstance("SHA-1").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("Huh, SHA-1 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
	    }

	    StringBuilder hex = new StringBuilder(hash.length * 2);

	    for (byte b : hash) {
	        int i = (b & 0xFF);
	        if (i < 0x10) hex.append('0');
	        hex.append(Integer.toHexString(i));
	    }

	    return hex.toString();
	}

	public static final int getColor(Context context, int id) {
		final int version = Build.VERSION.SDK_INT;
		if (version >= 23) {
			return ContextCompat.getColor(context, id);
		} else {
			return context.getResources().getColor(id);
		}
	}

	/**
	 * Сравнивает версии компонентов приложения
	 * @param v1
	 * @param v2
	 * @return
	 */
    public static int versionCompare(String v1, String v2) {
        String s1 = normalisedVersion(v1);
        String s2 = normalisedVersion(v2);
        int cmp = s1.compareTo(s2);
        //String cmpStr = cmp < 0 ? "<" : cmp > 0 ? ">" : "==";
        //System.out.printf("'%s' %s '%s'%n", v1, cmpStr, v2);
        return cmp;
    }

    public static String normalisedVersion(String version) {
        return normalisedVersion(version, ".", 4);
    }

    public static String normalisedVersion(String version, String sep, int maxWidth) {
        String[] split = Pattern.compile(sep, Pattern.LITERAL).split(version);
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            sb.append(String.format("%" + maxWidth + 's', s));
        }
        return sb.toString();
	}

	public static boolean compress(String inputFile, String compressedFile) {
		try {
			File file = new File(compressedFile);
			if (file.exists()) file.delete();
			ZipFile zipFile = new ZipFile(compressedFile);
			File inputFileH = new File(inputFile);
			ZipParameters parameters = new ZipParameters();
			
			// COMP_DEFLATE is for compression
			// COMP_STORE no compression
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			// DEFLATE_LEVEL_ULTRA = maximum compression
			// DEFLATE_LEVEL_MAXIMUM
			// DEFLATE_LEVEL_NORMAL = normal compression
			// DEFLATE_LEVEL_FAST
			// DEFLATE_LEVEL_FASTEST = fastest compression
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			
			// file compressed
			zipFile.addFile(inputFileH, parameters);
			
			long uncompressedSize = inputFileH.length();
			File outputFileH = new File(compressedFile);
			long comrpessedSize = outputFileH.length();
			
			//System.out.println("Size "+uncompressedSize+" vs "+comrpessedSize);
			double ratio = (double)comrpessedSize/(double)uncompressedSize;
			//System.out.println("File compressed with compression ratio : "+ ratio);
			Log.d("com.aistkem.currencyconverter.repository.CommonCoreMethods", "compress(): " + "File compressed with compression ratio: " + ratio);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Поиск индекса элемента в массиве по его значению
	 * @param array - массив значений
	 * @param value - значение массива
	 * @return Индекс значения в массиве
	 */
	public static <T> int getIndex(T[] array, T value) {
		int index = -1;
		for (int ii = 0; ii < array.length; ii++) {
		    if (array[ii].equals(value)) {
		        index = ii;
		        break;
		    }
		}
		return index;
	}

	/**
	 * Проверка строки на числовое значение
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str) {
		try {
			Float.parseFloat(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

}
