package com.aistkem.currencyconverter.repository;

import android.content.ContentValues;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.CoreApplication;
import com.aistkem.currencyconverter.CurrencyConverterApplication;
import com.aistkem.currencyconverter.helpers.core.CoreDbHelper;
import com.aistkem.currencyconverter.repository.core.TestCoreMethods;

public final class TestMethods extends TestCoreMethods {

	private static TestMethods mInstance;

	public TestMethods(CurrencyConverterApplication application) {
		super(application);
	}

	public static void initInstance(CurrencyConverterApplication application) {
		if (mInstance == null) {
			mInstance = new TestMethods(application);
		}
	}

	public static TestMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/**
	 * Запрос временного СМС-пароля для подтверждения регистрации
	 * @param phone
	 * @return
	 */
	public boolean SmsPasswordRegistrationRequest(String phone, String keyword) {
		CoreDbHelper.ClearTable(mApplication.GetDb(), "remote_dictionaries");
		AccountMethods.getInstance().ClearPrivateData();
		if (AccountMethods.getInstance().SmsPasswordRegistrationRequest(phone, keyword)) {
			return true;
		}
		return false;
	}

	/**
	 * Запрос временного СМС-пароля с сервера приложения
	 * @param phone
	 * @param keyword
	 * @return true/false
	 */
	public boolean SmsPasswordRequest(String phone, String keyword) {
		if (AccountMethods.getInstance().SmsPasswordRequest(phone, keyword)) {
			return true;
		}
		return false;
	}

	/**
	 * Передача временного пароля на сервер, запрос идентификатора пользователя приложения
	 * @param password
	 * @return true/false
	 */
	public boolean UserIdRequest(String password) {
		
		ContentValues cv = new ContentValues();
		cv.put("family", "Егоров-обмен валюты");
		cv.put("name", "Владимир-обмен валюты");
		cv.put("patr", "Викторович-обмен валюты");
		cv.put("keyword", CoreApplication.prepareKeyword("кошка-мышка"));
		long rowCount = mApplication.GetDb().update("userinfo", cv, "_id = -1", null);
		
		AccountMethods.getInstance().SendUserInfo(-1, -1);
		
		if (AccountMethods.getInstance().UserIdRequest(password)) {
			return true;
		}
		return false;
	}

	/**
	 * Передача анкетных данных на сервер
	 * @return true/false
	 */
	public boolean SendUserInfo() {
		
		ContentValues cv = new ContentValues();
		cv.put("family", "Егоров-обмен валюты 2");
		cv.put("name", "Владимир-обмен валюты 2");
		cv.put("patr", "Викторович-обмен валюты 2");
		cv.put("keyword", CoreApplication.prepareKeyword("кошка-мышка"));
		long rowCount = mApplication.GetDb().update("userinfo", cv, "_id = " + mApplication.userId, null);
		
		if (AccountMethods.getInstance().SendUserInfo(mApplication.userId, 0) != null) {
			return true;
		}
		return false;
	}

	/**
	 * Запрос анкетных данных с сервера
	 * @return true/false
	 */
	public boolean UserInfoRequest() {
		if (AccountMethods.getInstance().UserInfoRequest() != null) {
			return true;
		}
		return false;
	}

	/* *** Работа с локальной БД *** */

}
