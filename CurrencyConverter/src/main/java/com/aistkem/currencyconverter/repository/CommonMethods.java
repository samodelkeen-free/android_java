package com.aistkem.currencyconverter.repository;

import com.aistkem.currencyconverter.Constants;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;
import com.aistkem.currencyconverter.CurrencyConverterApplication;

public final class CommonMethods extends CommonCoreMethods {

	private static CommonMethods mInstance;

	public CommonMethods(CurrencyConverterApplication application) {
		super(application);
	}

	public static void initInstance(CurrencyConverterApplication application) {
		if (mInstance == null) {
			mInstance = new CommonMethods(application);
		}
	}

	public static CommonMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/* *** Работа с локальной БД *** */

	/* *** Вспомогательные статические методы *** */

}
