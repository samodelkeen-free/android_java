package com.aistkem.currencyconverter.repository.core;

import com.aistkem.currencyconverter.CoreApplication;

import android.util.Log;

public class TestCoreMethods {

	private static TestCoreMethods mInstance;

	protected final String LOG_TAG = this.getClass().getName();
	protected final CoreApplication mApplication;

	public TestCoreMethods(CoreApplication application) {
		mApplication = application;
	}

	public static void initInstance(CoreApplication application) {
		if (mInstance == null) {
			mInstance = new TestCoreMethods(application);
		}
	}

	public static TestCoreMethods getInstance() {
		return mInstance;
	}

	/* *** Работа с удаленной БД и сервисами *** */

	/* *** Работа с локальной БД *** */

	/* *** Вспомогательные статические методы *** */

}
