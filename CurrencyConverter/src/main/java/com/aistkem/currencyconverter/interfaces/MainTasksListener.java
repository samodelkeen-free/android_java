package com.aistkem.currencyconverter.interfaces;

public interface MainTasksListener {
	
	/**
	 * Событие вызова формы приветствия
	 */
	public void TaskWelcomeEvent();
	
	/**
	 * Событие вызова формы авторизации
	 */
	public void TaskLoginEvent();
	
	/**
	 * Событие вызова формы настроек
	 */
	public void TaskOptionsEvent();
	
	/**
	 * Событие вызова формы "О программе"
	 */
	public void TaskAboutEvent();
	
	/**
	 * Событие вызова формы тестирования
	 */
	public void TaskTestEvent();
	
}
