package com.aistkem.currencyconverter.interfaces.core;

public interface CoreTasksListener {
	
	/**
	 * Событие переключения между Activities
	 */
	public void TaskSwitchActivityEvent();
	
}
