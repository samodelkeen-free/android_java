package com.aistkem.currencyconverter.interfaces;

import android.os.Bundle;

import com.aistkem.currencyconverter.Classes.Currency;

public interface CurrencyListListener {
	
	/**
	 * Событие запроса текущих курсов валют с сервера ЦБ
	 * @param Без параметров
	 */
	public void StartRemoteRequestEvent();
	
	/**
	 * Событие получения какого-либо ответа от сервера ЦБ
	 * @param resultCode - код результата обработки
	 * @param resultData - данные, полученные в ходе обработки
	 */
	public void StopRemoteRequestEvent(int resultCode, Bundle resultData);
	
	/**
	 * Событие отмены запроса к серверу ЦБ (если бы возможно было отменить HTTP-запрос, то пригодилось бы)
	 * @param Без параметров
	 */
	public void CancelRemoteRequestEvent();
	
	/**
	 * Событие выбора значения из списка валют
	 * @param currency - экземпляр объекта "Валюта"
	 * @param task - идентификатор задачи (1 - выбор исходной валюты, 2 - выбор конечной валюты)
	 */
	public void SelectCurrencyEvent(Currency currency, int task);
	
}
