package com.aistkem.currencyconverter.interfaces;

import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;

public interface ConverterListener {
	
	/**
	 * Событие отображения диалога выбора валюты из списка
	 * @param fragment - ссылка на фрагмент, вызывающий диалог выбора валюты (пригодится, если будем возвращать ответ во фрагмент)
	 * @param task - идентификатор задачи (1 - выбор исходной валюты, 2 - выбор конечной валюты)
	 */
	public void ShowCurrencyListEvent(PlaceholderFragment fragment, int task);

	/**
	 * Событие пересчета валюты
	 * @param value - количество единиц валюты
	 */
	public void ConvertCurrencyEvent(float value);
	
}
