package com.aistkem.currencyconverter;

import java.io.File;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.aistkem.currencyconverter.activities.core.CoreActivity;
import com.aistkem.currencyconverter.helpers.DbHelper;
import com.aistkem.currencyconverter.helpers.ServiceHelper;
import com.aistkem.currencyconverter.repository.AccountMethods;
import com.aistkem.currencyconverter.repository.CommonMethods;
import com.aistkem.currencyconverter.repository.CurrencyMethods;
import com.aistkem.currencyconverter.repository.TestMethods;
import com.aistkem.currencyconverter.services.CurrencyConverterForegroundService;

public final class CurrencyConverterApplication extends CoreApplication {

	@Override
	public void onCreate() {
		super.onCreate();
		boolean needSendLog = false;
		mServiceHelper = new ServiceHelper(this);
		sharedPreferences = getSharedPreferences("currencyconverter", Context.MODE_PRIVATE);
		// Инициализация файла лога:
		File sd = this.getExternalFilesDir(null);
		if (sd != null) {
			logFilePath = sd.getPath();
			logFileName = logFilePath + File.separator + "currencyconverter.log";
			needSendLog = compressLogFile();
			//clearLog();
		}
		// Создание экземпляра класса переменных:
		variables = new Variables(this);

		// Инициализация объекта соединения с БД:
		dbHelper = new DbHelper(this);
		DbHelper.Vacuum(GetDb());
		isOldSqlite = CommonMethods.versionCompare(DbHelper.GetSqliteVersion(GetDb()), "3.7.11") < 0;

		CommonMethods.initInstance(this);	// Инициализируем singleton для хранилища общих методов работы
		AccountMethods.initInstance(this);	// Инициализируем singleton для хранилища методов работы с аккаунтом пользователя
		CurrencyMethods.initInstance(this);	// Инициализируем singleton для хранилища основных методов работы приложения
		TestMethods.initInstance(this);		// Инициализируем singleton для хранилища тестовых методов

		AccountMethods.getInstance().InitUser();	// Вариант получения идентификатора пользователя (и признака авторизации) в Application
		InitApplication();
		if (isLogined && needSendLog) sendLogFiles();
		Log.v(LOG_TAG, "onCreate(). Application created at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
	}

	/**
	 * Инициализация приложения
	 */
	@Override
	protected void InitApplication() {
		super.InitApplication();
	}

	public void CreateServiceConnection() {
		if (serviceConnection != null) return;
	    /* Объект для управления сервисом */
	    serviceConnection = new ServiceConnection() {
	    	@Override
	    	public void onServiceConnected(ComponentName name, IBinder service) {
	    		Log.d(LOG_TAG, "Application onServiceConnected()");
	    		if ( (appService = (CurrencyConverterForegroundService) ((CurrencyConverterForegroundService.ServiceBinder) service).getService()) != null ) {
		    		isBound = true;
		    		if (mCurrentActivity != null)
		    			((CoreActivity) mCurrentActivity).onBindServiceFragmentNotification();
	    		}
	    	}
	    	
	    	@Override
	    	public void onServiceDisconnected(ComponentName name) {
	    		Log.d(LOG_TAG, "Application onServiceDisconnected()");
	    		isBound = false;
	    		if (mCurrentActivity != null)
	    			((CoreActivity) mCurrentActivity).onUnBindServiceFragmentNotification();
	    	}
	    };
	}

	/*
	 * Запуск сервиса, если не запущен, и подключение к нему
	 */
	public void StartService() {
		serviceIntent = new Intent(this, CurrencyConverterForegroundService.class);
		if (startService(serviceIntent) != null) {
			bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
		}
	}

}