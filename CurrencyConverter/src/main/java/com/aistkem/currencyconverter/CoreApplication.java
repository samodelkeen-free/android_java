package com.aistkem.currencyconverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import com.aistkem.currencyconverter.helpers.core.CoreDbHelper;
import com.aistkem.currencyconverter.helpers.core.CoreServiceHelper;
import com.aistkem.currencyconverter.activities.core.CoreActivity;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment;
import com.aistkem.currencyconverter.fragments.core.PlaceholderFragment.ReplaceFragmentData;
import com.aistkem.currencyconverter.repository.core.AccountCoreMethods;
import com.aistkem.currencyconverter.repository.core.CommonCoreMethods;
import com.aistkem.currencyconverter.services.core.CoreService;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;

public class CoreApplication extends Application {

	protected final String LOG_TAG = this.getClass().getName();

	protected CoreServiceHelper mServiceHelper;
	
	protected Activity mCurrentActivity = null;
	protected CoreService appService;
	protected boolean isBound = false;
	protected Intent serviceIntent = null;
	protected ServiceConnection serviceConnection;

	protected Variables variables;
    protected CoreDbHelper dbHelper;

	public SharedPreferences sharedPreferences;

	protected static String logFilePath = null;
    protected static String logFileName = null;
    public static int userId = -1;				// Идентификатор текущего пользователя на сервере приложения
    public static boolean isLogined = false;	// Признак авторизации текущего пользователя в системе
    public static Point displaySize;
	// Application image files location (for projects with camera usage)
	public static File mediaStorageDir;
	public static File thumbStorageDir;

	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat readableDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	public static boolean isOldSqlite = false;
	
	/* *** Геттеры и сеттеры *** */

	public final Variables GetVariables() {
		return this.variables;
	}

	public final CoreService GetService() {
		return this.appService;
	}

	public final CoreDbHelper GetDbHelper() {
		return this.dbHelper;
	}

	public final SQLiteDatabase GetDb() {
		return this.dbHelper.getWritableDatabase();
	}

	public Activity getCurrentActivity() {
		return this.mCurrentActivity;
	}

	public void setCurrentActivity(Activity currentActivity) {
		this.mCurrentActivity = currentActivity;
	}

	public CoreServiceHelper getServiceHelper() {
		return mServiceHelper;
	}

	public static CoreApplication getApplication(Context context) {
		if (context instanceof CoreApplication) {
			return (CoreApplication) context;
		}
		return (CoreApplication) context.getApplicationContext();
	}

	@Override
	public void onCreate() {	// EVV: ВНИМАНИЕ: Метод запускается только при создании Application. Помним, что Application НЕ ВСЕГДА УНИЧТОЖАЕТСЯ при закрытии приложения!
		super.onCreate();
		/**/ // TODO: Раскомментировать для логирования в файл!
		Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException (Thread thread, Throwable e) {
				handleUncaughtException (thread, e);
			}
		});
		/**/
		//SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		CommonCoreMethods.initInstance(this);			// Инициализируем singleton для хранилища общих методов работы
		AccountCoreMethods.initInstance(this);			// Инициализируем singleton для хранилища методов работы с аккаунтом пользователя

        mediaStorageDir = new File(getExternalFilesDir(null), Constants.IMAGE_DIRECTORY_NAME);
		// Create the media storage directory if it does not exist
		if (!mediaStorageDir.exists())
			if (!mediaStorageDir.mkdirs()) {
				Log.e(LOG_TAG, "Oops! Failed create " + Constants.IMAGE_DIRECTORY_NAME + " directory");
			}
        thumbStorageDir = new File(getExternalFilesDir(null), Constants.THUMB_DIRECTORY_NAME);
		// Create the thumbnail storage directory if it does not exist
		if (!thumbStorageDir.exists())
			if (!thumbStorageDir.mkdirs()) {
				Log.e(LOG_TAG, "Oops! Failed create " + Constants.THUMB_DIRECTORY_NAME + " directory");
			}
		
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		readableDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	/**
	 * Инициализация приложения
	 */
	protected void InitApplication() {
		
	}

	public void CreateServiceConnection() {
		if (serviceConnection != null) return;
	    /* Объект для управления сервисом */
	    serviceConnection = new ServiceConnection() {
	    	@Override
	    	public void onServiceConnected(ComponentName name, IBinder service) {
	    		Log.d(LOG_TAG, "Application onServiceConnected()");
	    		if ( (appService = ((CoreService.ServiceBinder) service).getService()) != null ) {
		    		isBound = true;
		    		if (mCurrentActivity != null)
		    			((CoreActivity) mCurrentActivity).onBindServiceFragmentNotification();
	    		}
	    	}
	    	
	    	@Override
	    	public void onServiceDisconnected(ComponentName name) {
	    		Log.d(LOG_TAG, "Application onServiceDisconnected()");
	    		isBound = false;
	    		if (mCurrentActivity != null)
	    			((CoreActivity) mCurrentActivity).onUnBindServiceFragmentNotification();
	    	}
	    };
	}

	/**
	 * Запуск сервиса, если не запущен, и подключение к нему
	 */
	public void StartService() {
		serviceIntent = new Intent(this, CoreService.class);
		if (startService(serviceIntent) != null) {
			bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
		}
	}

	/**
	 * Отключение от сервиса
	 */
	public void UnbindService() {
		if (!isBound) return;
		unbindService(serviceConnection);
		isBound = false;
	}

	/**
	 * Отключение от сервиса и его остановка
	 */
	public void UnbindStopService() {
		//serviceIntent = new Intent(this, GeoService.class);
		if (serviceIntent != null) {
			if (isBound) {
				unbindService(serviceConnection);
				isBound = false;
			}
			stopService(serviceIntent);
		}
    	//stopService(new Intent(this, GeoService.class));
	}

	/**
	 * Признак связи с сервисом
	 */
	public boolean IsBound() {
		return isBound;
	}

	/**
	 * Открыть БД
	 */
	/*	EVV: Протестировать новый вариант и убрать!
	public void OpenDb() {
		db = dbHelper.getWritableDatabase();
	}
	*/
	/**
	 * Закрыть БД
	 */
	public void CloseDb() {
		dbHelper.close();
	}

	/**
	 * Обработчик вызова необрабатываемого исключения в приложении
	 * @param thread
	 * @param e
	 */
	public void handleUncaughtException (Thread thread, Throwable e) {
		/*
		e.printStackTrace(); // not all Android versions will print the stack trace automatically
		Intent intent = new Intent ();
		intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
		intent.setFlags (Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
		startActivity (intent);
		*/
		saveLog(this, "*** Unhandled exception: ***" + "\n" + GetStackTrace(e)); // Сохраняем в файле логов на устройстве клиента
		System.exit(1); // kill off the crashed app
	}

	/* *** Блок служебных статических функций *** */

	/*
	 * Получить разрешение экрана для Activity
	 */
	public static Point GetDisplaySize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point displaySize = new Point();
        display.getSize(displaySize);
        return displaySize;
	}

	/**
	 * Отобразить фрагмент в контейнере.
	 * @param activity - активность
	 * @param container - контейнер для фрагментов
	 * @param oldFragment - старый фрагмент
	 * @param newFragmentTag - тег нового фрагмента
	 * @param toStack - поместить транзакцию в стек
	 * @param newFragmentRequestCode - код возврата из фрагмента
	 * @return Идентификатор стека
	 */
	public static String ShowFragment(AppCompatActivity activity, int container, PlaceholderFragment oldFragment, String newFragmentTag, boolean toStack, int newFragmentRequestCode) {
		return ShowFragment(activity, container, oldFragment, newFragmentTag, toStack, newFragmentRequestCode, false);
	}

	/**
	 * Отобразить фрагмент в контейнере.
	 * @param activity - активность
	 * @param container - контейнер для фрагментов
	 * @param oldFragment - старый фрагмент
	 * @param newFragmentTag - тег нового фрагмента
	 * @param toStack - поместить транзакцию в стек
	 * @param newFragmentRequestCode - код возврата из фрагмента
	 * @param hideOnly - только спрятать старый фрагмент, не уничтожая его
	 * @return Идентификатор стека
	 */
	public static String ShowFragment(AppCompatActivity activity, int container, PlaceholderFragment oldFragment, String newFragmentTag, boolean toStack, int newFragmentRequestCode, boolean hideOnly) {
		FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        String stackIdentifier = "";
        Fragment newFragment = fm.findFragmentByTag(newFragmentTag);
        if (newFragment != null) {
            ft.show(newFragment);
        }

        if (oldFragment != null) {
        	if (hideOnly) ft.hide(oldFragment);	// только прячем
        	else ft.remove(oldFragment);		// удаляем из контейнера
        }

        if (toStack) {
        	if (oldFragment != null) {
        		stackIdentifier = oldFragment.GetTag();
	        	if (newFragmentRequestCode > 0 && newFragment != null)
	        		newFragment.setTargetFragment(oldFragment, newFragmentRequestCode);
        	} else {
        		stackIdentifier = "NULL";
        	}
        	ft.addToBackStack(stackIdentifier);
        }

		ft.commit();
		return stackIdentifier;
	}

	/**
	 * Заменить фрагмент в контейнере.
	 * @param activity
	 * @param container
	 * @param oldFragment
	 * @param newFragment
	 * @param toStack
	 * @param newFragmentRequestCode
	 * @return Идентификатор стека
	 */
	public static String ChangeFragment(AppCompatActivity activity, int container, PlaceholderFragment oldFragment, PlaceholderFragment newFragment, boolean toStack, int newFragmentRequestCode) {
		return ChangeFragment(activity, container, oldFragment, newFragment, toStack, newFragmentRequestCode, false);
	}

	/**
	 * Заменить фрагмент в контейнере.
	 * @param activity - активность
	 * @param container - контейнер для фрагментов
	 * @param oldFragment - старый фрагмент
	 * @param newFragment - новый фрагмент
	 * @param toStack - поместить транзакцию в стек
	 * @param newFragmentRequestCode - код возврата из фрагмента
	 * @param hideOnly - только спрятать старый фрагмент, не уничтожая его
	 * @return Идентификатор стека
	 */
	public static String ChangeFragment(AppCompatActivity activity, int container, PlaceholderFragment oldFragment, PlaceholderFragment newFragment, boolean toStack, int newFragmentRequestCode, boolean hideOnly) {
		FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        String stackIdentifier = "";
        if (newFragment != null) {
	        Fragment prevFragment = fm.findFragmentByTag(newFragment.GetTag());
	        if (prevFragment != null) {
	            ft.remove(prevFragment);
	        }
        }

        if (oldFragment != null) {
        	if (hideOnly) ft.hide(oldFragment);	// только прячем
        	else ft.remove(oldFragment);		// удаляем из контейнера
        }
        if (newFragment != null) {
	        Bundle args = new Bundle();
	        //args.putBoolean(IS_LOGINED, true);
	        //args.putInt("userid", 0);
	        //args.putString("guid", "");
	        newFragment.setArguments(args);
			ft.add(container, newFragment, newFragment.GetTag());
        }

        if (toStack) {
        	if (oldFragment != null) {
        		stackIdentifier = oldFragment.GetTag();
	        	if (newFragmentRequestCode > 0 && newFragment != null)
	        		newFragment.setTargetFragment(oldFragment, newFragmentRequestCode);
        	} else {
        		stackIdentifier = "NULL";
        	}
        	ft.addToBackStack(stackIdentifier);
        }

		ft.commit();
		return stackIdentifier;
	}

	public static String ChangeFragment(AppCompatActivity activity, List<ReplaceFragmentData> replaceFragments) {
		return ChangeFragment(activity, replaceFragments, null);
	}

	/**
	 * Заменить группу фрагментов.
	 * @param activity - активити, содержащее фрагменты
	 * @param replaceFragments - список фрагментов для замены
	 * @param stateName - наименование состояния стека
	 * @return Идентификатор состояния стека
	 */
	public static String ChangeFragment(AppCompatActivity activity, List<ReplaceFragmentData> replaceFragments, String stateName) {
		FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        String stackIdentifier = "";
        for (ReplaceFragmentData replaceFragmentData : replaceFragments) {
	        if (replaceFragmentData.newFragment != null) {
		        Fragment prevFragment = fm.findFragmentByTag(replaceFragmentData.newFragment.GetTag());
		        if (prevFragment != null) {
		            ft.remove(prevFragment);
		        }
	        }

	        if (replaceFragmentData.toStack) {
	        	if (replaceFragmentData.oldFragment != null) {
		        	stackIdentifier += replaceFragmentData.oldFragment.GetTag();
		        	if (replaceFragmentData.requestCode > 0 && replaceFragmentData.newFragment != null)
		        		replaceFragmentData.newFragment.setTargetFragment(replaceFragmentData.oldFragment, replaceFragmentData.requestCode);
	        	} else stackIdentifier += "NULL";
	        }

	        if (replaceFragmentData.oldFragment != null) ft.remove(replaceFragmentData.oldFragment);
	        if (replaceFragmentData.newFragment != null) {
		        Bundle args = new Bundle();
		        //args.putBoolean(IS_LOGINED, true);
		        //args.putInt("userid", 0);
		        //args.putString("guid", "");
		        replaceFragmentData.newFragment.setArguments(args);
				ft.replace(replaceFragmentData.containerId, replaceFragmentData.newFragment, replaceFragmentData.newFragment.GetTag());
	        }
        }
        stackIdentifier = (stateName != null) ? stateName : stackIdentifier;
        if (!stackIdentifier.isEmpty()) ft.addToBackStack(stackIdentifier);
		ft.commit();
		return stackIdentifier;
	}

	// TODO: Разобраться с необходимостью использования такого варианта!
	/**
	 * Отобразить диалог. Пригодится, если будем возвращать результат в вызывающий фрагмент.
	 * @param fragment - фрагмент-родитель
	 * @param dialogFragment - фрагмент диалога
	 * @param dialogFragmentRequestCode - код возврата из фрагмента
	 * @return
	 */
	public static void ShowDialogFragment(PlaceholderFragment fragment, DialogFragment dialogFragment, int dialogFragmentRequestCode) {
		FragmentManager fm = fragment.getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prevFragment = fm.findFragmentByTag("dialog");
        if (prevFragment != null) {
            ft.remove(prevFragment);
        }
        ft.addToBackStack(null);
        
        dialogFragment.setTargetFragment(fragment, dialogFragmentRequestCode);
        dialogFragment.show(ft, "dialog");
	}

	/**
	 * Получить и сохранить масштабированное изображение c привязкой к разрешению экрана.
	 * @param filePath Путь до исходного изображения.
	 * @param scale Коэффициент масштабирования (процент от высоты экрана устройства).
	 * @param displaySize Размер экрана устройства.
	 * @return Результирующее изображение.
	 */
	public static Bitmap GetScaledBitmap(String filePath, int scale, Point displaySize) {
		Bitmap outputBitmap = null;
		int indexSeparator = filePath.lastIndexOf(File.separator);
		int indexExtension = filePath.lastIndexOf(".");
		String fileName = filePath.substring(indexSeparator + 1, indexExtension) + "_" + scale + ".png";
		File file = new File(thumbStorageDir.getPath() + File.separator + fileName);
		if (file.exists()) {
			outputBitmap = BitmapFactory.decodeFile(file.getPath());
		} else {
			Bitmap inputBitmap = BitmapFactory.decodeFile(filePath);
			float maxSize = displaySize.y * scale / 100;
			float width = inputBitmap.getWidth();
			float height = inputBitmap.getHeight();
			float widthScale = width / maxSize;
			float heightScale = height / maxSize;
			float newScale = (widthScale > heightScale) ? widthScale : heightScale;
			int newWidth = Math.round(width / newScale);
			int newHeight = Math.round(height / newScale);
			try {
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				outputBitmap = Bitmap.createScaledBitmap(inputBitmap, newWidth, newHeight, false);
				outputBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
				fileOutputStream.flush();
				fileOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
	        }
			inputBitmap.recycle();
		}
		return outputBitmap;
	}

	/*
	 * Получение содержимого стека трассировки
	 */
	public static String GetStackTrace(final Throwable throwable) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}

	/*
	 * Очистка файла логирования на SD Card
	 */
	public static void clearLog() {
		if (logFileName == null || logFileName.isEmpty()) return;
		File logFile = new File(logFileName);
		if (logFile.exists()) logFile.delete();
	}

	/*
	 * Добавление строки в файл логирования на SD Card
	 */
	public static void saveLog(Context context, String text) {
		if (logFileName == null || logFileName.isEmpty()) return;
		File logFile = new File(logFileName);
		String logTag = context.getClass().getName();
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				String message = GetStackTrace(e);
				Log.e(logTag, message);
				return;
			}
		}
		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
			buf.append(new java.sql.Timestamp(System.currentTimeMillis()).toString() + "\t" + logTag + "\t" + text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			String message = GetStackTrace(e);
			Log.e(logTag, message);
		}
	}

	/**
	 * Сжатие в Zip-архив файла журнала отладки
	 * @return true, если сжатие файлов логов прошло успешно
	 */
	public static boolean compressLogFile() {
		File file = new File(logFileName);
		if (!file.exists()) return false;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String zipFileName = logFileName.replaceAll("\\.log$", "_" + simpleDateFormat.format(file.lastModified()) + ".zip");
		if (CommonCoreMethods.compress(logFileName, zipFileName)) {
			clearLog();
			return true;
		}
		return false;
	}

	/**
	 * Отправка файла журнала отладки на сервер
	 * @return
	 */
	public static boolean sendLogFile() {
		File file = new File(logFileName);
		if (!file.exists()) return true;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String zipFileName = logFileName.replaceAll("\\.log$", "_" + simpleDateFormat.format(file.lastModified()) + ".zip");
		if (CommonCoreMethods.compress(logFileName, zipFileName)) {
			if (AccountCoreMethods.getInstance().SendLog("journal files", zipFileName))	{	// Отправляем почтой на сервер
				clearLog();
				return true;
			}
		}
		return false;
	}

	/**
	 * Отправка сжатых файлов журнала отладки на сервер
	 * @return
	 */
	public static boolean sendLogFiles() {
		if (logFilePath == null || logFilePath.isEmpty()) return false;
		File file = new File(logFilePath);
		File[] matchingFiles = file.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".zip");
			}
		});
		if (matchingFiles != null && matchingFiles.length > 0) {
			String[] fileNames = new String[matchingFiles.length];
			for (int ii = 0; ii < matchingFiles.length; ++ii) {
				fileNames[ii] = matchingFiles[ii].getAbsolutePath();
			}
			if (AccountCoreMethods.getInstance().SendLog("journal files", fileNames)) {	// Отправляем почтой на сервер
				return true;
			}
		}
		return false;
	}

	/**
	 * Инициализация LogCat
	 */
	public static void setupLogCat(Context context) {
	    File sd = context.getExternalFilesDir(null);
	    String filename	= sd.getPath() + File.separator + Constants.PACKAGE + ".log";
	    String command = "logcat -f " + filename + " -r 128 -v time " + Constants.PACKAGE + ":V";
	    Log.d(context.getClass().getName(), "command: " + command);
	    try {
	        Runtime.getRuntime().exec(command);
	    }
	    catch(IOException e) {
        	String message = CoreApplication.GetStackTrace(e);
        	Log.e(context.getClass().getName(), message);
	    }
	}

	/*
	 * Проверяет номер телефона на корректность (и убирает первый пробел ?!)
	 */
	public static String preparePhone(String str) {
		
		final String regexPhone = "^\\+\\d{1,3}\\s\\(\\d{3}\\)\\s\\d{3}\\-\\d{2}-\\d{2}$";
		//final String regexNumber0 = "^\\+\\d+\\s|\\D+";
		final String regexReplace = "\\s";
		
		String result = "";
		if (str.matches(regexPhone)) {
			result = str.replaceFirst(regexReplace, "");
		}
		return result;
	}

	/*
	 * Обработка введенного ключевого слова/фразы (изъятие некорректных символов)
	 */
	public static String prepareKeyword(String str) {
		
		final String regexKeyword = "^[^\"'|]*$";
		final String regexReplace = "[\\t\\n\\r]";
		
		String result = "";
		if (str.matches(regexKeyword)) {
			result = str.replaceAll(regexReplace, " ");
		}
		return result.trim();
	}

}